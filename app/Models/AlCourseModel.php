<?php namespace App\Models;

use CodeIgniter\Model;

class AlCourseModel extends Model
{
    protected $table = 'al_course';
    protected $primaryKey = 'id';
    protected $allowedFields = ['code', 'name', 'status'];

    protected $validationRules = [
        'code' => 'required|max_length[10]',
        'name' => 'required|max_length[100]',
    ];

    protected $validationRulesForCreateUpdate = [
        'code' => 'required|is_unique[al_course.code]|max_length[10]',
        'name' => 'required|max_length[100]',
    ];

    protected $validationRulesForUpdateSameCode = [
        'code' => 'required|max_length[10]',
        'name' => 'required|max_length[100]',
    ];

    public function getCourse()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getCourseByID($id)
    {
        return $this->find($id);
    }

    public function registerCourse($getPost)
    {   
        $data = [
            'code' => $getPost['code'],
            'name'  => $getPost['name'],
        ];

        if (!$this->checkUniqueCode($getPost['code']))
            $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editCourse($id, $getPost)
    {
        $data = [
            'id' => $id,
            'code' => $getPost['code'],
            'name' => $getPost['name'],
        ];

        $alCourseModel = $this->getCourseByID($id);

        if ($alCourseModel['code'] == $getPost['code'])
            $this->setValidationRules($this->validationRulesForUpdateSameCode);
        else
            if (!$this->checkUniqueCode($getPost['code']))
                $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function removeCourse($id)
    {   
        $data = [
            'id' => $id,
            'status' => 0,
        ];

        return $this->save($data);
    }

    public function checkUniqueCode($code)
    {
        $check_unique_code = $this
            ->where([
                'code' => $code,
                'status' => 1,
            ])
            ->first();

        if (!$check_unique_code)
            return true;
        else
            return false;
    }
}
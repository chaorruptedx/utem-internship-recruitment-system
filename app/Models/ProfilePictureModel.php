<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\User;
use App\Models\PersonalDetailsModel;
use App\Models\OrganizationDetailsModel;

class ProfilePictureModel extends Model
{
    protected $table = 'profile_picture';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'type', 'path', 'status'];

    public function getProfilePictureByID($id)
    {
        return $this
            ->where([
                'id' => $id,
            ])
            ->first();
    }

    public function uploadProfilePicture($id_user, $file)
    {
        if ($file->isValid() && !$file->hasMoved())
        {
            $userModel = new UserModel();
            $personalDetailsModel = new PersonalDetailsModel();
            $organizationDetailsModel = new OrganizationDetailsModel();

            $user_data = $userModel->getUserByID($id_user);

            if ($user_data['role'] == 2 || $user_data['role'] == 3)
                $details_data = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);
            else if ($user_data['role'] == 4)
                $details_data = $organizationDetailsModel->getOrganizationDetailsByIDUser($id_user);
                
            if ($details_data['id_profile_picture'] != null)
                if ($validation = $this->removeOldProfilePicture($details_data['id_profile_picture']) !== true)
                    return $validation;

            $file->move('profile_picture/'.$id_user);
            
            $name = $file->getName();
            $type = $file->getExtension();
            $path = "profile_picture/".$id_user."/";

            $data = [
                'name' => $name,
                'type' => $type,
                'path' => $path,
            ];

            if ($this->save($data))
            {
                if ($user_data['role'] == 2 || $user_data['role'] == 3)
                    if ($validation = $personalDetailsModel->updateProfilePicture($details_data['id'], $this->InsertID()) === true)
                        return true;
                    else
                        return $validation;
                else if ($user_data['role'] == 4)
                    if ($validation = $organizationDetailsModel->updateProfilePicture($details_data['id'], $this->InsertID()) === true)
                        return true;
                    else
                        return $validation;
            }
            else
            {
                return $this->errors();
            }
        }
    }

    public function removeOldProfilePicture($id)
    {
        $data = [
            'id' => $id,
            'status' => 0,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }
}
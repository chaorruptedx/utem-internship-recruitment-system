<?php namespace App\Models;

use CodeIgniter\Model;

class InternshipApplicationModel extends Model
{
    protected $table = 'internship_application';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_personal_details', 'id_organization_details', 'application_status', 'notes'];

    public function getInternshipApplicationByID($id)
    {
        return $this->find($id);
    }

    public function getAppliedInternship($id_personal_details)
    {
        return $this
            ->select([
                'internship_application.*',
                'organization_details.name AS organization_name',
            ])
            ->join('organization_details', 'internship_application.id_organization_details = organization_details.id')
            ->where([
                'internship_application.id_personal_details' => $id_personal_details,
                'internship_application.status' => 1,
            ])
            ->findAll();
    }

    public function getByIDPersonalOrganizationDetails($id_personal_details, $id_organization_details, $application_status)
    {
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'id_organization_details'  => $id_organization_details,
                'status' => 1,
            ])
            ->whereIn('application_status', $application_status)
            ->first();
    }

    public function getInternshipApplicationByIDUser($id_user) // For Organization
    {
        return $this
            ->select([
                'internship_application.*',
                'personal_details.name AS student_name',
                'al_course.name AS course_name',
                'organization_details.name as organization_name',
            ])
            ->join('organization_details', 'internship_application.id_organization_details = organization_details.id')
            ->join('personal_details', 'personal_details.id = internship_application.id_personal_details')
            ->join('al_course', 'al_course.id = personal_details.id_course', 'left')
            ->where([
                'internship_application.status' => 1,
                'organization_details.id_user' => $id_user,
            ])
            ->findAll();
    }
    
    public function getStudentApplicationStatus($id_organization_details) // Organization Dashboard
    {
        return $this
            ->select([
                'SUM(CASE
                    WHEN application_status = 1 THEN 1
                    ELSE 0
                END) AS new',
                'SUM(CASE
                    WHEN application_status = 2 THEN 1
                    ELSE 0
                END) AS in_review',
                'SUM(CASE
                    WHEN application_status = 3 THEN 1
                    ELSE 0
                END) AS awaiting_student_reply',
                'SUM(CASE
                    WHEN application_status = 4 THEN 1
                    ELSE 0
                END) AS student_accepted',
            ], false)
            ->where([
                'id_organization_details' => $id_organization_details,
                'status' => 1,
            ])
            ->first();
    }

    public function updateInternship($id_personal_details, $id_organization_details, $application_status) // Apply New Internship
    {   
        $data = [
            'id_personal_details' => $id_personal_details,
            'id_organization_details'  => $id_organization_details,
            'application_status' => $application_status,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function updateNewApplicantStatus($id) // Organization First Time View Student Application
    {
        $check_new = $this
            ->where([
                'id' => $id,
                'application_status' => 1,
                'status' => 1,
            ])
            ->first();

        if ($check_new)
        {
            $data = [
                'id' => $id,
                'application_status' => 2,
            ];

            return $this->save($data);
        }

        return 0;
    }

    public function updateInternshipStatus($id, $application_status) // Update Application Status
    {   
        $data = [
            'id' => $id,
            'application_status' => $application_status,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function updateInternshipNotes($id, $getPost) // Update Application Status
    {   
        $data = [
            'id' => $id,
            'notes' => $getPost['notes'],
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }
}
<?php namespace App\Models;

use CodeIgniter\Model;

class AlLanguageModel extends Model
{
    protected $table = 'al_language';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'status'];

    protected $validationRules = [
        'name' => 'required|max_length[50]',
    ];

    protected $validationRulesForCreateUpdate = [
        'name' => 'required|is_unique[al_language.name]|max_length[50]',
    ];

    protected $validationRulesForUpdateSameName = [
        'name' => 'required|max_length[50]',
    ];

    public function getLanguage()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getLanguageByID($id)
    {
        return $this->find($id);
    }

    public function addLanguage($getPost)
    {   
        $data = [
            'name' => $getPost['name'],
        ];

        if (!$this->checkUniqueName($getPost['name']))
            $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editLanguage($id, $getPost)
    {
        $data = [
            'id' => $id,
            'name' => $getPost['name'],
            'status' => 1,
        ];

        $alLanguageModel = $this->getLanguageByID($id);

        if ($alLanguageModel['name'] == $getPost['name'])
            $this->setValidationRules($this->validationRulesForUpdateSameName);
        else
            if (!$this->checkUniqueName($getPost['name']))
                $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function removeLanguage($id)
    {   
        $data = [
            'id' => $id,
            'status' => 0,
        ];
        
        return $this->save($data);
    }

    public function checkUniqueName($name)
    {
        $check_unique_name = $this
            ->where([
                'name' => $name,
                'status' => 1,
            ])
            ->first();

        if (!$check_unique_name)
            return true;
        else
            return false;
    }
}
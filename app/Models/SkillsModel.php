<?php namespace App\Models;

use CodeIgniter\Model;

class SkillsModel extends Model
{
    protected $table = 'skills';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_personal_details','name', 'scale', 'status'];

    protected $validationRulesSkills = [
        'name' => 'required',
        'scale' => 'required',
    ];

    public function getSkillsByIDPersonalDetails($id_personal_details)
    {
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1
            ])
            ->findAll();
    }

    public function createSkills($id_personal_details, $getPost)
    {
        $this->setValidationRules($this->validationRulesSkills);

        if ($getPost['skill']['name'] != null && $getPost['skill']['scale'] != null)
        {
            foreach ($getPost['skill']['name'] as $key => $value)
            {
                $data = [
                    'id_personal_details' => $id_personal_details,
                    'name' => $value,
                    'scale' => $getPost['skill']['scale'][$key],
                ];

                if (!$this->save($data))
                    return $this->errors();
            }
        }
        else
        {
            $data = [
                'id_personal_details' => $id_personal_details,
                'name' => '',
                'scale' => '',
            ];

            if (!$this->save($data))
                    return $this->errors();
        }
        
        return true;
    }

    public function updateSkills($id_personal_details, $getPost)
    {
        $old_skills = $this->getSkillsByIDPersonalDetails($id_personal_details);

        if ($old_skills != null)
        {
            foreach ($old_skills as $value)
            {
                $data = [
                    'id' => $value['id'],
                    'status' => 0,
                ];

                $this->save($data);
            }
        }

        return $this->createSkills($id_personal_details, $getPost);
    }
}


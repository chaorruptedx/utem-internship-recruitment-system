<?php namespace App\Models;

use CodeIgniter\Model;

class AchievementModel extends Model
{
    protected $table = 'achievement';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_personal_details','name','year', 'status'];

    public function getAchievementByID($id)
    {
        
        return $this->find($id);
    }

    public function getAchievementDetailsByIDUser($id_personal_details)
    {
    
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1,
            ])
            ->first();
    }

    public function getAchievementsByIDPersonalDetails($id_personal_details)
    {
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1
            ])
            ->findAll();
    }

    // public function createAchievement($id_personal_details,$getPost)
    // {   
    //     $data = [
            
    //         'id_personal_details' => $id_personal_details,
    //         'name' => $getPost ['name'],

    //         'year' => $getPost ['year'],
    //     ];

    //     if ($this->save($data))
    //         return true;
    //     else
    //         return $this->errors();
    // }

    public function createAchievement($id_personal_details, $getPost)
    {
        if ($getPost['achievement']['year'] != null && $getPost['achievement']['name'] != null)
        {
            foreach ($getPost['achievement']['name'] as $key => $value)
            {
                $data = [
                    'id_personal_details' => $id_personal_details,
                    'name' => $value,
                    'year' => $getPost['achievement']['year'][$key],
                ];

                if (!$this->save($data))
                    return $this->errors();
            }
        }
        // else
        // {
        //     $data = [
        //         'id_personal_details' => $id_personal_details,
        //         'name' => '',
        //         'year' => '',
        //     ];

        //     if (!$this->save($data))
        //             return $this->errors();
        // }
        
        return true;
    }

    public function updateAchievement($id_personal_details, $getPost)
    {

        $old_achievements = $this->getAchievementsByIDPersonalDetails($id_personal_details);

        if ($old_achievements != null)
        {
            foreach ($old_achievements as $value)
            {
                $data = [
                    'id' => $value['id'],
                    'status' => 0,
                ];

                $this->save($data);
            }
        }
        
        return $this->createAchievement($id_personal_details, $getPost);
    }
}
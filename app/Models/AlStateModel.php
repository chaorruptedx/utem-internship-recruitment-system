<?php namespace App\Models;

use CodeIgniter\Model;

class AlStateModel extends Model
{
    protected $table = 'al_state';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'type', 'status'];

    protected $validationRules = [
        'name' => 'required|max_length[50]',
        'type' => 'required',
    ];

    protected $validationRulesForCreateUpdate = [
        'name' => 'required|is_unique[al_state.name]|max_length[50]',
        'type' => 'required',
    ];

    protected $validationRulesForUpdateSameName = [
        'name' => 'required|max_length[50]',
        'type' => 'required',
    ];

    public function getState()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getStateByID($id)
    {
        return $this->find($id);
    }

    public function addState($getPost)
    {   
        $data = [
            'name' => $getPost['name'],
            'type'  => $getPost['type'],
        ];

        if (!$this->checkUniqueName($getPost['name']))
            $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editState($id, $getPost)
    {
        $data = [
            'id' => $id,
            'name' => $getPost['name'],
            'type' => $getPost['type'],
        ];

        $alStateModel = $this->getStateByID($id);

        if ($alStateModel['name'] == $getPost['name'])
            $this->setValidationRules($this->validationRulesForUpdateSameName);
        else
            if (!$this->checkUniqueName($getPost['name']))
                $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function removeState($id)
    {   
        $data = [
            'id' => $id,
            'status' => 0,
        ];

        return $this->save($data);
    }

    public function checkUniqueName($name)
    {
        $check_unique_name = $this
            ->where([
                'name' => $name,
                'status' => 1,
            ])
            ->first();

        if (!$check_unique_name)
            return true;
        else
            return false;
    }
}
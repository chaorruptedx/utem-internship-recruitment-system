<?php namespace App\Models;

use CodeIgniter\Model;

class SupervisionModel extends Model
{
    protected $table = 'supervision';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_supervisor', 'id_supervisee', 'status'];

    public function getSuperviseeByIDSupervisor($id_supervisor)
    {
        return $this
            ->select([
                'id_supervisee',
                'personal_details.name AS personal_details_name',
                'personal_details.user_no',
                'al_faculty.code AS faculty_code',
                'al_course.code AS course_code',
                'personal_details.tel_no',
                'email',
            ])
            ->join('personal_details', 'personal_details.id = supervision.id_supervisee')
            ->join('al_faculty', 'personal_details.id_faculty = al_faculty.id', 'left')
            ->join('al_course', 'personal_details.id_course = al_course.id', 'left')
            ->join('user', 'personal_details.id_user = user.id')
            ->where([
                'supervision.id_supervisor' => $id_supervisor,
                'supervision.status' => 1,
            ])
            ->findAll();
    }

    public function getSupervisorByIDPersonalDetails($id_personal_details)
    {
        return $this
            ->select([
                'personal_details.*',
                'user.*',
            ])
            ->join('personal_details', 'personal_details.id = supervision.id_supervisor')
            ->join('user', 'personal_details.id_user = user.id')
            ->where([
                'supervision.id_supervisee' => $id_personal_details,
                'supervision.status' => 1,
            ])
            ->first();
    }

    public function assignStudentSupervisor($id_supervisor, $id_students)
    {
        $supervisors = $this
            ->where([
                'id_supervisor' => $id_supervisor,
                'status' => 1,
            ])
            ->findAll();

        if (!empty($supervisors) && is_array($supervisors))
        {
            foreach ($supervisors as $supervisor)
            {
                if (!in_array($supervisor['id_supervisee'], $id_students)) // Remove Student from Current Supervisor
                {
                    $data = [
                        'id' => $supervisor['id'],
                        'status' => 0,
                    ];

                    $this->save($data);

                    if (($key = array_search($supervisor['id_supervisee'], $id_students)) !== false)
                    {
                        unset($id_students[$key]);
                    }
                }
                else if (in_array($supervisor['id_supervisee'], $id_students)) // Ignore Student Already in Current Supervisor
                {
                    if (($key = array_search($supervisor['id_supervisee'], $id_students)) !== false)
                    {
                        unset($id_students[$key]);
                    }
                }
            }
        }

        if (!empty($id_students) && is_array($id_students))
        {
            foreach ($id_students as $id_student) // Add New Student under Current Supervisor
            {
                $data = [
                    'id_supervisor' => $id_supervisor,
                    'id_supervisee' => $id_student,
                ];

                $this->save($data);
            }
        }
    }
}
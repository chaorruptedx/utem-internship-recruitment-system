<?php namespace App\Models;

use CodeIgniter\Model;

class EducationModel extends Model
{
    protected $table = 'education';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_personal_details','cgpa'];

    protected $validationRulesCGPA = [
        'cgpa' => 'required|decimal|less_than_equal_to[4.00]',
    ];

    public function getEducationByID($id)
    {
        
        return $this->find($id);
    }

    public function getCGPAByIDPersonalDetails($id_personal_details)
    {
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1
            ])
            ->where('cgpa IS NOT NULL', null, false)
            ->first();
    }


    public function getEducationDetailsByIDUser($id_personal_details)
    {
    
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1,
            ])
            ->first();
    }

    public function createEducation($id_personal_details,$getPost)
    {   
        $data = [
            
            'id_personal_details' => $id_personal_details,
            'cgpa' => $getPost ['cgpa'],
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editResume($id, $getPost)
    {   
        $data = [
            'id' =>$id,
            'objective_statement' => $getPost['objective_statement'],
            'id_personal_details' => $id_personal_details,
            
        ];    

        if ($this->save($data))
            return true;
        else
            return $this->errors();
}

    public function createCGPA($id_personal_details, $getPost)
    {
        $data = [
            'id_personal_details' => $id_personal_details,
            'cgpa' => $getPost['cgpa'],
        ];

        $this->setValidationRules($this->validationRulesCGPA);

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function updateCGPA($id, $getPost)
    {
        $data = [
            'id' => $id,
            'cgpa' => $getPost['cgpa'],
        ];

        $this->setValidationRules($this->validationRulesCGPA);

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }
}


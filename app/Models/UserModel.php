<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\PersonalDetailsModel;
use App\Models\OrganizationDetailsModel;

class UserModel extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $allowedFields = ['email', 'password', 'role', 'status'];

    protected $validationRules = [
        'email' => 'required|valid_email|max_length[255]',
        'password' => 'required',
        'role' => 'required',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    protected $validationRulesForCreate = [
        'email' => 'required|valid_email|is_unique[user.email]|max_length[255]',
        'password' => 'required',
        'role' => 'required',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    protected $validationRulesForUpdate = [
        'email' => 'required|valid_email|is_unique[user.email]|max_length[255]',
        'role' => 'required',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    protected $validationRulesForUpdateAccount = [
        'email' => 'required|valid_email|is_unique[user.email]|max_length[255]',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    protected $validationRulesForUpdateSameEmail = [
        'email' => 'required|valid_email|max_length[255]',
        'role' => 'required',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    protected $validationRulesForUpdateSameEmailAccount = [
        'email' => 'required|valid_email|max_length[255]',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    public function getUser()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getUserByID($id)
    {
        return $this->find($id);
    }

    public function getUserByEmail($email)
    {
        return $this->where([
                'email' => $email,
                'status' => 1
            ])
            ->first();
    }

    public function getNumberOfUser()
    {
        return $this
            ->select([
                'SUM(CASE
                    WHEN role = 1 THEN 1
                    ELSE 0
                END) AS admin',
                'SUM(CASE
                    WHEN role = 2 THEN 1
                    ELSE 0
                END) AS coordinator',
                'SUM(CASE
                    WHEN role = 3 THEN 1
                    ELSE 0
                END) AS student',
                'SUM(CASE
                    WHEN role = 4 THEN 1
                    ELSE 0
                END) AS organization',
            ], FALSE)
            ->where([
                'status' => 1,
            ])
            ->first();
    }

    public function checkUniqueEmail($email)
    {
        $check_unique_email = $this
            ->where([
                'email' => $email,
                'status' => 1,
            ])
            ->first();

        if (!$check_unique_email)
            return true;
        else
            return false;
    }

    public function registerUser($getPost)
    {   
        $data = [
            'email' => $getPost['email'],
            'password'  => MD5($getPost['password']),
            'role' => $getPost['role'],
        ];

        if (!$this->checkUniqueEmail($getPost['email']))
            $this->setValidationRules($this->validationRulesForCreate);
        
        if ($this->save($data))
        {
            if ($getPost['role'] == 2 || $getPost['role'] == 3) {
                
                $personalDetailsModel = new PersonalDetailsModel();
                $validation = $personalDetailsModel->createPersonalDetails($this->InsertID(), $getPost);
            }

            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function editUser($id, $getPost)
    {   
        if (!empty($getPost['password']))
        {
            $data = [
                'id' => $id,
                'email' => $getPost['email'],
                'password'  => MD5($getPost['password']),
                'role' => $getPost['role'],
            ];
        }
        else
        {
            $data = [
                'id' => $id,
                'email' => $getPost['email'],
                'role' => $getPost['role'],
            ];
        }

        $userModel = $this->getUserByID($id);

        if ($userModel['email'] == $getPost['email'])
        {
            $this->setValidationRules($this->validationRulesForUpdateSameEmail);
        }
        else
        {
            if (!$this->checkUniqueEmail($getPost['email']))
                $this->setValidationRules($this->validationRulesForUpdate);
        }
        
        if ($this->save($data))
        {
            $personalDetailsModel = new PersonalDetailsModel();
            $organizationDetailsModel = new OrganizationDetailsModel();

            if ($getPost['role'] == 2 || $getPost['role'] == 3)
            {
                if ($organizationDetailsModel->getOrganizationDetailsByIDUser($id))
                    $organizationDetailsModel->deleteOrganizationDetails($id);

                if (!$personalDetailsModel->getPersonalDetailsByIDUser($id))
                {
                    if ($personal_details = $personalDetailsModel->getDeletedPersonalDetailsByIDUser($id))
                    {
                        $personalDetailsModel->restorePersonalDetails($id);
                        $personalDetailsModel->updatePersonalDetails($personal_details['id'], $getPost);
                    }
                    else
                        $personalDetailsModel->createPersonalDetails($id, $getPost);
                }
                else if ($personal_details = $personalDetailsModel->getPersonalDetailsByIDUser($id))
                {
                    $personalDetailsModel->updatePersonalDetails($personal_details['id'], $getPost);
                }
            }
            else if ($getPost['role'] == 4)
            {
                if ($personalDetailsModel->getPersonalDetailsByIDUser($id))
                    $personalDetailsModel->deletePersonalDetails($id);

                if (!$organizationDetailsModel->getOrganizationDetailsByIDUser($id))
                {
                    if ($organizationDetailsModel->getDeletedOrganizationDetailsByIDUser($id))
                        $organizationDetailsModel->restoreOrganizationDetails($id);
                }
            }
            else
            {
                if ($personalDetailsModel->getPersonalDetailsByIDUser($id))
                    $personalDetailsModel->deletePersonalDetails($id);

                if ($organizationDetailsModel->getOrganizationDetailsByIDUser($id))
                    $organizationDetailsModel->deleteOrganizationDetails($id);
            }

            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function editAccount($id, $getPost)
    {   
        if (!empty($getPost['password']))
        {
            $data = [
                'id' => $id,
                'email' => $getPost['email'],
                'password'  => MD5($getPost['password']),
            ];
        }
        else
        {
            $data = [
                'id' => $id,
                'email' => $getPost['email'],
            ];
        }

        $userModel = $this->getUserByID($id);

        if ($userModel['email'] == $getPost['email'])
        {
            $this->setValidationRules($this->validationRulesForUpdateSameEmailAccount);
        }
        else
        {
            if (!$this->checkUniqueEmail($getPost['email']))
                $this->setValidationRules($this->validationRulesForUpdateAccount);
        }
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function removeUser($id)
    {   
        $data = [
            'id' => $id,
            'status' => 0,
        ];

        if ($this->save($data))
        {
            $personalDetailsModel = new PersonalDetailsModel();

            if ($personalDetailsModel->getPersonalDetailsByIDUser($id))
                $personalDetailsModel->deletePersonalDetails($id);
        }
    }

    public function authenticateUser($getPost)
    {   
        $findUser = $this->where([
                'email' => $getPost['email'],
                'password' => MD5($getPost['password']),
                'status' => 1
            ])
            ->find();

        if ($findUser)
            return true;
        else
            return false;
    }
}
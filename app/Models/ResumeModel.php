<?php namespace App\Models;

use CodeIgniter\Model;

class ResumeModel extends Model
{
    protected $table = 'resume';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_personal_details','objective_statement'];

    public function getResumeByID($id)
    {
        
        return $this->find($id);
    }

    public function getResumeDetailsByIDUser($id_personal_details)
    {
    
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1,
            ])
            ->first();
    }
    public function createResume($id_personal_details, $getPost)
    {   
        $data = [
            'id_personal_details' => $id_personal_details,
            'objective_statement' => $getPost ['objective_statement'],
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editResume($id, $getPost)
    {
        $data = [
            'id' => $id,
            'objective_statement' => $getPost['objective_statement'],
        ];
            
            if ($this->save($data))
                return true;
            else
                return $this->errors();
    }

}


<?php namespace App\Models;

use CodeIgniter\Model;

class AnnouncementModel extends Model
{
    protected $table = 'announcement';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_user', 'title', 'content', 'status'];

    protected $validationRules = [
        'title' => 'required|max_length[100]',
        'content' => 'required|max_length[65535]',
    ];

    public function getAnnouncement()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getAnnouncementByID($id)
    {
        return $this->find($id);
    }
   
    public function createAnnouncement($id_user, $getPost)
    {   
        $data = [
            'id_user' => $id_user,
            'title' => $getPost['title'],
            'content'  => $getPost['content'],
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editAnnouncement($id, $id_user, $getPost)
    {
        $data = [
            'id' => $id,
            'id_user' => $id_user,
            'title' => $getPost['title'],
            'content'  => $getPost['content'],
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function removeAnnouncement($id)
    {   
        $data = [
            'id' => $id,
            'status' => 0,
        ];

        return $this->save($data);
    }

    
}
<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\AlCourseModel;

class PersonalDetailsModel extends Model
{
    protected $table = 'personal_details';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_user',  'id_course', 'id_profile_picture', 'nric_no', 'name', 'address','status','tel_no','user_no'];

    protected $validationRulesResume = [
        'address' => 'required|max_length[255]',
        'tel_no' => 'required|max_length[20]',
    ];

    public function getPersonalDetailsByID($id)
    {
        return $this
            // nama table.all , nama table.(apa attribute yg kau nak)
            ->select('personal_details.*,
            al_course.name AS course_name,
            achievement.name AS achievement_name,
            achievement.description,
            achievement.year,
            education.cgpa,
            education.name As edu_name,
            education.certificate,
            education.start_year,
            education.end_year,
            experience.name AS experience_name,
            experience.description AS exp_description,
            experience.start_year AS exp_start_year,
            experience.end_year AS exp_end_year')
            // 'nama table' , 'nama_table.nama attribute(relationship) = nama_table.nama attribute(relationship) , 'left' (join)
            ->join('al_course', 'personal_details.id_course = al_course.id','left')
            ->join('education', 'education.id_personal_details =personal_details.id','left')
            ->join('achievement', 'achievement.id_personal_details =personal_details.id','left')
            ->join('experience', 'experience.id_personal_details =personal_details.id','left')
            ->where([
                'personal_details.id' => $id,
                'personal_details.status' => 1,
            ])
            ->first();
    }

    public function getPersonalDetailsByIDUser($id_user)
    {
        return $this
            // nama table.all , nama table.(apa attribute yg kau nak)
            ->select('personal_details.*,
            al_course.name AS course_name,
            achievement.name AS achievement_name,
            achievement.description,
            achievement.year,
            education.cgpa,
            education.name As edu_name,
            education.certificate,
            education.start_year,
            education.end_year,
            experience.name AS experience_name,
            experience.description AS exp_description,
            experience.start_year AS exp_start_year,
            experience.end_year AS exp_end_year')
            // 'nama table' , 'nama_table.nama attribute(relationship) = nama_table.nama attribute(relationship) , 'left' (join)
            ->join('al_course', 'personal_details.id_course = al_course.id','left')
            ->join('education', 'education.id_personal_details =personal_details.id','left')
            ->join('achievement', 'achievement.id_personal_details =personal_details.id','left')
            ->join('experience', 'experience.id_personal_details =personal_details.id','left')
            ->where([
                'id_user' => $id_user,
                'personal_details.status' => 1,
            ])
            ->first();
    }

    public function getStudentInternshipStatus($id_user) // Coordinator Dashboard
    {
        return $this
            ->select([
                'DISTINCT(internship_application.id_personal_details)',
                'CASE
                    WHEN MAX(internship_application.application_status) = 4 THEN "got_internship"
                    WHEN MAX(internship_application.application_status) = 1 OR MAX(internship_application.application_status) = 2 OR MAX(internship_application.application_status) = 3 THEN "pending"
                    WHEN MAX(internship_application.application_status) IS NULL OR MAX(internship_application.application_status) = 0 THEN "not_apply"
                END AS student_status',
            ], false)
            ->join('supervision', 'supervision.id_supervisor = personal_details.id AND supervision.status = 1')
            ->join('personal_details student_personal_details', 'student_personal_details.id = supervision.id_supervisee AND student_personal_details.status = 1')
            ->join('al_state', 'al_state.id = student_personal_details.id_state', 'left')
            ->join('internship_application', 'internship_application.id_personal_details = student_personal_details.id AND internship_application.status = 1', 'left')
            ->where([
                'personal_details.id_user' => $id_user,
                'personal_details.status' => 1
            ])
            ->groupBy([
                'internship_application.id_personal_details',
            ])
            ->findAll();
    }

    public function getStudentInternshipStatePending($id_user) // Coordinator Dashboard
    {
        $alStateModel = new AlStateModel();

        $state = $alStateModel->getState();

        if (!empty($state) && is_array($state))
        {
            foreach ($state as $state_data)
            {
                $query[] = "
                    IFNULL(SUM(CASE
                        WHEN organization_details.id_state = '".$state_data['id']."' THEN 1
                        ELSE 0
                    END), 0) AS '".$state_data['id']."'
                ";
            }
        }

        return $this
            ->select($query, false)
            ->join('supervision', 'supervision.id_supervisee = personal_details.id AND supervision.status = 1')
            ->join('personal_details coordinator_personal_details', 'coordinator_personal_details.id = supervision.id_supervisor AND coordinator_personal_details.status = 1')
            ->join('internship_application', 'internship_application.id_personal_details = personal_details.id AND internship_application.status = 1 AND (internship_application.application_status = 1 OR internship_application.application_status = 2 OR internship_application.application_status = 3)')
            ->join('organization_details', 'organization_details.id = internship_application.id_organization_details AND organization_details.status = 1')
            ->join('al_state', 'al_state.id = organization_details.id_state', 'left')
            ->where([
                'coordinator_personal_details.id_user' => $id_user,
                'personal_details.status' => 1,
            ])
            ->first();
    }

    public function getStudentInternshipStateAccepted($id_user) // Coordinator Dashboard
    {
        $alStateModel = new AlStateModel();

        $state = $alStateModel->getState();

        if (!empty($state) && is_array($state))
        {
            foreach ($state as $state_data)
            {
                $query[] = "
                    IFNULL(SUM(CASE
                        WHEN organization_details.id_state = '".$state_data['id']."' THEN 1
                        ELSE 0
                    END), 0) AS '".$state_data['id']."'
                ";
            }
        }
        
        return $this
            ->select($query, false)
            ->join('supervision', 'supervision.id_supervisee = personal_details.id AND supervision.status = 1')
            ->join('personal_details coordinator_personal_details', 'coordinator_personal_details.id = supervision.id_supervisor AND coordinator_personal_details.status = 1')
            ->join('internship_application', 'internship_application.id_personal_details = personal_details.id AND internship_application.status = 1 AND internship_application.application_status = 4')
            ->join('organization_details', 'organization_details.id = internship_application.id_organization_details AND organization_details.status = 1')
            ->join('al_state', 'al_state.id = organization_details.id_state', 'left')
            ->where([
                'coordinator_personal_details.id_user' => $id_user,
                'personal_details.status' => 1,
            ])
            ->first();
    }

    public function updateStudentResume($id,$getPost) // (id personal details, data details)
    {   
        $data = [
            'id' => $id,
            // 'id_course' => $getPost['id_course'],
            // 'nric_no' => $getPost['nric_no'],
            // 'name' => $getPost['name'],
            'address' => $getPost['address'],
            'tel_no' => $getPost['tel_no'],
            // 'user_no' => $getPost['user_no'],
        ];

        $this->setValidationRules($this->validationRulesResume);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function getDeletedPersonalDetailsByIDUser($id_user)
    {
        return $this
            ->where([
                'id_user' => $id_user,
                'status' => 0,
            ])
            ->first();
    }

    public function getCoordinator()
    {
        return $this
            ->select([
                'personal_details.id',
                'personal_details.name',
                'email',
                'tel_no',
                'al_course.code',
            ])
            ->join('user', 'personal_details.id_user = user.id')
            ->join('al_course', 'personal_details.id_course = al_course.id', 'left')
            ->where([
                'personal_details.status' => 1,
                'role' => 2,
            ])
            ->findAll();
    }

    public function getCoordinatorByID($id)
    {
        return $this
            ->select([
                'personal_details.id AS personal_details_id',
                'personal_details.name AS personal_details_name',
                'email',
                'tel_no',
                'al_faculty.name AS faculty_name',
                'al_course.name AS course_name',
            ])
            ->join('user', 'personal_details.id_user = user.id', 'left')
            ->join('al_faculty', 'personal_details.id_faculty = al_faculty.id', 'left')
            ->join('al_course', 'personal_details.id_course = al_course.id', 'left')
            ->where([
                'personal_details.id' => $id,
                'personal_details.status' => 1,
            ])
            ->first();
    }

    public function getStudentSupervision($id_supervisor)
    {
        return $this
            ->select([
                'DISTINCT(id_supervisee)',
                'personal_details.id',
                'personal_details.name AS personal_details_name',
                'personal_details.user_no',
                'al_faculty.code AS faculty_code',
                'al_course.code AS course_code',
                'tel_no',
                'email',
                'CASE
                    WHEN supervision.id_supervisor = '.$id_supervisor.' THEN "green"
                    WHEN supervision.id_supervisee IS NOT NULL THEN "grey"
                    ELSE "white"
                END AS student_status'
            ], FALSE)
            ->join('user', 'personal_details.id_user = user.id')
            ->join('al_faculty', 'personal_details.id_faculty = al_faculty.id', 'left')
            ->join('al_course', 'personal_details.id_course = al_course.id', 'left')
            ->join('supervision', 'supervision.id_supervisee = personal_details.id AND supervision.status = 1', 'left')
            ->where([
                'personal_details.status' => 1,
                'user.role' => 3,
            ])
            ->findAll();
    }

    public function getStudentSupervisionStatus()
    {
        return $this
            ->select([
                'SUM(CASE
                    WHEN supervision.id IS NULL THEN 1
                    ELSE 0
                END) AS student_unsupervised',
                'SUM(CASE
                    WHEN supervision.id_supervisor IS NOT NULL THEN 1
                    ELSE 0
                END) AS student_supervised',
            ], FALSE)
            ->join('user', 'personal_details.id_user = user.id')
            ->join('supervision', 'personal_details.id = supervision.id_supervisee AND supervision.status = 1', 'left')
            ->where([
                'personal_details.status' => 1,
                'user.role' => 3,
            ])
            ->first();
    }

    public function getStudentUnderProgramme()
    {
        $alCourseModel = new AlCourseModel();

        $course = $alCourseModel->getCourse();
        
        if (!empty($course) && is_array($course))
        {
            foreach ($course as $course_data)
            {
                $query[] = "
                    SUM(CASE
                        WHEN al_course.code = '".$course_data['code']."' THEN 1
                        ELSE 0
                    END) AS ".$course_data['code']."
                ";
            }

            return $this
                ->select($query, FALSE)
                ->join('user', 'personal_details.id_user = user.id', 'left')
                ->join('al_course', 'personal_details.id_course = al_course.id', 'left')
                ->where([
                    'personal_details.status' => 1,
                    'user.role' => 3,
                ])
                ->first();
        }
        else
        {
            return false;
        }

        
    }

    public function createPersonalDetails($id_user, $getPost)
    {   
        $data = [
            'id_user' => $id_user,
            'id_course' => $getPost['id_course'],
            'nric_no' => $getPost['nric_no'],
            'user_no' => $getPost['user_no'],
            'name' => $getPost['name'],
            'tel_no' => $getPost['tel_no'],
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function updatePersonalDetails($id, $getPost)
    {   
        $data = [
            'id' => $id,
            'id_course' => $getPost['id_course'],
            'nric_no' => $getPost['nric_no'],
            'user_no' => $getPost['user_no'],
            'name' => $getPost['name'],
            'tel_no' => $getPost['tel_no'],
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function updateProfilePicture($id_personal_details, $id_profile_picture)
    {   
        $data = [
            'id' => $id_personal_details,
            'id_profile_picture' => $id_profile_picture,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function deletePersonalDetails($id_user)
    {   
        $personalDetailsModel = $this
            ->where([
                'id_user' => $id_user,
                'status' => 1,
            ])
            ->first();

        $data = [
            'id' => $personalDetailsModel['id'],
            'status' => 0,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function restorePersonalDetails($id_user)
    {
        $personalDetailsModel = $this
            ->where([
                'id_user' => $id_user,
                'status' => 0,
            ])
            ->orderBy('created_at', 'DESC')
            ->first();

        $data = [
            'id' => $personalDetailsModel['id'],
            'status' => 1,
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function checkResumeCompletion($id_user)
    {
        $resume_completion = $this
            ->select([
                '*',
            ])
            ->where([
                'personal_details.id_user' => $id_user,
            ])
            ->where('personal_details.address IS NOT NULL', null, false)
            ->where('personal_details.tel_no IS NOT NULL', null, false)
            ->join('resume', 'personal_details.id = resume.id_personal_details AND resume.status = 1')
            ->join('education', 'personal_details.id = education.id_personal_details AND education.status = 1')
            ->join('skills', 'personal_details.id = skills.id_personal_details AND skills.status = 1')
            ->join('achievement', 'personal_details.id = achievement.id_personal_details AND achievement.status = 1')
            ->join('experience', 'personal_details.id = experience.id_personal_details AND experience.status = 1')
            ->first();

        if ($resume_completion)
            return true;
        else
            return false;
    }
}
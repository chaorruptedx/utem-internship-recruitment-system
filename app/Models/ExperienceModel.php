<?php namespace App\Models;

use CodeIgniter\Model;

class ExperienceModel extends Model
{
    protected $table = 'experience';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_personal_details','start_year','end_year','name','position', 'status'];

    public function getExperienceByID($id)
    {
        
        return $this->find($id);
    }

    public function getExperienceDetailsByIDUser($id_personal_details)
    {
    
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1,
            ])
            ->first();
    }

    public function getExperiencesByIDPersonalDetails($id_personal_details)
    {
    
        return $this
            ->where([
                'id_personal_details' => $id_personal_details,
                'status' => 1,
            ])
            ->findAll();
    }

    public function createExperience($id_personal_details,$getPost)
    {   
        if ($getPost['experience']['name'] != null)
        {
            foreach ($getPost['experience']['name'] as $key => $value)
            {
                $data = [
            
                    'id_personal_details' => $id_personal_details,
                    'start_year' => $getPost['experience']['start_year'][$key],
                    'end_year' => $getPost['experience']['end_year'][$key],
                    'name' => $value,
                    'position' => $getPost['experience']['position'][$key],
                ];

                if (!$this->save($data))
                    return $this->errors();
            }
        }
    }

    public function updateExperience($id_personal_details, $getPost)
    {

        $old_experiences = $this->getExperiencesByIDPersonalDetails($id_personal_details);

        if ($old_experiences != null)
        {
            foreach ($old_experiences as $value)
            {
                $data = [
                    'id' => $value['id'],
                    'status' => 0,
                ];

                $this->save($data);
            }
        }
        
        return $this->createExperience($id_personal_details, $getPost);
    }
}


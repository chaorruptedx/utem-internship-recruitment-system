<?php namespace App\Models;

use CodeIgniter\Model;


class OrganizationDetailsModel extends Model
{
    protected $table = 'organization_details';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_user','id_profile_picture','name', 'id_state','type','address','tel_no','work_days' ,'start_day', 'end_day','open_hour', 'close_hour', 'job_description', 'status'];

    public function getOrganizationDetails()
    {
        return $this
            ->select('organization_details.*, al_state.name AS state_name')
            ->join('al_state', 'al_state.id = organization_details.id_state', 'left')
            ->where('organization_details.status', 1)
            ->findAll();
    }

    public function getOrganizationByID($id)
    {
        
        return $this
            ->select([
                'organization_details.*',
                'al_state.name AS state_name',
                'profile_picture.name AS profile_picture_name',
                'profile_picture.path AS profile_picture_path', 
                'user.email',
            ])
            ->join('user', 'organization_details.id_user = user.id', 'left')
            ->join('profile_picture', 'organization_details.id_profile_picture = profile_picture.id', 'left')
            ->join('al_state', 'al_state.id = organization_details.id_state', 'left')
            ->where([
                'organization_details.id' => $id
            ])
            ->first($id);
    }

    public function getOrganizationBySearch($getPost)
    {
        $this->where('status', 1);

        if ($getPost['name'])
            $this->where('name', $getPost['name']);
        
        if ($getPost['id_state'])
            $this->where('id_state', $getPost['id_state']);

        if ($getPost['job_description'])
            $this->like('job_description', $getPost['job_description']);

        return $this->findAll();
    }

    public function getOrganizationDetailsByIDUser($id_user)
    {
        return $this
            ->where([
                'id_user' => $id_user,
                'status' => 1,
            ])
            ->first();
    }

    public function getDeletedOrganizationDetailsByIDUser($id_user)
    {
        return $this
            ->where([
                'id_user' => $id_user,
                'status' => 0,
            ])
            ->first();
    }

    public function createProfile($id_user,$getPost)
    {   
        $data = [
            'id_user' => $id_user,
            'id_profile_picture' => $getPost['id_profile_picture'],
            'name' => $getPost['name'],
            'id_state' => $getPost['id_state'],
            'type' => $getPost['type'],
            'address' => $getPost['address'],
            'start_day' => $getPost['start_day'],
            'end_day' => $getPost['end_day'],
            'open_hour' => $getPost['open_hour'],
            'close_hour' => $getPost['close_hour'],
            'job_description' => $getPost['job_description'],
            'tel_no' => $getPost['tel_no'],
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editCompanyProfile($id, $getPost)
    {   
        $data = [
            'id' => $id,
            'name' => $getPost['name'],
            'type' => $getPost['type'],
            'address' => $getPost['address'],
            'start_day' => $getPost['start_day'],
            'end_day' => $getPost['end_day'],
            'open_hour' => $getPost['open_hour'],
            'close_hour' => $getPost['close_hour'],
            'job_description' => $getPost['job_description'],
            'tel_no' => $getPost['tel_no'],
        ];

            if ($this->save($data))
                return true;
            else
                return $this->errors();
    }

    public function updateProfilePicture($id_organization_details, $id_profile_picture)
    {   
        $data = [
            'id' => $id_organization_details,
            'id_profile_picture' => $id_profile_picture,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function deleteOrganizationDetails($id_user)
    {   
        $organizationDetailsModel = $this
            ->where([
                'id_user' => $id_user,
                'status' => 1,
            ])
            ->first();

        $data = [
            'id' => $organizationDetailsModel['id'],
            'status' => 0,
        ];
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function restoreOrganizationDetails($id_user)
    {
        $organizationDetailsModel = $this
            ->where([
                'id_user' => $id_user,
                'status' => 0,
            ])
            ->orderBy('created_at', 'DESC')
            ->first();

        $data = [
            'id' => $organizationDetailsModel['id'],
            'status' => 1,
        ];

        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function checkProfileCompletion($id_user)
    {
        $profile_completion = $this
            ->select([
                '*',
            ])
            ->where([
                'organization_details.id_user' => $id_user,
            ])
            ->where('organization_details.id_profile_picture IS NOT NULL', null, false)
            ->where('organization_details.id_state IS NOT NULL', null, false)
            ->where('organization_details.name IS NOT NULL', null, false)
            ->where('organization_details.type IS NOT NULL', null, false)
            ->where('organization_details.address IS NOT NULL', null, false)
            ->where('organization_details.tel_no IS NOT NULL', null, false)
            ->where('organization_details.job_description IS NOT NULL', null, false)
            ->where('organization_details.start_day IS NOT NULL', null, false)
            ->where('organization_details.end_day IS NOT NULL', null, false)
            ->where('organization_details.open_hour IS NOT NULL', null, false)
            ->where('organization_details.close_hour IS NOT NULL', null, false)
            ->first();

        if ($profile_completion)
            return true;
        else
            return false;
    }
}
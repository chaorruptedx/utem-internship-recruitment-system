<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

use App\Models\OrganizationDetailsModel;

class CompanyProfileFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $session = session();

        $organizationDetailsModel = new OrganizationDetailsModel();
        
        if ($session->has('id') && $session->get('role') == 4)
		{
            if (!$organizationDetailsModel->checkProfileCompletion($session->get('id')))
            {
                return redirect()->to(base_url('public/company/OrganisationProfile/create_profile'));
            }
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}
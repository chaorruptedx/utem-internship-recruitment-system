<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

use App\Models\PersonalDetailsModel;

class StudentResumeFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $session = session();

        $personalDetailsModel = new PersonalDetailsModel();
        
        if ($session->has('id') && $session->get('role') == 3)
		{
            if (!$personalDetailsModel->checkResumeCompletion($session->get('id')))
            {
                return redirect()->to(base_url('public/student/resume/create_resume'));
            }
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}
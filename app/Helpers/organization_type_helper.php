<?php  
if ( ! function_exists('organization_type'))
{
    function organization_type($type)
    {
        if ($type == 1)
            return 'Government';
        else if ($type == 2)
            return 'Private';
    }
}
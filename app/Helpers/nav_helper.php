<?php  
if ( ! function_exists('active_link'))
{
    function striposa($haystack, $needles=array(), $offset=0) {

        $chr = array();

        foreach($needles as $needle)
        {
            $res = stripos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }

        if(empty($chr)) return false;

        return min($chr);
    }

    function active_link($controllers, $methods = null)
    {
        $router_controller = service('router')->controllerName();
        $router_method = service('router')->methodName();

        if ($methods !== null)
            return (striposa($router_controller, $controllers) == TRUE && in_array($router_method, $methods)) ? 'active' : '';
        else
            return (striposa($router_controller, $controllers) == TRUE) ? 'active' : '';
    }    
}
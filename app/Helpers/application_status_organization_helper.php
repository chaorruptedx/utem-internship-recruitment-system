<?php  
if ( ! function_exists('application_status_organization'))
{
    function application_status_organization($application_status, $type)
    {
        if ($type == 1)
        {
            if ($application_status == 0)
                return 'Reject';
            else if ($application_status == 1)
                return 'New';
            else if ($application_status == 2)
                return 'In Review';
            else if ($application_status == 3)
                return 'Awaiting Student Reply';
            else if ($application_status == 4)
                return 'Student Accepted';
        }
        else if ($type == 2)
        {
            if ($application_status == 0)
                return 'text-danger';
            else if ($application_status == 1)
                return 'text-success';
            else if ($application_status == 2)
                return 'text-warning';
            else if ($application_status == 3)
                return 'text-info';
            else if ($application_status == 4)
                return 'text-primary';
        }
    }
}
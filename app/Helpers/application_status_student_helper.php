<?php  
if ( ! function_exists('application_status_student'))
{
    function application_status_student($application_status, $type)
    {
        if ($type == 1)
        {
            if ($application_status == 0)
                return 'Reject';
            else if ($application_status == 1)
                return 'Applied';
            else if ($application_status == 2)
                return 'In Review';
            else if ($application_status == 3)
                return 'Waiting for Student Approval';
            else if ($application_status == 4)
                return 'Student Accepted';
        }
        else if ($type == 2)
        {
            if ($application_status == 0)
                return 'text-danger';
            else if ($application_status == 1)
                return 'text-primary';
            else if ($application_status == 2)
                return 'text-warning';
            else if ($application_status == 3)
                return 'text-info';
            else if ($application_status == 4)
                return 'text-success';
        }
    }
}
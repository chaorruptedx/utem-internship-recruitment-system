<?php  
if ( ! function_exists('role_name'))
{
    function role_name($role)
    {
        if ($role == 1)
            return 'Admin';
        else if ($role == 2)
            return 'Coordinator';
        else if ($role == 3)
            return 'Student';
        else if ($role == 4)
            return 'Organization';
    }
}
<?php  
if ( ! function_exists('working_day'))
{
    function working_day($day_number)
    {
        if ($day_number == 0)
            return 'Monday';
        else if ($day_number == 1)
            return 'Tuesday';
        else if ($day_number == 2)
            return 'Wednesday';
        else if ($day_number == 3)
            return 'Thursday';
        else if ($day_number == 4)
            return 'Friday';
        else if ($day_number == 5)
            return 'Saturday';
        else if ($day_number == 6)
            return 'Sunday';
    }    
}
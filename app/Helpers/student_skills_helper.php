<?php  
if ( ! function_exists('student_skills'))
{
    function student_skills($skill_scale, $type)
    {
        if ($type == 1)
        {
            if ($skill_scale == 1)
                return '20%';
            else if ($skill_scale == 2)
                return '40%';
            else if ($skill_scale == 3)
                return '60%';
            else if ($skill_scale == 4)
                return '80%';
            else if ($skill_scale == 5)
                return '100%';
        }
        else if ($type == 2)
        {
            if ($skill_scale == 1)
                return '20';
            else if ($skill_scale == 2)
                return '40';
            else if ($skill_scale == 3)
                return '60';
            else if ($skill_scale == 4)
                return '80';
            else if ($skill_scale == 5)
                return '100';
        }
    }
}
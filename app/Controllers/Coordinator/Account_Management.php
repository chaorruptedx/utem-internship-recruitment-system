<?php namespace App\Controllers\Coordinator;
use App\Controllers\Coordinator\CoordinatorBaseController;
use App\Models\UserModel;
use App\Models\PersonalDetailsModel;
use App\Models\ProfilePictureModel;

class Account_Management extends CoordinatorBaseController
{
	public function index()
	{
        $id_user = $this->session->get('id');

        $userModel = new UserModel();
        $personalDetailsModel = new PersonalDetailsModel;
        $profilePictureModel = new ProfilePictureModel();

        $userModel = $userModel->getUserByID($id_user);
        $data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);
        $data['profile_picture'] = $profilePictureModel->getProfilePictureByID($data['personal_details']['id_profile_picture']);
        
        if ($this->request->getMethod() === 'post')
		{	
            $file = $this->request->getFile('file');

            if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($id_user, $file);
            }

			$userModel = new UserModel();
			
			$validation = $userModel->editAccount($id_user, $this->request->getPost());
			
			if ($validation === true)
				$success = 'User account successfully updated.';
			else
				$errors = $validation;

            $userModel = $userModel->getUserByID($id_user);
            $data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);
            $data['profile_picture'] = $profilePictureModel->getProfilePictureByID($data['personal_details']['id_profile_picture']);
		}
        
		echo view('Coordinator/header');
		echo view('Coordinator/Account_Management/index', [
            'success' => $success,
			'errors' => $errors,
            'userModel' => $userModel,
            'profile_picture' => $data['profile_picture'],
		]);
		echo view('Coordinator/footer');
    }
}
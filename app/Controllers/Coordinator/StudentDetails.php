<?php namespace App\Controllers\Coordinator;
use App\Controllers\Coordinator\CoordinatorBaseController;

class StudentDetails extends CoordinatorBaseController
{
	public function index()
	{
		echo view('Coordinator/header');
		echo view('Coordinator/student/stud_details');
		echo view('Coordinator/footer');
	}
}
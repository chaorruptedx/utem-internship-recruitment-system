<?php namespace App\Controllers\Coordinator;
use App\Controllers\Coordinator\CoordinatorBaseController;
use App\Models\AlStateModel;
use App\Models\PersonalDetailsModel;

class Dashboard extends CoordinatorBaseController
{
	public function index()
	{
		$id_user = $this->session->get('id');
		
		$alStateModel = new AlStateModel();
		$personalDetailsModel = new PersonalDetailsModel();

		$student_internship_status = $personalDetailsModel->getStudentInternshipStatus($id_user);

		if (!empty($student_internship_status) && is_array($student_internship_status))
		{
			$not_apply = 0;
			$pending = 0;
			$got_internship = 0;

			foreach ($student_internship_status as $student_internship_status_data)
			{
				if ($student_internship_status_data['student_status'] == 'not_apply')
				{
					$not_apply++;
				}
				else if ($student_internship_status_data['student_status'] == 'pending')
				{
					$pending++;
				}
				else if ($student_internship_status_data['student_status'] == 'got_internship')
				{
					$got_internship++;
				}
			}

			$data_internship['not_apply'] = $not_apply;
			$data_internship['pending'] = $pending;
			$data_internship['got_internship'] = $got_internship;
		}

		$student_internship_state_accepted = $personalDetailsModel->getStudentInternshipStateAccepted($id_user);
		$student_internship_state_pending = $personalDetailsModel->getStudentInternshipStatePending($id_user);

		if (!empty($student_internship_state_accepted) && is_array($student_internship_state_accepted))
		{
			$student_internship_state['accepted'][] = 'data1';

			foreach ($student_internship_state_accepted as $state_id => $number_of_student)
			{
				$student_internship_state['state_name'][] = $alStateModel->getStateByID($state_id)['name'];
				$student_internship_state['accepted'][] = $number_of_student;
			}
		}

		if (!empty($student_internship_state_pending) && is_array($student_internship_state_pending))
		{
			$student_internship_state['pending'][] = 'data2';

			foreach ($student_internship_state_pending as $state_name => $number_of_student)
			{
				$student_internship_state['pending'][] = $number_of_student;
			}
		}

		echo view('Coordinator/header');
		echo view('Coordinator/Dashboard/dashboard');
		echo view('Coordinator/footer', [
			'student_internship_status' => $data_internship,
			'student_internship_state' => $student_internship_state,
		]);
	}
}
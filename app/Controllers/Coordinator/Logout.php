<?php namespace App\Controllers\Coordinator;
use App\Controllers\Coordinator\CoordinatorBaseController;

class Logout extends CoordinatorBaseController
{
	public function index()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('public/'));
	}
}
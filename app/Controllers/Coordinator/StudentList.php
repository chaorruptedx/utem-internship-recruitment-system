<?php namespace App\Controllers\Coordinator;
use App\Controllers\Coordinator\CoordinatorBaseController;
use App\Models\PersonalDetailsModel;
use App\Models\SupervisionModel;

class StudentList extends CoordinatorBaseController
{
	public function index()
	{
		$personalDetailsModel = new PersonalDetailsModel();
		$supervisionModel = new SupervisionModel();

		$personal_details = $personalDetailsModel->getPersonalDetailsByIDUser($this->session->get('id'));
		$student = $supervisionModel->getSuperviseeByIDSupervisor($personal_details['id']);

		echo view('Coordinator/header');
		echo view('Coordinator/student/list_students', [
			'student' => $student,
		]);
		echo view('Coordinator/footer');
	}
}
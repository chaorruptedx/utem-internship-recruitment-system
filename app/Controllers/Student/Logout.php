<?php namespace App\Controllers\Student;
use App\Controllers\Student\StudentBaseController;

class Logout extends StudentBaseController
{
	public function index()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('public/'));
	}
}
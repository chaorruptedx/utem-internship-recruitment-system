<?php namespace App\Controllers\Student;
use App\Controllers\Student\StudentBaseController;
use App\Models\AnnouncementModel;
use App\Models\PersonalDetailsModel;
use App\Models\InternshipApplicationModel;

class Dashboard extends StudentBaseController
{
	public function index()
	{
		$id_user = $this->session->get('id');

		$AnnouncementModel = new AnnouncementModel();
		$Announce_data = $AnnouncementModel->getAnnouncement();

		$personalDetailsModel = new PersonalDetailsModel();
		$personal_details = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);

		$intershipApplicationModel = new InternshipApplicationModel();
		$internship_application = $intershipApplicationModel->getAppliedInternship($personal_details['id']);

		echo view('Student/header');
		echo view('Student/Dashboard/dashboard.php',[
			'AnnouncementModel' => $Announce_data,
			'internship_application' => $internship_application,
		]);
		echo view('Student/footer');
	}
	
	public function accept_internship($id_internship_application)
	{
		$intershipApplicationModel = new InternshipApplicationModel();
		$internship_application = $intershipApplicationModel->updateInternshipStatus($id_internship_application, 4);

		return redirect()->to(base_url('public/student/dashboard'));
	}

	public function reject_internship($id_internship_application)
	{
		$intershipApplicationModel = new InternshipApplicationModel();
		$internship_application = $intershipApplicationModel->updateInternshipStatus($id_internship_application, 0);

		return redirect()->to(base_url('public/student/dashboard'));
	}
}
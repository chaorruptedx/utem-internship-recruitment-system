<?php namespace App\Controllers\Student;
use App\Controllers\Student\StudentBaseController;
use App\Models\AlStateModel;
use App\Models\PersonalDetailsModel;
use App\Models\OrganizationDetailsModel;
use App\Models\InternshipApplicationModel;

class StudentApplicants extends StudentBaseController
{
	public function index()
	{
		$organizationDetailsModel = new OrganizationDetailsModel();

		$data['list_of_organization'] = $organizationDetailsModel->getOrganizationDetails();

		echo view('Student/header');
		echo view('Student/Applicants/comparing', [
			'list_of_organization' => $data['list_of_organization'],
		]);
		echo view('Student/footer');
	}

	public function compare_organization_data() // Receive Data from AJAX
	{
		$organizationDetailsModel = new OrganizationDetailsModel();

		$organization_details = $organizationDetailsModel->getOrganizationByID($this->request->getPost('id_organization'));

		$organization_details['type'] = organization_type($organization_details['type']);
		$organization_details['start_day'] = working_day($organization_details['start_day'])." - ".working_day($organization_details['end_day']);
		$organization_details['open_hour'] = date("g:i A", strtotime($organization_details['open_hour']))." - ".date("g:i A", strtotime($organization_details['close_hour']));
		
		return json_encode($organization_details);
	}
	
    public function organization_list()
	{
		// $alStateModel = new AlStateModel;
		$organizationDetailsModel = new OrganizationDetailsModel;

		// $data['al_state'] = $alStateModel->getState();
		$data['organization_details'] = $organizationDetailsModel->getOrganizationDetails();

		// if ($this->request->getMethod() === 'post')
		// {
		// 	$data['organization_details'] = $organizationDetailsModel->getOrganizationBySearch($this->request->getPost());
			
		// 	$search['name'] = $this->request->getPost('name');
		// 	$search['id_state'] = $this->request->getPost('id_state');
		// 	$search['job_description'] = $this->request->getPost('job_description');
		// }

		echo view('Student/header');
		echo view('Student/Applicants/list_of_organization_v2', [
			// 'al_state' => $data['al_state'],
			'organization_details' => $data['organization_details'],
			// 'name_search' => $search['name'],
			// 'id_state_search' => $search['id_state'],
			// 'job_description_search' => $search['job_description'],
		]);
		echo view('Student/footer');
	}
	
	public function view_organization($id_organization_details)
	{
		$id_user = $this->session->get('id');

		$personalDetailsModel = new PersonalDetailsModel;
		$organizationDetailsModel = new OrganizationDetailsModel;
		$internshipApplicationModel = new InternshipApplicationModel;

		$data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);
		$data['organization_details'] = $organizationDetailsModel->getOrganizationByID($id_organization_details);
		$data['internship_application'] = $internshipApplicationModel->getByIDPersonalOrganizationDetails($data['personal_details']['id'], $data['organization_details']['id'], [1, 2, 3, 4]);
		
		echo view('Student/header');
		echo view('Student/Applicants/view_organization_v2', [
			'organization_details' => $data['organization_details'],
			'internship_application' => $data['internship_application'],
		]);
		echo view('Student/footer');
	}

	public function apply_organization($id_organization_details)
	{
		$id_user = $this->session->get('id');

		$personalDetailsModel = new PersonalDetailsModel;
		$internshipApplicationModel = new InternshipApplicationModel;

		$data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);

		$internshipApplicationModel->updateInternship($data['personal_details']['id'], $id_organization_details, 1);

		return redirect()->to(base_url('public/student/StudentApplicants/view_organization/'.$id_organization_details));
	}
}
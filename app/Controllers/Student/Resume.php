<?php namespace App\Controllers\Student;
use App\Controllers\Student\StudentBaseController;
use App\Models\PersonalDetailsModel;
use App\Models\ResumeModel;
use App\Models\UserModel;
use App\Models\AlCourseModel;
use App\Models\AlFacultyModel;
use App\Models\EducationModel;
use App\Models\AchievementModel;
use App\Models\ExperienceModel;
use App\Models\SkillsModel;
use App\Models\SupervisionModel;
use App\Models\ProfilePictureModel;

class Resume extends StudentBaseController
{
	public function index()
	{
		$id = $this->session->get('id');

		$personalDetailsModel = new personalDetailsModel();
		$personalDetailsModel = $personalDetailsModel->getPersonalDetailsByIDUser($id);
		
		$AlCourseModel = new AlCourseModel();
		$AlCourseModel = $AlCourseModel->getCourseByID($personalDetailsModel['id_course']);

		$AlFacultyModel = new AlFacultyModel();
		$AlFacultyModel = $AlFacultyModel->getFacultyByID($personalDetailsModel['id_faculty']);

		$UserModel = new UserModel();
		$UserModel = $UserModel->getUserByID($id);
	
		$ResumeModel = new ResumeModel();
		$ResumeModel = $ResumeModel-> getResumeDetailsByIDUser($personalDetailsModel['id']);

		$profilePictureModel = new ProfilePictureModel();
		$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($personalDetailsModel['id_profile_picture']);

		$EducationModel = new EducationModel();
		$cgpa = $EducationModel->getCGPAByIDPersonalDetails($personalDetailsModel['id']);

		$skillsModel = new SkillsModel();
		$skills = $skillsModel->getSkillsByIDPersonalDetails($personalDetailsModel['id']);

		$AchievementModel = new AchievementModel();
		$achievements = $AchievementModel->getAchievementsByIDPersonalDetails($personalDetailsModel['id']);

		$ExperienceModel = new ExperienceModel();
		$experiences = $ExperienceModel->getExperiencesByIDPersonalDetails($personalDetailsModel['id']);

		$supervisionModel = new SupervisionModel();
		$coordinator = $supervisionModel->getSupervisorByIDPersonalDetails($personalDetailsModel['id']);

		echo view('Student/header');
		echo view('Student/Resume/stud_resume', [
			'personalDetailsModel' => $personalDetailsModel,
			'UserModel' => $UserModel,
			'AlCourseModel' => $AlCourseModel,
			'AlFacultyModel' => $AlFacultyModel,
			'ResumeModel' => $ResumeModel,
			'profile_picture' => $data['profile_picture'],
			'cgpa' => $cgpa,
			'skills' => $skills,
			'achievements' => $achievements,
			'experiences' => $experiences,
			'coordinator' => $coordinator,
		]);
		echo view('Student/footer');

		
	}

	
	public function create_resume()
	{	
		$id = $this->session->get('id'); //id user
		
		$personalDetailsModel = new personalDetailsModel();
		$ResumeModel = new ResumeModel();
		$EducationModel = new EducationModel();
		$ExperienceModel = new ExperienceModel();
		$skillsModel = new SkillsModel();
		$achievementsModel = new AchievementModel();
		$supervisionModel = new SupervisionModel();
		$profilePictureModel = new ProfilePictureModel();
		// $AlCourseModel = new AlCourseModel();

		// $AlCourseModel = $AlCourseModel->getCourse();

		$personal_detail = $personalDetailsModel->getPersonalDetailsByIDUser($id); //cari personal details berdasarkan id user yg log in
		$resume = $ResumeModel->getResumeDetailsByIDUser($personal_detail['id']);
		$cgpa = $EducationModel->getCGPAByIDPersonalDetails($personal_detail['id']);
		$skills = $skillsModel->getSkillsByIDPersonalDetails($personal_detail['id']);
		$achievements = $achievementsModel->getAchievementsByIDPersonalDetails($personal_detail['id']);
		$experiences = $ExperienceModel->getExperiencesByIDPersonalDetails($personal_detail['id']);
		$coordinator = $supervisionModel->getSupervisorByIDPersonalDetails($personal_detail['id']);
		$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($personal_detail['id_profile_picture']);

		if ($this->request->getMethod() === 'post')
		{
			if ($resume['id'] != null)
				$validation = $ResumeModel->editResume($resume['id'], $this->request->getPost()); //hntr id resume, id get post
			else
				$validation = $ResumeModel->createResume($personal_detail['id'], $this->request->getPost()); //hntr id personal, id get post

			if ($validation == true)
				$validation = $personalDetailsModel->updateStudentResume($personal_detail['id'], $this->request->getPost());

			if ($cgpa['id'] != null)
				$validation = $EducationModel->updateCGPA($cgpa['id'], $this->request->getPost());
			else
				$validation = $EducationModel->createCGPA($personal_detail['id'], $this->request->getPost());

			if ($skills != null)
			{
				$validation = $skillsModel->updateSkills($personal_detail['id'], $this->request->getPost());
			}
			else
			{
				$validation = $skillsModel->createSkills($personal_detail['id'], $this->request->getPost());
			}

			if ($achievements != null)
			{
				$validation = $achievementsModel->updateAchievement($personal_detail['id'], $this->request->getPost());
			}
			else
			{
				$validation = $achievementsModel->createAchievement($personal_detail['id'], $this->request->getPost());
			}

			if ($experiences != null)
			{
				$validation = $ExperienceModel->updateExperience($personal_detail['id'], $this->request->getPost());
			}
			else
			{
				$validation = $ExperienceModel->createExperience($personal_detail['id'], $this->request->getPost());
			}

			$file = $this->request->getFile('file');

			if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($id, $file);
            }

			$personal_detail = $personalDetailsModel->getPersonalDetailsByIDUser($id); //cari personal details berdasarkan id user yg log in
			$resume = $ResumeModel->getResumeDetailsByIDUser($personal_detail['id']);
			$cgpa = $EducationModel->getCGPAByIDPersonalDetails($personal_detail['id']);
			$skills = $skillsModel->getSkillsByIDPersonalDetails($personal_detail['id']);
			$achievements = $achievementsModel->getAchievementsByIDPersonalDetails($personal_detail['id']);
			$experiences = $ExperienceModel->getExperiencesByIDPersonalDetails($personal_detail['id']);
			$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($personal_detail['id_profile_picture']);
			
			if ($validation === true)
				$success = 'Resume successfully updated';
			else
				$errors = $validation;
		}
		
		if (!empty($skills))
			$skills_count = count($skills);

		if (!empty($achievements))
			$achievements_count = count($achievements);

		if (!empty($experiences))
			$experiences_count = count($experiences);


		echo view('Student/header');
		echo view('Student/Resume/create_resume', [
			'success' => $success,
			'errors' => $errors,
			'personal_detail' => $personal_detail,
			'resume' => $resume,
			'cgpa' => $cgpa,
			'skills' => $skills,
			'achievements' => $achievements,
			'experiences' => $experiences,
			'coordinator' => $coordinator,
			'profile_picture' => $data['profile_picture'],


			// 'AlCourseModel' => $AlCourseModel,
		]);
		echo view('Student/footer', [
			'skills_count' => $skills_count,
			'achievements_count' => $achievements_count,
			'experiences_count' => $experiences_count,
		]);
	}

	// public function edit_resume($id)
	// {
	
	// 	if ($this->request->getMethod() === 'post')
	// 	{	
			
	// 		$ResumeModel = new ResumeModel();
			
	// 		$ResumeModel = $ResumeModel->editResume($id, $this->request->getPost());
			
	// 		if ($validation === true)
	// 			$success = 'Company information successfully updated.';
	// 		else
	// 			$errors = $validation;

	// 	}
		
	// 	echo view('Student/header');
	// 	echo view('Student/resume/edit_resume', [
	// 		'success' => $success,
	// 		'errors' => $errors,
	// 		'ResumeModel' => $ResumeModel,
	// 	]);
	// 	echo view('Student/footer');
	// }

	public function edit_resume()
	{
		$id = $this->session->get('id'); //id user
		
		$personalDetailsModel = new personalDetailsModel();
		$ResumeModel = new ResumeModel();
		$EducationModel = new EducationModel();
		$AchievementModel = new AchievementModel();
		$ExperienceModel = new ExperienceModel();
		$skillsModel = new SkillsModel();
		$achievementsModel = new AchievementModel();
		$supervisionModel = new SupervisionModel();
		$profilePictureModel = new ProfilePictureModel();
		// $AlCourseModel = new AlCourseModel();

		// $AlCourseModel = $AlCourseModel->getCourse();

		$personal_detail = $personalDetailsModel->getPersonalDetailsByIDUser($id); //cari personal details berdasarkan id user yg log in
		$resume = $ResumeModel->getResumeDetailsByIDUser($personal_detail['id']);
		$cgpa = $EducationModel->getCGPAByIDPersonalDetails($personal_detail['id']);
		$skills = $skillsModel->getSkillsByIDPersonalDetails($personal_detail['id']);
		$achievements = $achievementsModel->getAchievementsByIDPersonalDetails($personal_detail['id']);
		$experiences = $ExperienceModel->getExperiencesByIDPersonalDetails($personal_detail['id']);
		$coordinator = $supervisionModel->getSupervisorByIDPersonalDetails($personal_detail['id']);
		$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($personal_detail['id_profile_picture']);

		if ($this->request->getMethod() === 'post')
		{
			if ($resume['id'] != null)
				$validation = $ResumeModel->editResume($resume['id'], $this->request->getPost()); //hntr id resume, id get post
			else
				$validation = $ResumeModel->createResume($personal_detail['id'], $this->request->getPost()); //hntr id personal, id get post

			if ($validation == true)
				$validation = $personalDetailsModel->updateStudentResume($personal_detail['id'], $this->request->getPost());

			if ($cgpa['id'] != null)
				$validation = $EducationModel->updateCGPA($cgpa['id'], $this->request->getPost());
			else
				$validation = $EducationModel->createCGPA($personal_detail['id'], $this->request->getPost());

			if ($skills != null)
			{
				$validation = $skillsModel->updateSkills($personal_detail['id'], $this->request->getPost());
			}
			else
			{
				$validation = $skillsModel->createSkills($personal_detail['id'], $this->request->getPost());
			}

			if ($achievements != null)
			{
				$validation = $achievementsModel->updateAchievement($personal_detail['id'], $this->request->getPost());
			}
			else
			{
				$validation = $achievementsModel->createAchievement($personal_detail['id'], $this->request->getPost());
			}

			if ($experiences != null)
			{
				$validation = $ExperienceModel->updateExperience($personal_detail['id'], $this->request->getPost());
			}
			else
			{
				$validation = $ExperienceModel->createExperience($personal_detail['id'], $this->request->getPost());
			}

			$file = $this->request->getFile('file');

			if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($id, $file);
            }

			$personal_detail = $personalDetailsModel->getPersonalDetailsByIDUser($id); //cari personal details berdasarkan id user yg log in
			$resume = $ResumeModel->getResumeDetailsByIDUser($personal_detail['id']);
			$cgpa = $EducationModel->getCGPAByIDPersonalDetails($personal_detail['id']);
			$skills = $skillsModel->getSkillsByIDPersonalDetails($personal_detail['id']);
			$achievements = $achievementsModel->getAchievementsByIDPersonalDetails($personal_detail['id']);
			$experiences = $ExperienceModel->getExperiencesByIDPersonalDetails($personal_detail['id']);
			$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($personal_detail['id_profile_picture']);
			
			if ($validation === true)
				$success = 'Resume successfully updated';
			else
				$errors = $validation;
		}
		
		if (!empty($skills))
			$skills_count = count($skills);

		if (!empty($achievements))
			$achievements_count = count($achievements);

		if (!empty($experiences))
			$experiences_count = count($experiences);


		echo view('Student/header');
		echo view('Student/Resume/create_resume', [
			'success' => $success,
			'errors' => $errors,
			'personal_detail' => $personal_detail,
			'resume' => $resume,
			'cgpa' => $cgpa,
			'skills' => $skills,
			'achievements' => $achievements,
			'experiences' => $experiences,
			'coordinator' => $coordinator,
			'profile_picture' => $data['profile_picture'],


			// 'AlCourseModel' => $AlCourseModel,
		]);
		echo view('Student/footer', [
			'skills_count' => $skills_count,
			'achievements_count' => $achievements_count,
			'experiences_count' => $experiences_count,
		]);
	}
}
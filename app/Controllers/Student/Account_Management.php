<?php namespace App\Controllers\Student;
use App\Controllers\Student\StudentBaseController;
use App\Models\UserModel;
use App\Models\PersonalDetailsModel;
use App\Models\ProfilePictureModel;

class Account_Management extends StudentBaseController
{
	public function index()
	{
        $id_user = $this->session->get('id');

        $userModel = new UserModel();
        $personalDetailsModel = new PersonalDetailsModel;
        $profilePictureModel = new ProfilePictureModel();

        $userModel = $userModel->getUserByID($id_user);
        $data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);
        $data['profile_picture'] = $profilePictureModel->getProfilePictureByID($data['personal_details']['id_profile_picture']);
        
        if ($this->request->getMethod() === 'post')
		{	
            $file = $this->request->getFile('file');

            if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($id_user, $file);
            }

			$userModel = new UserModel();
			
			$validation = $userModel->editAccount($id_user, $this->request->getPost());
			
			if ($validation === true)
				$success = 'User account successfully updated.';
			else
				$errors = $validation;

            $userModel = $userModel->getUserByID($id_user);
            $data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($id_user);
            $data['profile_picture'] = $profilePictureModel->getProfilePictureByID($data['personal_details']['id_profile_picture']);
		}
        
		echo view('Student/header');
		echo view('Student/Account_Management/index', [
            'success' => $success,
			'errors' => $errors,
            'userModel' => $userModel,
            'profile_picture' => $data['profile_picture'],
		]);
		echo view('Student/footer');
    }
}
<?php namespace App\Controllers;
use App\Models\UserModel;
use App\Models\PersonalDetailsModel;
use App\Models\OrganizationDetailsModel;

class Home extends BaseController
{
	public function index()
	{
		if ($this->session->has('id') && $this->session->get('role') == 1)
			return redirect()->to(base_url('public/admin/dashboard'));
		else if ($this->session->has('id') && $this->session->get('role') == 2)
			return redirect()->to(base_url('public/coordinator/dashboard'));
		else if ($this->session->has('id') && $this->session->get('role') == 3)
			return redirect()->to(base_url('public/student/dashboard'));
		else if ($this->session->has('id') && $this->session->get('role') == 4)
			return redirect()->to(base_url('public/company/dashboard'));

		if ($this->request->getMethod() === 'post')
		{
			$userModel = new UserModel();
			$personalDetailsModel = new PersonalDetailsModel();
			$organizationDetailsModel = new OrganizationDetailsModel();

			$validation = $userModel->authenticateUser($this->request->getPost());
			
			if ($validation === true)
			{
				$userModel = $userModel->getUserByEmail($this->request->getPost('email'));

				if ($userModel['role'] == 2 || $userModel['role'] == 3)
					$details = $personalDetailsModel->getPersonalDetailsByIDUser($userModel['id']);
				else if ($userModel['role'] == 4)
					$details = $organizationDetailsModel->getOrganizationDetailsByIDUser($userModel['id']);
				
				$this->session->set([
					'id' => $userModel['id'],
					'email' => $userModel['email'],
					'role' => $userModel['role'],
					'name' => $details['name'],
				]);

				if ($this->session->get('role') == 1)
					return redirect()->to(base_url('public/admin/dashboard'));
				else if ($this->session->get('role') == 2)
					return redirect()->to(base_url('public/coordinator/dashboard'));
				else if ($this->session->get('role') == 3)
					return redirect()->to(base_url('public/student/dashboard'));
				else if ($this->session->get('role') == 4)
					return redirect()->to(base_url('public/company/dashboard'));
				else
					$error = 'Error: Unknown role detected.';
			}
			else
			{
				$error = 'Wrong email or password. Please try again.';
			}
		}

		echo view('header');
		echo view('index', [
			'error' => $error,
		]);
		echo view('footer');
	}
}
<?php namespace App\Controllers\Company;
use App\Controllers\Company\CompanyBaseController;

class ApplicantsDetails extends CompanyBaseController
{
	public function index()
	{
		echo view('Company/header');
		echo view('Company/Applicants/applicants_details');
		echo view('Company/footer');

	}
}
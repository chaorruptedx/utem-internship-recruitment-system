<?php namespace App\Controllers\Company;
use App\Controllers\Company\CompanyBaseController;
use App\Models\UserModel;
use App\Models\OrganizationDetailsModel;
use App\Models\ProfilePictureModel;

class Account_Management extends CompanyBaseController
{
	public function index()
	{
        $id_user = $this->session->get('id');

        $userModel = new UserModel();
        $organizationDetailsModel = new OrganizationDetailsModel;
        $profilePictureModel = new ProfilePictureModel();

        $userModel = $userModel->getUserByID($id_user);
        $data['organization_details'] = $organizationDetailsModel->getOrganizationDetailsByIDUser($id_user);
        $data['profile_picture'] = $profilePictureModel->getProfilePictureByID($data['organization_details']['id_profile_picture']);
        
        if ($this->request->getMethod() === 'post')
		{	
            $file = $this->request->getFile('file');

            if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($id_user, $file);
            }

			$userModel = new UserModel();
			
			$validation = $userModel->editAccount($id_user, $this->request->getPost());
			
			if ($validation === true)
				$success = 'User account successfully updated.';
			else
				$errors = $validation;

            $userModel = $userModel->getUserByID($id_user);
            $data['organization_details'] = $organizationDetailsModel->getOrganizationDetailsByIDUser($id_user);
            $data['profile_picture'] = $profilePictureModel->getProfilePictureByID($data['organization_details']['id_profile_picture']);
		}
        
		echo view('Company/header');
		echo view('Company/Account_Management/index', [
            'success' => $success,
			'errors' => $errors,
            'userModel' => $userModel,
            'profile_picture' => $data['profile_picture'],
		]);
		echo view('Company/footer');
    }
}
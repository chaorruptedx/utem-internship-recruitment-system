<?php namespace App\Controllers\Company;
use App\Controllers\Company\CompanyBaseController;
use App\Models\OrganizationDetailsModel;
use App\Models\InternshipApplicationModel;

class Dashboard extends CompanyBaseController
{
	public function index()
	{
        $organizationDetailsModel = new OrganizationDetailsModel;
        $internshipApplicationModel = new InternshipApplicationModel;

        $organization_details = $organizationDetailsModel->getOrganizationDetailsByIDUser($this->session->get('id'));
        $student_application_status = $internshipApplicationModel->getStudentApplicationStatus($organization_details['id']);

		echo view('Company/header');
		echo view('Company/Dashboard/dashboard', [
            'student_application_status' => $student_application_status,
        ]);
		echo view('Company/footer');
	}
}
<?php namespace App\Controllers\Company;
use App\Controllers\Company\CompanyBaseController;
use App\Models\InternshipApplicationModel;
use App\Models\personalDetailsModel;
use App\Models\AlCourseModel;
use App\Models\AlFacultyModel;
use App\Models\UserModel;
use App\Models\ResumeModel;
use App\Models\ProfilePictureModel;
use App\Models\EducationModel;
use App\Models\SkillsModel;
use App\Models\AchievementModel;
use App\Models\ExperienceModel;
use App\Models\SupervisionModel;

class Applicants extends CompanyBaseController
{
	public function index()
	{
		$internshipApplicationModel = new InternshipApplicationModel();

		$data['internship_application'] = $internshipApplicationModel->getInternshipApplicationByIDUser($this->session->get('id'));
		
		echo view('Company/header');
		echo view('Company/Applicants/list_of_applicants', [
			'internship_application' => $data['internship_application'],
		]);
		echo view('Company/footer');
	}

	public function view_applicants_resume($id_internship_applicants, $id_personal_details)
	{
		$personalDetailsModel = new personalDetailsModel();
		$personalDetailsModel = $personalDetailsModel->getPersonalDetailsByID($id_personal_details);
		
		$id = $personalDetailsModel['id_user'];
		
		$AlCourseModel = new AlCourseModel();
		$AlCourseModel = $AlCourseModel->getCourseByID($personalDetailsModel['id_course']);

		$AlFacultyModel = new AlFacultyModel();
		$AlFacultyModel = $AlFacultyModel->getFacultyByID($personalDetailsModel['id_faculty']);

		$UserModel = new UserModel();
		$UserModel = $UserModel->getUserByID($id);
	
		$ResumeModel = new ResumeModel();
		$ResumeModel = $ResumeModel-> getResumeDetailsByIDUser($personalDetailsModel['id']);

		$profilePictureModel = new ProfilePictureModel();
		$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($personalDetailsModel['id_profile_picture']);

		$EducationModel = new EducationModel();
		$cgpa = $EducationModel->getCGPAByIDPersonalDetails($personalDetailsModel['id']);

		$skillsModel = new SkillsModel();
		$skills = $skillsModel->getSkillsByIDPersonalDetails($personalDetailsModel['id']);

		$AchievementModel = new AchievementModel();
		$achievements = $AchievementModel->getAchievementsByIDPersonalDetails($personalDetailsModel['id']);

		$ExperienceModel = new ExperienceModel();
		$experiences = $ExperienceModel->getExperiencesByIDPersonalDetails($personalDetailsModel['id']);

		$supervisionModel = new SupervisionModel();
		$coordinator = $supervisionModel->getSupervisorByIDPersonalDetails($personalDetailsModel['id']);

		$internshipApplicationModel = new InternshipApplicationModel();
		$internshipApplicationModel->updateNewApplicantStatus($id_internship_applicants);
		$internship_application = $internshipApplicationModel->getInternshipApplicationByID($id_internship_applicants);

		echo view('Company/header');
		echo view('Company/Applicants/view_applicants_resume', [
			'personalDetailsModel' => $personalDetailsModel,
			'UserModel' => $UserModel,
			'AlCourseModel' => $AlCourseModel,
			'AlFacultyModel' => $AlFacultyModel,
			'ResumeModel' => $ResumeModel,
			'profile_picture' => $data['profile_picture'],
			'cgpa' => $cgpa,
			'skills' => $skills,
			'achievements' => $achievements,
			'experiences' => $experiences,
			'coordinator' => $coordinator,
			'id_internship_applicants' => $id_internship_applicants,
			'internship_application' => $internship_application,
		]);
		echo view('Company/footer');
	}

	public function approve_applicant($id_internship_applicants, $id_personal_details)
	{
		$internshipApplicationModel = new InternshipApplicationModel();
		$internshipApplicationModel->updateInternshipStatus($id_internship_applicants, 3);

		return redirect()->to(base_url('public/company/applicants/view_applicants_resume/'.$id_internship_applicants.'/'.$id_personal_details));
	}

	public function reject_applicant($id_internship_applicants, $id_personal_details)
	{
		$internshipApplicationModel = new InternshipApplicationModel();
		$internshipApplicationModel->updateInternshipStatus($id_internship_applicants, 0);
		$internshipApplicationModel->updateInternshipNotes($id_internship_applicants, $this->request->getPost());

		return redirect()->to(base_url('public/company/applicants/view_applicants_resume/'.$id_internship_applicants.'/'.$id_personal_details));
	}
}
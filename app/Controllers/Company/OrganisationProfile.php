<?php namespace App\Controllers\Company;
use App\Controllers\Company\CompanyBaseController;
use App\Models\OrganizationDetailsModel;
use App\Models\AlStateModel;
use App\Models\UserModel;
use App\Models\ProfilePictureModel;

class OrganisationProfile extends CompanyBaseController
{
	
	public function index()
	{
		$id = $this->session->get('id');
		
		$organizationDetailsModel = new OrganizationDetailsModel();
		$organizationDetailsModel = $organizationDetailsModel->getOrganizationDetailsByIDUser($id);
		
		$AlStateModel = new AlStateModel();
		$AlStateModel = $AlStateModel->getStateByID($organizationDetailsModel['id_state']);

		$UserModel = new UserModel();
		$UserModel = $UserModel->getUserByID($id);

		$profilePictureModel = new ProfilePictureModel();
		$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($organizationDetailsModel['id_profile_picture']);

		echo view('Company/header');
		echo view('Company/profile/view_profile', [
			'organizationDetailsModel' => $organizationDetailsModel,
			'AlStateModel' => $AlStateModel,
			'UserModel' => $UserModel,
			'profile_picture' => $data['profile_picture'],
		]);
		echo view('Company/footer');

		
	}

	public function create_profile()
	{
		if ($this->request->getMethod() === 'post')
		{
			$organizationDetailsModel = new OrganizationDetailsModel();
			$profilePictureModel = new ProfilePictureModel();

			$id_user=$this->session->get('id');
			$validation = $organizationDetailsModel->createProfile($id_user,$this->request->getPost());

			$file = $this->request->getFile('file');

            if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($id_user, $file);
            }
			
			if ($validation === true)
				$success = 'Profile Successfully Created.';
			else
				$errors = $validation;
		}

		$AlStateModel = new AlStateModel();
		$AlStateModel = $AlStateModel->getState();

		
		echo view('Company/header');
		echo view('Company/profile/create_profile', [
			'success' => $success,
			'errors' => $errors,
			'AlStateModel' => $AlStateModel,
		]);
		echo view('Company/footer');
	}

	public function edit_profile($id)
	{
		$organizationDetailsModel = new OrganizationDetailsModel();

		$organization_data = $organizationDetailsModel->getOrganizationByID($id); // original data

		$profilePictureModel = new ProfilePictureModel();
		$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($organization_data['id_profile_picture']);

		if ($this->request->getMethod() === 'post')
		{	
			$validation = $organizationDetailsModel->editCompanyProfile($id, $this->request->getPost());
			$organization_data = $organizationDetailsModel->getOrganizationByID($id); //replace data lama dengan yg baru (updated)

			$file = $this->request->getFile('file');

            if ($file->getClientName() != null)
            {
                $profilePictureModel->uploadProfilePicture($this->session->get('id'), $file);
			}

			$organization_data = $organizationDetailsModel->getOrganizationByID($id); // original data
			
			$profilePictureModel = new ProfilePictureModel();
			$data['profile_picture'] = $profilePictureModel->getProfilePictureByID($organization_data['id_profile_picture']);
			
			if ($validation === true)
				$success = 'Organization information successfully updated.';
			else
				$errors = $validation;

		}
		$AlStateModel = new AlStateModel();
		$AlStateModel = $AlStateModel->getState();
		
		echo view('Company/header');
		echo view('Company/profile/edit_profile', [
			'success' => $success,
			'errors' => $errors,
			'organizationDetailsModel' => $organization_data, //dia hantar data view, view akan data kat warna oren
			'AlStateModel' => $AlStateModel,
			'profile_picture' => $data['profile_picture'],
		]);
		echo view('Company/footer');
	}

}
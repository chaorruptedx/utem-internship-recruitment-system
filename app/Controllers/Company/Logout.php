<?php namespace App\Controllers\Company;
use App\Controllers\Company\CompanyBaseController;

class Logout extends CompanyBaseController
{
	public function index()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('public/'));
	}
}
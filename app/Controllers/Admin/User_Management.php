<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\UserModel;
use App\Models\PersonalDetailsModel;
use App\Models\AlCourseModel;

class User_Management extends AdminBaseController
{
	public function index()
	{
		$userModel = new UserModel();
		$data['user'] = $userModel->getUser();

		echo view('Admin/header');
		echo view('Admin/User_Management/index', [
			'user' => $data['user']
		]);
		echo view('Admin/footer');
	}

	public function register_user()
	{
		$alCourseModel = new AlCourseModel();

		$data['course'] = $alCourseModel->getCourse();

		if ($this->request->getMethod() === 'post')
		{
			$userModel = new UserModel();
			$personalDetailsModel = new PersonalDetailsModel();

			$validation = $userModel->registerUser($this->request->getPost());
			
			if ($validation === true)
				$success = 'User successfully registered into the system.';
			else
				$errors = $validation;
		}
		
		echo view('Admin/header');
		echo view('Admin/User_Management/register_user', [
			'success' => $success,
			'errors' => $errors,
			'course' => $data['course'],
		]);
		echo view('Admin/footer');
	}

	public function view_user($id)
	{
		$userModel = new UserModel();
		$personalDetailsModel = new PersonalDetailsModel();
		$alCourseModel = new AlCourseModel();

		$user_details = $userModel->getUserByID($id);
		$data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($user_details['id']);

		echo view('Admin/header');
		echo view('Admin/User_Management/view_user', [
			'user_details' => $user_details,
			'personal_details' => $data['personal_details'],
		]);
		echo view('Admin/footer');
	}

	public function edit_user($id)
	{
		$userModel = new UserModel();
		$personalDetailsModel = new PersonalDetailsModel();
		$alCourseModel = new AlCourseModel();

		$user_details = $userModel->getUserByID($id);

		$data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($user_details['id']);

		$data['course'] = $alCourseModel->getCourse();

		if ($this->request->getMethod() === 'post')
		{
			$validation = $userModel->editUser($id, $this->request->getPost());
			$data['personal_details'] = $personalDetailsModel->getPersonalDetailsByIDUser($user_details['id']);
			
			if ($validation === true)
				$success = 'User information successfully updated.';
			else
				$errors = $validation;

			$user_details = $userModel->getUserByID($id);
		}
		
		echo view('Admin/header');
		echo view('Admin/User_Management/edit_user', [
			'success' => $success,
			'errors' => $errors,
			'userModel' => $user_details,
			'personal_details' => $data['personal_details'],
			'course' => $data['course'],
		]);
		echo view('Admin/footer');
	}

	public function remove_user($id)
	{
		$userModel = new UserModel();

		$userModel->removeUser($id);
		
		return redirect()->to(base_url('public/admin/user_management'));
	}
}
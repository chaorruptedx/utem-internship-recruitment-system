<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\AlCourseModel;

class Course_Management extends AdminBaseController
{
	public function index()
	{
		$alCourseModel = new AlCourseModel();
		$data['course'] = $alCourseModel->getCourse();

		echo view('Admin/header');
		echo view('Admin/Course_Management/index', [
			'course' => $data['course']
		]);
		echo view('Admin/footer');
	}

	public function register_course()
	{
		if ($this->request->getMethod() === 'post')
		{
			$alCourseModel = new AlCourseModel();

			$validation = $alCourseModel->registerCourse($this->request->getPost());
			
			if ($validation === true)
				$success = 'Course successfully registered into the system.';
			else
				$errors = $validation;
		}
		
		echo view('Admin/header');
		echo view('Admin/Course_Management/register_course', [
			'success' => $success,
			'errors' => $errors,
		]);
		echo view('Admin/footer');
	}

	public function edit_course($id)
	{
		$alCourseModel = new AlCourseModel();

		$alCourseModel = $alCourseModel->getCourseByID($id);

		if ($this->request->getMethod() === 'post')
		{	
			$alCourseModel = new AlCourseModel();
			
			$validation = $alCourseModel->editCourse($id, $this->request->getPost());
			
			if ($validation === true)
				$success = 'Course details successfully updated.';
			else
				$errors = $validation;

			$alCourseModel = $alCourseModel->getCourseByID($id);
		}
		
		echo view('Admin/header');
		echo view('Admin/Course_Management/edit_course', [
			'success' => $success,
			'errors' => $errors,
			'alCourseModel' => $alCourseModel,
		]);
		echo view('Admin/footer');
	}

	public function remove_course($id)
	{
		$alCourseModel = new AlCourseModel();

		$alCourseModel->removeCourse($id);
		
		return redirect()->to(base_url('public/admin/course_management'));
	}
}
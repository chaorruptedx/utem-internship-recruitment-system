<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;

class Logout extends AdminBaseController
{
	public function index()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('public/'));
	}
}
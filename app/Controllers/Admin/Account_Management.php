<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\UserModel;

class Account_Management extends AdminBaseController
{
	public function index()
	{
        $id_user = $this->session->get('id');

        $userModel = new UserModel();

		$userModel = $userModel->getUserByID($id_user);
        
        if ($this->request->getMethod() === 'post')
		{
			$userModel = new UserModel();
			
			$validation = $userModel->editAccount($id_user, $this->request->getPost());
			
			if ($validation === true)
				$success = 'User account successfully updated.';
			else
				$errors = $validation;

			$userModel = $userModel->getUserByID($id_user);
		}
        
		echo view('Admin/header');
		echo view('Admin/Account_Management/index', [
            'success' => $success,
			'errors' => $errors,
            'userModel' => $userModel,
		]);
		echo view('Admin/footer');
    }
}
<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\AlLanguageModel;

class Language_Management extends AdminBaseController
{
	public function index()
	{
		$alLanguageModel = new AlLanguageModel();
		$data['language'] = $alLanguageModel->getLanguage();

		echo view('Admin/header');
		echo view('Admin/Language_Management/index', [
			'language' => $data['language']
		]);
		echo view('Admin/footer');
	}

	public function add_language()
	{
		if ($this->request->getMethod() === 'post')
		{
			$alLanguageModel = new AlLanguageModel();

			$validation = $alLanguageModel->addLanguage($this->request->getPost());
			
			if ($validation === true)
				$success = 'Language successfully added into the system.';
			else
				$errors = $validation;
		}
		
		echo view('Admin/header');
		echo view('Admin/Language_Management/add_language', [
			'success' => $success,
			'errors' => $errors,
		]);
		echo view('Admin/footer');
	}

	public function edit_language($id)
	{
		$alLanguageModel = new AlLanguageModel();

		$alLanguageModel = $alLanguageModel->getLanguageByID($id);

		if ($this->request->getMethod() === 'post')
		{	
			$alLanguageModel = new AlLanguageModel();
			
			$validation = $alLanguageModel->editLanguage($id, $this->request->getPost());
			
			if ($validation === true)
				$success = 'Language details successfully updated.';
			else
				$errors = $validation;

			$alLanguageModel = $alLanguageModel->getLanguageByID($id);
		}
		
		echo view('Admin/header');
		echo view('Admin/Language_Management/edit_language', [
			'success' => $success,
			'errors' => $errors,
			'alLanguageModel' => $alLanguageModel,
		]);
		echo view('Admin/footer');
	}

	public function remove_language($id)
	{
		$alLanguageModel = new AlLanguageModel();

		$alLanguageModel->removeLanguage($id);
		
		return redirect()->to(base_url('public/admin/language_management'));
	}
}
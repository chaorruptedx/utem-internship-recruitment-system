<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\UserModel;
use App\Models\PersonalDetailsModel;

class Dashboard extends AdminBaseController
{
	public function index()
	{
		$userModel = new UserModel();
		$personalDetailsModel = new PersonalDetailsModel;

		$data['number_of_user'] = $userModel->getNumberOfUser();
		$data['student_supervision_status'] = $personalDetailsModel->getStudentSupervisionStatus();
		
		if ($data['student_under_programme'] = $personalDetailsModel->getStudentUnderProgramme())
		{
			$student_under_programme['number_of_course'][] = 'Programme';

			foreach ($data['student_under_programme'] as $course_code => $number_of_course)
			{
				$student_under_programme['course_code'][] = $course_code;
				$student_under_programme['number_of_course'][] = $number_of_course;
			}
		}
		else
		{
			$student_under_programme['course_code'][] = 'Programme';
			$student_under_programme['number_of_course'][] = 'Programme';
			$student_under_programme['number_of_course'][] = 0;
		}

		echo view('Admin/header');
		echo view('Admin/Dashboard/dashboard', [
			'number_of_user' => $data['number_of_user'],
		]);
		echo view('Admin/footer', [
			'student_supervision_status' => $data['student_supervision_status'],
			'student_under_programme' => $student_under_programme,
		]);
	}
}
<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\AnnouncementModel;

class Announcement extends AdminBaseController
{
	public function index()
	{
        $announcementModel = new AnnouncementModel();
        $data['announcement'] = $announcementModel->getAnnouncement();
        
		echo view('Admin/header');
		echo view('Admin/Announcement/index', [
			'announcement' => $data['announcement']
		]);
		echo view('Admin/footer');
    }
    
    public function create_announcement()
	{
		if ($this->request->getMethod() === 'post')
		{
			$announcementModel = new AnnouncementModel();

			$validation = $announcementModel->createAnnouncement($this->session->get('id'), $this->request->getPost());
			
			if ($validation === true)
				$success = 'Announcement successfully created.';
			else
				$errors = $validation;
		}
		
		echo view('Admin/header');
		echo view('Admin/Announcement/create_announcement', [
			'success' => $success,
			'errors' => $errors,
		]);
		echo view('Admin/footer');
    }

    public function view_announcement($id)
	{
		$announcementModel = new AnnouncementModel();

        $announcementModel = $announcementModel->getAnnouncementByID($id);
		
		echo view('Admin/header');
		echo view('Admin/Announcement/view_announcement', [
			'announcementModel' => $announcementModel,
		]);
		echo view('Admin/footer');
	}
    
    public function edit_announcement($id)
	{
		$announcementModel = new AnnouncementModel();

        $announcementModel = $announcementModel->getAnnouncementByID($id);
        
		if ($this->request->getMethod() === 'post')
		{
			$announcementModel = new AnnouncementModel();
			
			$validation = $announcementModel->editAnnouncement($id, $this->session->get('id'), $this->request->getPost());
			
			if ($validation === true)
				$success = 'Announcement successfully updated.';
			else
				$errors = $validation;

			$announcementModel = $announcementModel->getAnnouncementByID($id);
		}
		
		echo view('Admin/header');
		echo view('Admin/Announcement/edit_announcement', [
			'success' => $success,
			'errors' => $errors,
			'announcementModel' => $announcementModel,
		]);
		echo view('Admin/footer');
    }
    
    public function remove_announcement($id)
	{
		$announcementModel = new AnnouncementModel();

		$announcementModel->removeAnnouncement($id);
		
		return redirect()->to(base_url('public/admin/announcement'));
	}
}
<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\SupervisionModel;
use App\Models\UserModel;
use App\Models\PersonalDetailsModel;

class Supervision extends AdminBaseController
{
	public function index()
	{
		$personalDetailsModel = new PersonalDetailsModel();
		$data['coordinator'] = $personalDetailsModel->getCoordinator();
		
		echo view('Admin/header');
		echo view('Admin/Supervision/index', [
			'coordinator' => $data['coordinator']
		]);
		echo view('Admin/footer');
	}

	public function supervision($id_personal_details)
	{
		$personalDetailsModel = new PersonalDetailsModel();
		$supervisionModel = new SupervisionModel();
		$data['coordinator'] = $personalDetailsModel->getCoordinatorByID($id_personal_details);
		$data['supervisee'] = $supervisionModel->getSuperviseeByIDSupervisor($id_personal_details);

		echo view('Admin/header');
		echo view('Admin/Supervision/supervision', [
			'coordinator' => $data['coordinator'],
			'student' => $data['supervisee'],
		]);
		echo view('Admin/footer');
	}

	public function assign_student($id_personal_detail)
	{
		$supervisionModel = new SupervisionModel();
		$personalDetailsModel = new PersonalDetailsModel();
		$data['supervisor'] = $personalDetailsModel->getCoordinatorByID($id_personal_detail);
		$data['student'] = $personalDetailsModel->getStudentSupervision($id_personal_detail);
		
		if ($this->request->getMethod() === 'post')
		{
			$supervisionModel->assignStudentSupervisor($id_personal_detail, $this->request->getPost('student_status'));

			$data['student'] = $personalDetailsModel->getStudentSupervision($id_personal_detail);
		}

		echo view('Admin/header');
		echo view('Admin/Supervision/assign_student', [
			'supervisor' => $data['supervisor'],
			'student' => $data['student'],
		]);
		echo view('Admin/footer');
	}
}
<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\AlStateModel;

class State_Management extends AdminBaseController
{
	public function index()
	{
		$alStateModel = new AlStateModel();
		$data['state'] = $alStateModel->getState();

		echo view('Admin/header');
		echo view('Admin/State_Management/index', [
			'state' => $data['state']
		]);
		echo view('Admin/footer');
	}

	public function add_state()
	{
		if ($this->request->getMethod() === 'post')
		{
			$alStateModel = new AlStateModel();

			$validation = $alStateModel->addState($this->request->getPost());
			
			if ($validation === true)
				$success = 'State successfully added into the system.';
			else
				$errors = $validation;
		}
		
		echo view('Admin/header');
		echo view('Admin/State_Management/add_state', [
			'success' => $success,
			'errors' => $errors,
		]);
		echo view('Admin/footer');
	}

	public function edit_state($id)
	{
		$alStateModel = new AlStateModel();

		$alStateModel = $alStateModel->getStateByID($id);

		if ($this->request->getMethod() === 'post')
		{	
			$alStateModel = new AlStateModel();
			
			$validation = $alStateModel->editState($id, $this->request->getPost());
			
			if ($validation === true)
				$success = 'State details successfully updated.';
			else
				$errors = $validation;

			$alStateModel = $alStateModel->getStateByID($id);
		}
		
		echo view('Admin/header');
		echo view('Admin/State_Management/edit_state', [
			'success' => $success,
			'errors' => $errors,
			'alStateModel' => $alStateModel,
		]);
		echo view('Admin/footer');
	}

	public function remove_state($id)
	{
		$alStateModel = new AlStateModel();

		$alStateModel->removeState($id);
		
		return redirect()->to(base_url('public/admin/state_management'));
	}
}
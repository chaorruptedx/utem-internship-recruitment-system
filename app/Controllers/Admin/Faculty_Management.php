<?php namespace App\Controllers\Admin;
use App\Controllers\Admin\AdminBaseController;
use App\Models\AlFacultyModel;

class Faculty_Management extends AdminBaseController
{
	public function index()
	{
		$alFacultyModel = new AlFacultyModel();
		$data['faculty'] = $alFacultyModel->getFaculty();

		echo view('Admin/header');
		echo view('Admin/Faculty_Management/index', [
			'faculty' => $data['faculty']
		]);
		echo view('Admin/footer');
	}

	public function register_faculty()
	{
		if ($this->request->getMethod() === 'post')
		{
			$alFacultyModel = new AlFacultyModel();

			$validation = $alFacultyModel->registerFaculty($this->request->getPost());
			
			if ($validation === true)
				$success = 'Faculty details registered into the system.';
			else
				$errors = $validation;
		}
		
		echo view('Admin/header');
		echo view('Admin/Faculty_Management/register_faculty', [
			'success' => $success,
			'errors' => $errors,
		]);
		echo view('Admin/footer');
	}

	public function edit_faculty($id)
	{
		$alFacultyModel = new AlFacultyModel();

		$alFacultyModel = $alFacultyModel->getFacultyByID($id);

		if ($this->request->getMethod() === 'post')
		{	
			$alFacultyModel = new AlFacultyModel();
			
			$validation = $alFacultyModel->editFaculty($id, $this->request->getPost());
			
			if ($validation === true)
				$success = 'Faculty details successfully updated.';
			else
				$errors = $validation;

			$alFacultyModel = $alFacultyModel->getFacultyByID($id);
		}
		
		echo view('Admin/header');
		echo view('Admin/Faculty_Management/edit_faculty', [
			'success' => $success,
			'errors' => $errors,
			'alFacultyModel' => $alFacultyModel,
		]);
		echo view('Admin/footer');
	}

	public function remove_faculty($id)
	{
		$alFacultyModel = new AlFacultyModel();

		$alFacultyModel->removeFaculty($id);
		
		return redirect()->to(base_url('public/admin/faculty_management'));
	}
}
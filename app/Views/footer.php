<br>
<br>
<footer>
	<div class="copyrights">
		<p>&copy; <?= date('Y') ?> UTeM Internship Recruitment System</p>
	</div>
</footer>

<script src="<?php echo base_url('public/assets/bootstrap/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

</body>

</html>

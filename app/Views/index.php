<div class="container">
    <br>
    <h1 class="text-center">Welcome to UTeM Internship Recruitment System</h1>
    <br>
	<br>

    <!-- <div class="row">

		<div class="col-lg-3">
		&nbsp;
		</div>

        <div class="col-lg-6" style="background-color: lightblue;">

			<h1>Login</h1>

   			<p>Please fill out the following fields to login:</p>
			
			<label class="control-label" for="userlogin-email">Email</label>
			<input type="text" id="userlogin-email" class="form-control" name="UserLogin[email]" autofocus="" aria-required="true" aria-invalid="true">

			<div class="form-group field-userlogin-password required has-success">
				<label class="control-label" for="userlogin-password">Password</label>
				<input type="password" id="userlogin-password" class="form-control" name="UserLogin[password]" value="" aria-required="true" aria-invalid="false">
			</div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary" name="login-button">Login</button>
			</div>
		</div>

		<div class="col-lg-3">
		&nbsp;
		</div>

	</div>

    <br> -->

	<center>
		<div class="wrap-login100">
			<div class="login100-form-title" style="background-image: url(<?= base_url('public/background/Login Background.jpg'); ?>);">
				<span class="login100-form-title-1">Sign In</span>
			</div>
			<form action="<?= base_url('public/Home/index');?>" method="post" class="login100-form validate-form">
				<?= csrf_field() ?>
				<?php if (!empty($error)) : ?>
					<div class="alert alert-danger" role="alert">
							<?= $error ?>
					</div>
				<?php endif ?>
				<div class="wrap-input100 validate-input m-b-26" data-validate="Email is required">
					<span class="label-input100">Email</span>
					<input class="input100" type="email" name="email" placeholder="Enter Email">
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">
					<span class="label-input100">Password</span>
					<input class="input100" type="password" name="password" placeholder="Enter Password">
					<span class="focus-input100"></span>
				</div>
				<!-- <div class="flex-sb-m w-full p-b-30">
					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">Remember me</label>
					</div>
					<div>
						<a href="#" class="txt1">Forgot Password?</a>
					</div>
				</div> -->
				<div class="container-login100-form-btn">
					<button class="login100-form-btn">Login</button>
				</div>
			</form>
		</div>
	<center>
</div>


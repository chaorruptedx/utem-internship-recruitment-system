    <div class="page-header">
        <br><h1 class="text-center">Student Applicant List</h1> </br>
    </div>

    <div class="container">
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Student Name</th>
                    <th>Course</th>
                    <th>Current Approval Status</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($internship_application) && is_array($internship_application)) : ?>
                    <?php foreach ($internship_application as $internship_application_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$x; ?></td>
                            <td><?= esc($internship_application_data['student_name']); ?></td>
                            <td><?= esc($internship_application_data['course_name']); ?></td>
                            <td class="<?= esc(application_status_organization($internship_application_data['application_status'], 2)); ?>"><?= esc(application_status_organization($internship_application_data['application_status'], 1)); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/company/applicants/view_applicants_resume/'.$internship_application_data['id'].'/'.$internship_application_data['id_personal_details']);?>"><span class="fas fa-eye"></span></a>
                                &nbsp;
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>

<div class="container">
    <div class="card">
        <div class="card-body">
            <h3>Status Flow :</h3>                 
            <p class="text-center"><span class="text-success">New</span> <span class="fas fa-angle-right"></span> <span class="text-warning">In Review</span> <span class="fas fa-angle-right"></span> <span class="text-info">Awaiting Student Reply</span> <span class="fas fa-angle-right"></span> <span class="text-primary">Student Accepted</span></p>
        </div>
    </div>
</div>


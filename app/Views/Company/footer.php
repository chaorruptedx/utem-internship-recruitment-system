<br>
<br>
<footer>
	<div class="copyrights">
		<p>&copy; <?= date('Y') ?> UTeM Internship Recruitment System</p>
	</div>
</footer>

<script src="<?php echo base_url('public/assets/bootstrap/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/select2/js/select2.full.min.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#data-table').DataTable();
		var element = document.getElementById("data-table");
  		if (element !== null)
  			element.classList.add("table-striped", "table-hover", "table-dark", "text-center");

		$('select').select2();
	} );
</script>

</body>

</html>

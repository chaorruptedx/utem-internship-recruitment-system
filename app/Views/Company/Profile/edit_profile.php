<br><div class="page-header">
    <h1 align="center">EDIT PROFILE</h1> <br><br>
  </div>

  <div class="container">

  <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    
    <div class="main-body">    
            <form action="<?= base_url('public/company/organisationprofile/edit_profile/'.$organizationDetailsModel['id']);?>" method="post" enctype="multipart/form-data">
                <?= csrf_field() ?>
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <!-- <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150"> -->
                    <img src="<?= ($profile_picture != null) ? base_url('public/'.$profile_picture['path'].$profile_picture['name']) : base_url('public/profile_picture/default_profile_picture.jpg'); ?>" alt="Profile Picture" class="mx-auto rounded" style="max-width: 200px; max-height: 200px;">
                    <div class="mt-3">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="customFile" accept="image/*">
                        <label class="custom-file-label" for="customFile">Profile Picture</label>
                    </div><br><br>
                      <h4><input type="text" name="name" value="<?= $organizationDetailsModel['name'];  ?>" placeholder="Organization Name" class="form-control"/></h4>
                    
                        <select name="type" class="form-control">
                            <option value="" selected disabled>Choose Sector ...</option>
                            <option value="1" <?= ($organizationDetailsModel['type'] == 1) ? 'selected' : ''; ?>>Goverment Sector</option>
                            <option value="2" <?= ($organizationDetailsModel['type'] == 2) ? 'selected' : ''; ?>>Private Scetor</option>
                        </select>  
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe mr-2 icon-inline"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>Website</h6>
                    <span class="text-secondary">https://mycompany.com</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram mr-2 icon-inline text-danger"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>Instagram</h6>
                    <span class="text-secondary">noig</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook mr-2 icon-inline text-primary"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>Facebook</h6>
                    <span class="text-secondary">facebook</span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Address</h6>
                            </div>
                                <div class="col-sm-9 ">
                                <input type="text" name="address" value="<?= $organizationDetailsModel['address'];?>"  placeholder="Enter Organization Address" class="form-control">
                                </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">State</h6>
                            </div>
                            <div class="col-sm-9 ">
                                <select name="al_state" class="form-control" >
                                    <option value="" selected disabled>Choose State ...</option>
                                        <?php foreach ( $AlStateModel  as $state_data) : ?>
                                            <option value="<?= $state_data['id'] ?>" <?php if ($state_data['id'] == $organizationDetailsModel['id_state']) echo 'selected'; ?>><?=$state_data['name']?></option>
                                        <?php endforeach; ?>
                                </select>
                                
                            </div>
                        </div>
                        <!--hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary ">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 ">
                            <?= $UserModel['email']; ?>
                            </div>
                        </div-->
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Contact Number</h6>
                            </div>
                            <div class="col-sm-9 ">
                            <input type="number" name="tel_no" value="<?= $organizationDetailsModel['tel_no'];?>" placeholder="Contact Number" class="form-control"/>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Working Days</h6>
                            </div>
                            <div class="col-sm-9 ">
                            <label class="font-weight-bold"> Start Day</label>
                            <select name="start_day" class="form-control">
                                <option value="" selected disabled>Choose day ...</option>
                                <option value="0" <?php if ($organizationDetailsModel['start_day'] == '0') echo 'selected'; ?>>Monday</option>
                                <option value="1" <?php if ($organizationDetailsModel['start_day'] == '1') echo 'selected'; ?>>Tuesday</option>
                                <option value="2" <?php if ($organizationDetailsModel['start_day'] == '2') echo 'selected'; ?>>Wednesday</option>
                                <option value="3" <?php if ($organizationDetailsModel['start_day'] == '3') echo 'selected'; ?>>Thursday</option>
                                <option value="4" <?php if ($organizationDetailsModel['start_day'] == '4') echo 'selected'; ?>>Friday</option>
                                <option value="5" <?php if ($organizationDetailsModel['start_day'] == '5') echo 'selected'; ?>>Saturday</option>
                                <option value="6" <?php if ($organizationDetailsModel['start_day'] == '6') echo 'selected'; ?>>Sunday</option>
                            </select>
                        
                            <br><label class="font-weight-bold"> Close Day</label><br>
                            <select name="end_day" class="form-control">
                                <option value="" selected disabled>Choose day ...</option>
                                <option value="0" <?php if ($organizationDetailsModel['end_day'] == '0') echo 'selected'; ?>>Monday</option>
                                <option value="1" <?php if ($organizationDetailsModel['end_day'] == '1') echo 'selected'; ?>>Tuesday</option>
                                <option value="2" <?php if ($organizationDetailsModel['end_day'] == '2') echo 'selected'; ?>>Wednesday</option>
                                <option value="3" <?php if ($organizationDetailsModel['end_day'] == '3') echo 'selected'; ?>>Thursday</option>
                                <option value="4" <?php if ($organizationDetailsModel['end_day'] == '4') echo 'selected'; ?>>Friday</option>
                                <option value="5" <?php if ($organizationDetailsModel['end_day'] == '5') echo 'selected'; ?>>Saturday</option>
                                <option value="6" <?php if ($organizationDetailsModel['end_day'] == '6') echo 'selected'; ?>>Sunday</option>
                            </select>
                            </div>
                            
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Working Hours</h6>
                            </div>
                            <div class="col-sm-9">
                            <label class="font-weight-bold"> Open </label>
                            <input type="time" class="form-control" name="open_hour" value="<?= $organizationDetailsModel['open_hour'];?>"><br>
                            <label class="font-weight-bold"> Close </label>
                            <input type="time" class="form-control" name="close_hour" value="<?= $organizationDetailsModel['close_hour'];?>"></br>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Job Description</h6>
                            </div>
                            <div class="col-sm-9">
                            <textarea class="form-control" rows="4" name="job_description" ><?= $organizationDetailsModel['job_description'];?></textarea>
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br><div class="text-right btn-form form-group">
      <button type="submit" name="submit" class="btn btn-primary float-right" >Update Profile</button>
     </div>
     <div class="btn-form form-group">
        <a href="<?= base_url('public/company/organisationprofile');?>" class="btn btn-secondary">Back</a>
     </div>
        
    </div>
</div>
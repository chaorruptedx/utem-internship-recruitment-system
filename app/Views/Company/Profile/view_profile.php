<br><div class="page-header">
    <h1 align="center">ORGANIZATION PROFILE</h1> <br><br>
  </div>

  <div class="container">
    <div class="main-body">    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <!-- <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150"> -->
                    <img src="<?= ($profile_picture != null) ? base_url('public/'.$profile_picture['path'].$profile_picture['name']) : base_url('public/profile_picture/default_profile_picture.jpg'); ?>" alt="Profile Picture" class="mx-auto rounded" style="max-width: 200px; max-height: 200px;">
                    <div class="mt-3">
                      <h4><?= $organizationDetailsModel['name']; ?></h4>
                      <p class="text-secondary mb-1"><?php
                        if (esc($organizationDetailsModel['type']) == '1') :
                            echo 'Government';
                        elseif (esc($organizationDetailsModel['type']) == '2') :
                            echo 'Private';
                        endif;
                        ?>
                        </p>
                    
                      <!-- <button class="btn btn-primary">Apply Internship</button> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe mr-2 icon-inline"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>Website</h6>
                    <span class="text-secondary">https://mycompany.com</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram mr-2 icon-inline text-danger"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>Instagram</h6>
                    <span class="text-secondary">noig</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook mr-2 icon-inline text-primary"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>Facebook</h6>
                    <span class="text-secondary">facebook</span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Address</h6>
                            </div>
                                <div class="col-sm-9 ">
                                <?= $organizationDetailsModel['address']; ?>
                                </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">State</h6>
                            </div>
                                <div class="col-sm-9 ">
                                <?= $AlStateModel['name']; ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary ">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 ">
                            <?= $UserModel['email']; ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Contact Number</h6>
                            </div>
                            <div class="col-sm-9 ">
                            <?= $organizationDetailsModel['tel_no']; ?>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Working Days</h6>
                            </div>
                            <div class="col-sm-9 ">
                            
                            <?php
                                if (esc($organizationDetailsModel['start_day']) == '0') :
                                echo 'Monday';
                                elseif (esc($organizationDetailsModel['start_day']) == '1') :
                                echo 'Tuesday';
                                elseif (esc($organizationDetailsModel['start_day']) == '2') :
                                echo 'Wednesday';
                                elseif (esc($organizationDetailsModel['start_day']) == '3') :
                                echo 'Thursday';
                                elseif (esc($organizationDetailsModel['start_day']) == '4') :
                                echo 'Friday';
                                elseif (esc($organizationDetailsModel['start_day']) == '5') :
                                echo 'Saturday';
                                elseif (esc($organizationDetailsModel['start_day']) == '6') :
                                echo 'Sunday';
                                endif;
                            ?>
                            <strong> To </strong>
                            <?php
                                if (esc($organizationDetailsModel['end_day']) == '0') :
                                echo 'Monday';
                                elseif (esc($organizationDetailsModel['end_day']) == '1') :
                                echo 'Tuesday';
                                elseif (esc($organizationDetailsModel['end_day']) == '2') :
                                echo 'Wednesday';
                                elseif (esc($organizationDetailsModel['end_day']) == '3') :
                                echo 'Thursday';
                                elseif (esc($organizationDetailsModel['end_day']) == '4') :
                                echo 'Friday';
                                elseif (esc($organizationDetailsModel['end_day']) == '5') :
                                echo 'Saturday';
                                elseif (esc($organizationDetailsModel['end_day']) == '6') :
                                echo 'Sunday';
                                endif;
                            ?>
                            </div>
                            
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Working Hours</h6>
                            </div>
                            <div class="col-sm-9">
                            <strong>Open<br></strong> <?= date("g:i A", strtotime($organizationDetailsModel['open_hour'])); ?><br>
                            <strong>Close<br></strong><?= date("g:i A", strtotime($organizationDetailsModel['close_hour'])); ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 text-primary">
                                <h6 class="mb-0">Job Description</h6>
                            </div>
                            <div class="col-sm-9">
                            <?= $organizationDetailsModel['job_description']; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="btn-block pull-right">
      <a href="<?= base_url('public/company/organisationprofile/edit_profile/'.$organizationDetailsModel['id']);?>" class="btn btn-secondary pull-right" style="float: right;">Edit Profile</a>
    </div>
        
    </div>
</div>
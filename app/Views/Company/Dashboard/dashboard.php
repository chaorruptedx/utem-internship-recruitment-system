<div class="container">
    <br>
    <h1 class="text-center">Dashboard</h1>
    <br>

    <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-sm-12">
                    <h3 class="text-center">Student Internship Application</h3>
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading green"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content green">
                                    <div class="circle-tile-description text-faded">New</div>
                                    <div class="circle-tile-number text-faded "><?= $student_application_status['new']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/company/applicants');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading orange"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content orange">
                                    <div class="circle-tile-description text-faded">In Review</div>
                                    <div class="circle-tile-number text-faded "><?= $student_application_status['in_review']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/company/applicants');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading dark-blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content dark-blue">
                                    <div class="circle-tile-description text-faded">Awaiting Student Reply</div>
                                    <div class="circle-tile-number text-faded "><?= $student_application_status['awaiting_student_reply']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/company/applicants');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content blue">
                                    <div class="circle-tile-description text-faded">Student Accepted</div>
                                    <div class="circle-tile-number text-faded "><?= $student_application_status['student_accepted']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/company/applicants');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

    <br>
</div>
<div class="container">
    <br>
    <h1 class="text-center">User Account Details</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="card">
        <div class="card-body">

    <form action="<?= base_url('public/admin/account_management/index/') ?>" method="post">
        <?= csrf_field() ?>

        <div class="form-group">
            <label for="email" class="font-weight-bold">E-mail</label>
            <input type="email" name="email" value="<?= $userModel['email']; ?>" placeholder="Enter E-mail" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="email" class="font-weight-bold">New Password</label>
            <input type="password" name="password" placeholder="Enter New Password" class="form-control"/>
        </div>

        </div>
    </div>
    <br>

        <div class="text-right btn-form form-group">
            <button type="submit" name="submit" class="btn btn-primary"><span class="fas fa-check"></span>&nbsp;&nbsp;Update Account</button>
        </div>
    </form>

    <br>
</div>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>UTeM Internship Recruitment System</title>
	<style>
		body {
			background-image: url("<?php echo base_url('public/background/Background.jpg'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
		.copyrights {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			background-color: #212529;
			color: #FFFFFF80;
			text-align: center;
		}
	</style>
    <link rel="stylesheet" href="<?php echo base_url('public/assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/datatables/datatables.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/dashboard/admin/dashboard.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/c3/c3.min.css'); ?>">
</head>

<body>

<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<div class="container">
			<ul class="navbar-nav">
				<a class="navbar-brand" href="#">
					<img src="<?= base_url('public/logo/Universiti Teknikal Malaysia Melaka (UTeM) - Logo.png'); ?>" alt="logo" style="width:50px;">
				</a>
				<li class="nav-item">
					<a class="nav-link <?= active_link(['Dashboard'], ['index']); ?>" href="<?= base_url('public/admin/dashboard');?>"><span class="fas fa-home"></span>&nbsp;&nbsp;Home</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle <?= active_link(['User_Management', 'Faculty_Management', 'Course_Management', 'State_Management', 'Language_Management']); ?>" href="#" id="navbardrop" data-toggle="dropdown">
						<span class="fas fa-tasks"></span>&nbsp;&nbsp;Management
					</a>
					<div class="dropdown-menu pull-right">
						<a class="dropdown-item <?= active_link(['User_Management'], ['index', 'register_user', 'view_user', 'edit_user']); ?>" href="<?= base_url('public/admin/user_management');?>"><span class="fas fa-user"></span>&nbsp;&nbsp;User Management</a>
						<!-- <a class="dropdown-item <?= active_link(['Faculty_Management'], ['index', 'register_faculty', 'edit_faculty']); ?>" href="<?= base_url('public/admin/faculty_management');?>"><span class="fas fa-building"></span>&nbsp;&nbsp;Faculty Management</a> --> <!-- The System Is Focus on FTMK only -->
						<a class="dropdown-item <?= active_link(['Course_Management'], ['index', 'register_course', 'edit_course']); ?>" href="<?= base_url('public/admin/course_management');?>"><span class="fas fa-graduation-cap"></span>&nbsp;&nbsp;Programme Management</a>
						<a class="dropdown-item <?= active_link(['State_Management'], ['index', 'add_state', 'edit_state']); ?>" href="<?= base_url('public/admin/state_management');?>"><span class="fas fa-globe-asia"></span>&nbsp;&nbsp;State Management</a>
						<a class="dropdown-item <?= active_link(['Language_Management'], ['index', 'add_language', 'edit_language']); ?>" href="<?= base_url('public/admin/language_management');?>"><span class="fas fa-language"></span>&nbsp;&nbsp;Language Management</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link <?= active_link(['Supervision'], ['index', 'supervision', 'assign_student']); ?>" href="<?= base_url('public/admin/supervision');?>"><span class="fas fa-users"></span>&nbsp;&nbsp;Coordinator</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?= active_link(['Announcement'], ['index', 'create_announcement', 'view_announcement', 'edit_announcement']); ?>" href="<?= base_url('public/admin/announcement');?>"><span class="fas fa-bullhorn"></span>&nbsp;&nbsp;Announcement</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle <?= active_link(['Account_Management']); ?>" href="#" id="navbardrop" data-toggle="dropdown">
						<span class="fas fa-user"></span>&nbsp;&nbsp;Hi, <?= (session()->get('name')) ? session()->get('name') : session()->get('email'); ?>
					</a>
					<div class="dropdown-menu pull-right">
						<a class="dropdown-item <?= active_link(['Account_Management'], ['index']); ?>" href="<?= base_url('public/admin/account_management');?>"><span class="fas fa-user-cog"></span>&nbsp;&nbsp;Account Management</a>
						<a class="dropdown-item" href="<?= base_url('public/admin/logout');?>"><span class="fas fa-sign-out-alt"></span>&nbsp;&nbsp;Logout</a>
					</div>
				</li>
			</ul>
		</div>
  	</div>
</nav>
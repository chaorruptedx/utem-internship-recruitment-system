<div class="container">
    <br>
    <h1 class="text-center">Edit State Details</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="card">
        <div class="card-body">

    <form action="<?= base_url('public/admin/state_management/edit_state/'.$alStateModel['id']); ?>" method="post">
        <?= csrf_field() ?>

        <div class="form-group">
            <label for="name" class="font-weight-bold">Name</label>
            <input type="text" name="name" value="<?= $alStateModel['name']; ?>" placeholder="Enter State Name" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="type" class="font-weight-bold">Type</label>
            <select name="type" class="form-control">
                <option value="" selected disabled>Choose State Type ...</option>
                <option value="1" <?php if ($alStateModel['type'] == '1') echo 'selected'; ?>>Normal Weekends</option>
                <option value="2" <?php if ($alStateModel['type'] == '2') echo 'selected'; ?>>Non-normal Weekends</option>
            </select>
        </div>

        </div>
    </div>
    <br>

        <div class="text-right btn-form form-group">
            <a href="<?= base_url('public/admin/state_management/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
            <button type="submit" name="submit" class="btn btn-primary"><span class="fas fa-check"></span>&nbsp;&nbsp;Update State</button>
        </div>
    </form>

    <br>
</div>
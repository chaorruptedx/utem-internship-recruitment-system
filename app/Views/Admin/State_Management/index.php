<div class="container">
    <br>
    <h1 class="text-center">State Management</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of State</h2>
            </div>
        </div>

        <a href="<?= base_url('public/admin/state_management/add_state');?>" class="btn btn-success"><span class="fas fa-plus"></span>&nbsp;&nbsp;Add State</a><br><br>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($state) && is_array($state)) : ?>
                    <?php foreach ($state as $state_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($state_data['name']); ?></td>
                            <td>
                                <?php
                                    if (esc($state_data['type']) == '1') :
                                        echo 'Normal Weekends';
                                    elseif (esc($state_data['type']) == '2') :
                                        echo 'Non-normal Weekends';
                                    endif;
                                ?>
                            </td>
                            <td class="text-center">
                                <a href="<?= base_url('public/admin/state_management/edit_state/'.$state_data['id']);?>"><span class="fas fa-edit"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal<?= $state_data['id']; ?>"><span class="fas fa-trash"></span></a>
                            </td>
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $state_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remove State</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to remove state <b><?= esc($state_data['name']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/admin/state_management/remove_state/'.$state_data['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


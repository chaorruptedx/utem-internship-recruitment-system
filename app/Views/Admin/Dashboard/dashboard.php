<div class="container">
    <br>
    <h1 class="text-center">Dashboard</h1>
    <br>

    <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-sm-12">
                    <h3 class="text-center">Number of Users</h3>
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading red"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content red">
                                    <div class="circle-tile-description text-faded">Admin</div>
                                    <div class="circle-tile-number text-faded "><?= $number_of_user['admin']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading dark-blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content dark-blue">
                                    <div class="circle-tile-description text-faded">Coordinator</div>
                                    <div class="circle-tile-number text-faded "><?= $number_of_user['coordinator']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading green"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content green">
                                    <div class="circle-tile-description text-faded">Student</div>
                                    <div class="circle-tile-number text-faded "><?= $number_of_user['student']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading purple"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content purple">
                                    <div class="circle-tile-description text-faded">Organization</div>
                                    <div class="circle-tile-number text-faded "><?= $number_of_user['organization']; ?></div>
                                    <a class="circle-tile-footer" href="<?= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-sm-12">
                    <br><br>
                    <h3 class="text-center">Student Supervision Status</h3>
                    <div id="student_supervised_unsupervised"></div>
                </div>

                <div class="col-sm-12">
                    <br><br>
                    <h3 class="text-center">Number of Students under Programme</h3>
                    <div id="student_of_programme"></div>
                </div>

            </div>

        </div>
    </div>

    <br>
</div>
<div class="container">
    <br>
    <h1 class="text-center">Faculty Management</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Faculty</h2>
            </div>
        </div>

        <a href="<?= base_url('public/admin/faculty_management/register_faculty');?>" class="btn btn-success"><span class="fas fa-plus"></span>&nbsp;&nbsp;Register Faculty</a><br><br>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($faculty) && is_array($faculty)) : ?>
                    <?php foreach ($faculty as $faculty_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($faculty_data['code']); ?></td>
                            <td><?= esc($faculty_data['name']); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/admin/faculty_management/edit_faculty/'.$faculty_data['id']);?>"><span class="fas fa-edit"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal<?= $faculty_data['id']; ?>"><span class="fas fa-trash"></span></a>
                            </td>
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $faculty_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remove Faculty</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to remove faculty <b><?= esc($faculty_data['code']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/admin/faculty_management/remove_faculty/'.$faculty_data['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


<div class="container">
    <br>
    <h1 class="text-center">Faculty Registration</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="card">
        <div class="card-body">

    <form action="<?= base_url('public/admin/faculty_management/register_faculty');?>" method="post">
        <?= csrf_field() ?>

        <div class="form-group">
            <label for="code" class="font-weight-bold">Code</label>
            <input type="text" name="code" placeholder="Enter Faculty Code" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="name" class="font-weight-bold">Name</label>
            <input type="text" name="name" placeholder="Enter Faculty Name" class="form-control"/>
        </div>

        </div>
    </div>
    <br>

        <div class="text-right btn-form form-group">
            <a href="<?= base_url('public/admin/faculty_management/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
            <button type="submit" name="submit" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Register Faculty</button>
        </div>
    </form>

    <br>
</div>
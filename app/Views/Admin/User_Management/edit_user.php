<div class="container">
    <br>
    <h1 class="text-center">Edit User Details</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="card">
        <div class="card-body">

    <form action="<?= base_url('public/admin/user_management/edit_user/'.$userModel['id']);?>" method="post">
        <?= csrf_field() ?>

        <div class="form-group">
            <label for="email" class="font-weight-bold">E-mail</label>
            <input type="email" name="email" value="<?= $userModel['email']; ?>" placeholder="Enter E-mail" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="email" class="font-weight-bold">New Password</label>
            <input type="password" name="password" placeholder="Enter New Password" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="role" class="font-weight-bold">Role</label>
            <select id="user-management-role" name="role" class="form-control">
                <option value="" selected disabled>Choose Role ...</option>
                <option value="1" <?php if ($userModel['role'] == '1') echo 'selected'; ?>>Admin</option>
                <option value="2" <?php if ($userModel['role'] == '2') echo 'selected'; ?>>Coordinator</option>
                <option value="3" <?php if ($userModel['role'] == '3') echo 'selected'; ?>>Student</option>
                <option value="4" <?php if ($userModel['role'] == '4') echo 'selected'; ?>>Company</option>
            </select>
        </div>

        <div id="user-management-details"></div>

        </div>
    </div>
    <br>

        <div class="text-right btn-form form-group">
            <a href="<?= base_url('public/admin/user_management/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
            <button type="submit" name="submit" class="btn btn-primary"><span class="fas fa-check"></span>&nbsp;&nbsp;Update User</button>
        </div>
    </form>

    <br>
</div>
<div class="container">
    <br>
    <h1 class="text-center">User Registration</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="card">
        <div class="card-body">

    <form action="<?= base_url('public/admin/user_management/register_user');?>" method="post">
        <?= csrf_field() ?>

        <div class="form-group">
            <label for="email" class="font-weight-bold">E-mail</label>
            <input type="email" name="email" placeholder="Enter E-mail" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="email" class="font-weight-bold">Password</label>
            <input type="password" name="password" placeholder="Enter Password" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="role" class="font-weight-bold">Role</label>
            <select id="user-management-role" name="role" class="form-control role">
                <option value="" selected disabled>Choose Role ...</option>
                <option value="1">Admin</option>
                <option value="2">Coordinator</option>
                <option value="3">Student</option>
                <option value="4">Company</option>
            </select>
        </div>

        <div id="user-management-details"></div>

        </div>
    </div>
    <br>

        <div class="text-right btn-form form-group">
            <a href="<?= base_url('public/admin/user_management/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
            <button type="submit" name="submit" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Register User</button>
        </div>
    </form>

    <br>
</div>
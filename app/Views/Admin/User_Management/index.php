<div class="container">
    <br>
    <h1 class="text-center">User Management</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of User</h2>
            </div>
        </div>

        <a href="<?= base_url('public/admin/user_management/register_user');?>" class="btn btn-success"><span class="fas fa-user-plus"></span>&nbsp;&nbsp;Register User</a><br><br>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>E-mail</th>
                    <th>Role</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($user) && is_array($user)) : ?>
                    <?php foreach ($user as $user_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($user_data['email']); ?></td>
                            <td>
                                <?php
                                    if (esc($user_data['role']) == '1') :
                                        echo 'Admin';
                                    elseif (esc($user_data['role']) == '2') :
                                        echo 'Coordinator';
                                    elseif (esc($user_data['role']) == '3') :
                                        echo 'Student';
                                    elseif (esc($user_data['role']) == '4') :
                                        echo 'Company';
                                    endif;
                                ?>
                            </td>
                            <td class="text-center">
                                <a href="<?= base_url('public/admin/user_management/view_user/'.$user_data['id']);?>"><span class="fas fa-eye"></span></a>
                                &nbsp;
                                <a href="<?= base_url('public/admin/user_management/edit_user/'.$user_data['id']);?>"><span class="fas fa-edit"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal<?= $user_data['id']; ?>"><span class="fas fa-trash"></span></a>
                            </td>
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $user_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remove User</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to remove user <b><?= esc($user_data['email']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/admin/user_management/remove_user/'.$user_data['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


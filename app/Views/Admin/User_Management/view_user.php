<div class="container">
    <br>
    <h1 class="text-center">View User Details</h1>
    <br>

    <table class="table table-bordered table-striped table-hover table-dark text-center">
        <?php if ($user_details['role'] == 2 || $user_details['role'] == 3) : ?>
            <tr>
                <th style="width: 200px">Name</th>
                <td><?= $personal_details['name'] ?></td>
            </tr>
            <tr>
                <th>IC No.</th>
                <td><?= $personal_details['nric_no'] ?></td>
            </tr>
            <tr>
                <th><?php if ($user_details['role'] == 2) { echo 'Staff'; } else if ($user_details['role'] == 3) { echo 'Matric'; } ?> No.</th>
                <td><?= $personal_details['user_no'] ?></td>
            </tr>
            <tr>
                <th>Programme</th>
                <td><?= $personal_details['course_name'] ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <th>Role</th>
            <td><?= role_name($user_details['role']); ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= $user_details['email'] ?></td>
        </tr>
    </table>

    <br>

    <div class="text-right btn-form form-group">
        <a href="<?= base_url('public/admin/user_management/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
    </div>

    <br>
</div>
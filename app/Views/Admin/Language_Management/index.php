<div class="container">
    <br>
    <h1 class="text-center">Language Management</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Language</h2>
            </div>
        </div>

        <a href="<?= base_url('public/admin/language_management/add_language');?>" class="btn btn-success"><span class="fas fa-plus"></span>&nbsp;&nbsp;Add Language</a><br><br>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Name</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($language) && is_array($language)) : ?>
                    <?php foreach ($language as $language_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($language_data['name']); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/admin/language_management/edit_language/'.$language_data['id']);?>"><span class="fas fa-edit"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal<?= $language_data['id']; ?>"><span class="fas fa-trash"></span></a>
                            </td>
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $language_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remove Language</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to remove language <b><?= esc($language_data['name']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/admin/language_management/remove_language/'.$language_data['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


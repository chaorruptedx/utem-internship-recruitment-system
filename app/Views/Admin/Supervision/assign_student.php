<div class="container">
    <br>
    <h1 class="text-center">Coordinator Details</h1>
    <br>

    <div class="container">
        <div class="row">
            <label for="personal_details_name" class="control-label col-md-2 text-right font-weight-bold">Name</label>
            <div class="col-md-10">
                <input disabled type="text" name="personal_details_name" value="<?= $supervisor['personal_details_name']; ?>" class="form-control"/>
            </div>
            <label for="email" class="control-label col-md-2 text-right font-weight-bold">E-mail</label>
            <div class="col-md-10">
                <input disabled type="text" name="email" value="<?= $supervisor['email']; ?>" class="form-control"/>
            </div>
            <label for="no_tel" class="control-label col-md-2 text-right font-weight-bold">Tel. No.</label>
            <div class="col-md-10">
                <input disabled type="text" name="no_tel" value="<?= $supervisor['tel_no']; ?>" class="form-control"/>
            </div>
            <label for="faculty_name" class="control-label col-md-2 text-right font-weight-bold">Faculty</label>
            <div class="col-md-10">
                <input disabled type="text" name="faculty_name" value="<?//= $supervisor['faculty_name']; ?>Faculty of Information and Communications Technology" class="form-control"/>
            </div>
            <label for="course_name" class="control-label col-md-2 text-right font-weight-bold">Programme</label>
            <div class="col-md-10">
                <input disabled type="text" name="course_name" value="<?= $supervisor['course_name']; ?>" class="form-control"/>
            </div>
        </div>
    </div>

    <br>

    <div class="text-right btn-form form-group">
        <a href="<?= base_url('public/admin/supervision/supervision/'.$supervisor['personal_details_id']);?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
    </div>

    <br>
    <h1 class="text-center">Assign Student</h1>
    <br>

    <form action="<?= base_url('public/admin/supervision/assign_student/'.$supervisor['personal_details_id']);?>" method="post">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <h2>List of All Students</h2>
                </div>
            </div>

                <span class="border bg-success" style="font-size: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Current Coordinator
                <br>
                <span class="border bg-warning" style="font-size: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Other Coordinator
                <br>
                <span class="border bg-dark" style="font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;No Coordinator

            <br><br>
            
            <table id="data-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" width="10px">No.</th>
                        <th>Name / Email</th>
                        <th>Matric ID</th>
                        <th>Faculty</th>
                        <th>Programme</th>
                        <th>Tel. No.</th>
                        <th class="text-center" width="110px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($student) && is_array($student)) : ?>
                        <?php foreach ($student as $student_data) : ?>
                            <tr class="
                            <?php
                                if (esc($student_data['student_status']) == 'green') :
                                    echo 'text-dark table-success';
                                elseif (esc($student_data['student_status']) == 'grey') :
                                    echo 'text-dark table-warning';
                                endif;
                            ?>
                            ">
                                <td class="text-center"><?= ++$no ?></td>
                                <td><?= (esc($student_data['personal_details_name'])) ? esc($student_data['personal_details_name']) : esc($student_data['email']); ?></td>
                                <td><?= esc($student_data['user_no']); ?></td>
                                <td><?//= esc($student_data['faculty_code']); ?>FTMK</td>
                                <td><?= esc($student_data['course_code']); ?></td>
                                <td><?= esc($student_data['tel_no']); ?></td>
                                <td class="text-center">
                                    <input
                                    <?php
                                        if (esc($student_data['student_status']) == 'green') :
                                            echo 'checked'; $text = '1';
                                        elseif (esc($student_data['student_status']) == 'grey') :
                                            echo 'checked disabled'; $text = '2';
                                        else :
                                            $text = '3';
                                        endif;
                                    ?>
                                    type="checkbox" name="student_status[]" value="<?= esc($student_data['id']); ?>">
                                    <span style="display: none ;"><?= $text ?></span>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

        <br>

        <div class="text-right btn-form form-group">
            <button type="submit" name="submit" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Update Student</button>
        </div>
    </form>

    <br>
</div>
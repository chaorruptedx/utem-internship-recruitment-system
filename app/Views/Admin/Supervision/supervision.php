<div class="container">
    <br>
    <h1 class="text-center">Coordinator Details</h1>
    <br>

    <div class="container">
        <div class="row">
            <label for="personal_details_name" class="control-label col-md-2 text-right font-weight-bold">Name</label>
            <div class="col-md-10">
                <input disabled type="text" name="personal_details_name" value="<?= $coordinator['personal_details_name']; ?>" class="form-control"/>
            </div>
            <label for="email" class="control-label col-md-2 text-right font-weight-bold">E-mail</label>
            <div class="col-md-10">
                <input disabled type="text" name="email" value="<?= $coordinator['email']; ?>" class="form-control"/>
            </div>
            <label for="no_tel" class="control-label col-md-2 text-right font-weight-bold">Tel. No.</label>
            <div class="col-md-10">
                <input disabled type="text" name="no_tel" value="<?= $coordinator['tel_no']; ?>" class="form-control"/>
            </div>
            <label for="faculty_name" class="control-label col-md-2 text-right font-weight-bold">Faculty</label>
            <div class="col-md-10">
                <input disabled type="text" name="faculty_name" value="<?//= $coordinator['faculty_name']; ?>Faculty of Information and Communications Technology" class="form-control"/>
            </div>
            <label for="course_name" class="control-label col-md-2 text-right font-weight-bold">Programme</label>
            <div class="col-md-10">
                <input disabled type="text" name="course_name" value="<?= $coordinator['course_name']; ?>" class="form-control"/>
            </div>
        </div>
    </div>

    <br>

    <div class="text-right btn-form form-group">
        <a href="<?= base_url('public/admin/supervision/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
    </div>

    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Students under Coordinator</h2>
            </div>
        </div>

        <a href="<?= base_url('public/admin/supervision/assign_student/'.$coordinator['personal_details_id']);?>" class="btn btn-success"><span class="fas fa-user-plus"></span>&nbsp;&nbsp;Assign New Student</a><br><br>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Name / Email</th>
                    <th>Matric ID</th>
                    <th>Faculty</th>
                    <th>Programme</th>
                    <th>Tel. No.</th>
                    <!-- <th class="text-center" width="110px">Actions</th> -->
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($student) && is_array($student)) : ?>
                    <?php foreach ($student as $student_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= (esc($student_data['personal_details_name'])) ? esc($student_data['personal_details_name']) : esc($student_data['email']); ?></td>
                            <td><?= esc($student_data['user_no']); ?></td>
                            <td><?//= esc($student_data['faculty_code']); ?>FTMK</td>
                            <td><?= esc($student_data['course_code']); ?></td>
                            <td><?= esc($student_data['tel_no']); ?></td>
                            <!-- <td class="text-center">
                                <a href="<?= base_url('public/admin/supervision/view_student_details/'.$student_data['id_supervisee']);?>"><span class="fas fa-eye"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal<?= $student_data['id_supervisee']; ?>"><span class="fas fa-trash"></span></a>
                            </td> -->
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $user_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remove Student From Supervisor</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to remove student <b><?= esc($student_data['personal_details_name']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/admin/supervision/remove_student/'.$student_data['id_supervisee']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>
<div class="container">
    <br>
    <h1 class="text-center">Coordinator</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Coordinator</h2>
            </div>
        </div>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Name</th>
                    <th>Programme</th>
                    <th>Email</th>
                    <th>Tel. No.</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($coordinator) && is_array($coordinator)) : ?>
                    <?php foreach ($coordinator as $coordinator_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($coordinator_data['name']); ?></td>
                            <td><?= esc($coordinator_data['code']); ?></td>
                            <td><?= esc($coordinator_data['email']); ?></td>
                            <td><?= esc($coordinator_data['tel_no']); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/admin/supervision/supervision/'.$coordinator_data['id']);?>"><span class="fas fa-users-cog"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


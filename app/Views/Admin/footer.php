<br>
<br>
<footer>
	<div class="copyrights">
		<p>&copy; <?= date('Y') ?> UTeM Internship Recruitment System</p>
	</div>
</footer>

<script src="<?php echo base_url('public/assets/bootstrap/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/d3/d3.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/c3/c3.min.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#data-table').DataTable();
		var element = document.getElementById("data-table");
		if (element !== null)
  			element.classList.add("table-striped", "table-hover", "table-dark", "text-center");

		$('select').select2();

		var student_supervision_status_data = <?= json_encode($student_supervision_status) ?>;
		
		var student_supervised_unsupervised = c3.generate({
			bindto: '#student_supervised_unsupervised',
			data: {
				columns: [
					['Supervised', 0],
					['Unsupervised', 0],
				],
				type : 'pie',
			}
		});
		
		if (student_supervision_status_data != null)
		{
			setTimeout(function () {
				student_supervised_unsupervised.load({
					columns: [
						['Supervised', student_supervision_status_data.student_supervised],
						['Unsupervised', student_supervision_status_data.student_unsupervised],
					],
				});
			}, 100);
		}

		var student_under_programme_data = <?= json_encode($student_under_programme) ?>;
		
		var student_of_programme = c3.generate({
			bindto: '#student_of_programme',
			data: {
				columns: [
					['Programme', 0]
				],
				type: 'bar',
			},
			axis: {
				x: {
					type: 'category',
					categories: ['PROGRAMME']
				}
			}
		});

		if (student_under_programme_data != null) 
		{
			setTimeout(function () {
				student_of_programme.load({
					columns: [
						student_under_programme_data.number_of_course
					],
					categories: student_under_programme_data.course_code
				});
			}, 100);
		}

		$(".custom-file-input").on("change", function() {
			var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});

		var user_management_details = (function userManagementDetails()
		{
			var role = $("select#user-management-role").val();

			var select_options = <?php
				if (!empty($course) && is_array($course)) {
					foreach ($course as $course_data)
					{
						$selected = '';

						if ($personal_details['id_course'] == $course_data['id'])
							$selected = 'selected';

						$select_options .= '<option value="'.$course_data['id'].'" '.$selected.'>'.$course_data['name'].'</option>';
					}
				}
				
				echo "'".$select_options."'";
			?>

			var personal_details_name = '<?= $personal_details['name'] ?>';
			var personal_details_nric_no = '<?= $personal_details['nric_no'] ?>';
			var personal_details_user_no = '<?= $personal_details['user_no'] ?>';
			var personal_details_tel_no = '<?= $personal_details['tel_no'] ?>';

			$("div#user-management-details-remove").remove();

			if (role == 2) {

				var form_element = [
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="name" class="font-weight-bold">Name</label>',
						'<input id="name" type="text" name="name" value="'+personal_details_name+'" placeholder="Enter User Name" class="form-control"/>',
					'</div>',
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="user_no" class="font-weight-bold">Staff No.</label>',
						'<input type="text" name="user_no" value="'+personal_details_user_no+'" placeholder="Enter Staff No." class="form-control"/>',
					'</div>',
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="id_course" class="font-weight-bold">Programme</label>',
						'<select name="id_course" class="form-control role">',
						'<option value="" selected disabled>Choose Programme ...</option>',
						select_options,
						'</select>',
					'</div>',
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="tel_no" class="font-weight-bold">Tel. No.</label>',
						'<input type="number" name="tel_no" value="'+personal_details_tel_no+'" placeholder="Enter Tel. No." class="form-control"/>',
					'</div>',
				];

				$("div#user-management-details").append(form_element.join(''));
			}
			else if (role == 3)
			{
				var form_element = [
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="name" class="font-weight-bold">Name</label>',
						'<input id="name" type="text" name="name" value="'+personal_details_name+'" placeholder="Enter User Name" class="form-control"/>',
					'</div>',
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="nric_no" class="font-weight-bold">IC No.</label>',
						'<input type="number" name="nric_no" value="'+personal_details_nric_no+'" placeholder="Enter IC No." class="form-control"/>',
					'</div>',
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="user_no" class="font-weight-bold">Matric No.</label>',
						'<input type="text" name="user_no" value="'+personal_details_user_no+'" placeholder="Enter Matric No." class="form-control"/>',
					'</div>',
					'<div id="user-management-details-remove" class="form-group">',
						'<label for="id_course" class="font-weight-bold">Programme</label>',
						'<select name="id_course" class="form-control role">',
						'<option value="" selected disabled>Choose Programme ...</option>',
						select_options,
						'</select>',
					'</div>',
				];

				$("div#user-management-details").append(form_element.join(''));
			}
			else
			{
				$("div#user-management-details-remove").remove();
			}
		});

		$("#user-management-role").on("change", user_management_details);

		if ($("#user-management-role").val() == 2 || $("#user-management-role").val() == 3)
		{
			$(user_management_details);
		}
	
	} );
</script>

</body>

</html>
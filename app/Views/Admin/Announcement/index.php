<div class="container">
    <br>
    <h1 class="text-center">Announcement</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Announcement</h2>
            </div>
        </div>

        <a href="<?= base_url('public/admin/announcement/create_announcement');?>" class="btn btn-success"><span class="fas fa-plus"></span>&nbsp;&nbsp;Create Announcement</a><br><br>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Title</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($announcement) && is_array($announcement)) : ?>
                    <?php foreach ($announcement as $announcement_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($announcement_data['title']); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/admin/announcement/view_announcement/'.$announcement_data['id']);?>"><span class="fas fa-eye"></span></a>
                                &nbsp;
                                <a href="<?= base_url('public/admin/announcement/edit_announcement/'.$announcement_data['id']);?>"><span class="fas fa-edit"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal<?= $announcement_data['id']; ?>"><span class="fas fa-trash"></span></a>
                            </td>
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $announcement_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remove Announcement</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to remove announcement: <b><?= esc($announcement_data['title']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/admin/announcement/remove_announcement/'.$announcement_data['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


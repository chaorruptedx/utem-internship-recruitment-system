<div class="container">
    <br>
    <h1 class="text-center">Edit Announcement</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="card">
        <div class="card-body">

    <form action="<?= base_url('public/admin/announcement/edit_announcement/'.$announcementModel['id']);?>" method="post">
        <?= csrf_field() ?>

        <div class="form-group">
            <label for="title" class="font-weight-bold">Title</label>
            <input type="text" name="title" value="<?= $announcementModel['title']; ?>" placeholder="Enter Announcement Title" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="content" class="font-weight-bold">Content</label>
            <textarea name="content" rows="10" placeholder="Enter Announcement Content" class="form-control"><?= $announcementModel['content']; ?></textarea>
        </div>

        </div>
    </div>
    <br>

            <div class="text-right btn-form form-group">
                <a href="<?= base_url('public/admin/announcement/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
                <button type="submit" name="submit" class="btn btn-primary"><span class="fas fa-check"></span>&nbsp;&nbsp;Update Announcement</button>
            </div>
    </form>

    <br>
</div>
<div class="container">
    <br>
    <h1 class="text-center">View Announcement</h1>
    <br>

    <table class="table table-bordered table-striped table-hover table-dark text-center">
        <tr>
            <th style="width: 200px">Title</th>
            <td><?= $announcementModel['title'] ?></td>
        </tr>
        <tr>
            <th>Content</th>
            <td><?= $announcementModel['content'] ?></td>
        </tr>
    </table>
    <br>

    <div class="text-right btn-form form-group">
        <a href="<?= base_url('public/admin/announcement/index');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
        <a href="<?= base_url('public/admin/announcement/edit_announcement/'.$announcementModel['id']);?>" class="btn btn-primary"><span class="fas fa-edit"></span>&nbsp;&nbsp;Edit</a>
    </div>

    <br>
</div>
<br>
<br>
<footer>
	<div class="copyrights">
		<p>&copy; <?= date('Y') ?> UTeM Internship Recruitment System</p>
	</div>
</footer>

<script src="<?php echo base_url('public/assets/bootstrap/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/d3/d3.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/c3/c3.min.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#data-table').DataTable();
		var element = document.getElementById("data-table");
		if (element !== null)
  			element.classList.add("table-striped", "table-hover", "table-dark", "text-center");

		$('select').select2();

		var student_internship_status_bar_chart = c3.generate({
			bindto: '#student-internship-status-bar',
			data: {
				x : 'x',
				columns: [
					['x', 'Internship Status'],
					['data1', 0],
					['data2', 0],
					['data3', 0],
				],
				names: {
					data1: 'Not Apply',
					data2: 'Pending',
					data3: 'Got Internship',
				},
				type: 'bar',
				colors: {
					data1: 'red',
					data2: 'orange',
					data3: 'green'
				},
				color: function (color, d) {
					// d will be 'id' when called for legends
					return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
				}
			},
			axis: {
				rotated: true,
				x: {
					type: 'category' // this needed to load string x value
				}
			}
		});

		var student_internship_status_chart = c3.generate({
			bindto: '#student-internship-status',
			data: {
				columns: [
					['data1', 0],
					['data2', 0],
					['data3', 0],
				],
				names: {
					data1: 'Not Apply',
					data2: 'Pending',
					data3: 'Got Internship',
				},
				type : 'pie',
				colors: {
					data1: 'red',
					data2: 'orange',
					data3: 'green'
				},
				color: function (color, d) {
					// d will be 'id' when called for legends
					return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
				}
			}
		});

		var student_internship_status_data = <?= json_encode($student_internship_status) ?>;

		if (student_internship_status_data != null)
		{
			setTimeout(function () {
				student_internship_status_bar_chart.load({
					columns: [
						['data1', student_internship_status_data.not_apply],
						['data2', student_internship_status_data.pending],
						['data3', student_internship_status_data.got_internship],
					],
				});
			}, 100);

			setTimeout(function () {
				student_internship_status_chart.load({
					columns: [
						['data1', student_internship_status_data.not_apply],
						['data2', student_internship_status_data.pending],
						['data3', student_internship_status_data.got_internship],
					],
				});
			}, 100);
		}

		var student_placement_state_chart = c3.generate({
			bindto: '#student-placement-state',
			data: {
				columns: [
					['data1', 0],
					['data2', 0],
				],
				names: {
					data1: 'Got Internship',
					data2: 'Pending',
				},
				type: 'bar',
				groups: [
					['data1', 'data2']
				],
				colors: {
					data1: 'green',
					data2: 'orange',
				},
				color: function (color, d) {
					// d will be 'id' when called for legends
					return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
				}
			},
			axis: {
				x: {
					type: 'category',
					categories: ['State']
				}
			}
		});

		var student_placement_state_data = <?= json_encode($student_internship_state) ?>;

		if (student_placement_state_data != null) 
		{
			setTimeout(function () {
				student_placement_state_chart.load({
					columns: [
						student_placement_state_data.accepted,
						student_placement_state_data.pending
					],
					categories: student_placement_state_data.state_name
				});
			}, 100);
		}

	} );
</script>

</body>

</html>

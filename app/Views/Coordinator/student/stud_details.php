<div class="page-header">
    <h1 align="center">STUDENT DETAILS</h1> </br>
  </div>

    <div class="container">
        <div class="row text-center">
            <div class="col-12 my-4">
                <h3 class="text-left">Student Details</h3>
                <table class="table table-bordered table-dark">
                    <tr>
                        <th>Name</th>
                        <td>Ahmad</td>
                    </tr>
                    <tr>
                        <th>Matric Number</th>
                        <td>B031837393</td>
                    </tr>
                    <tr>
                        <th>Course</th>
                        <td>BITS</td>
                    </tr>
                    <tr>
                        <th>Email Address</th>
                        <td>B031837393@student.utem.my</td>
                    </tr>
                    <tr>
                        <th>Contact Number</th>
                        <td>0197848832</td>
                    </tr>
                    <tr>
                        <th>Resume</th>
                        <td>
                            <a href="#">Ahmad</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-12 my-4">
                <h3 class="text-left">List Of Applications</h3>
                <table class="table table-bordered table-dark">
                    <tr>
                        <th>No</th>
                        <th>Company Name</th>
                        <th>Application Date</th>
                        <th>Application Status</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Company 1</td>
                        <td>December 10 , 1915</td>
                        <td class="text-warning">Pending</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Company 2</td>
                        <td>December 10 , 1915</td>
                        <td class="text-warning">Pending</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Company 3</td>
                        <td>December 10 , 1915</td>
                        <td class="text-warning">Pending</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Company 4</td>
                        <td>December 10 , 1915</td>
                        <td class="text-danger">Rejected</td>
                    </tr>
                </table>
            </div>
            <div class="btn-form form-group">
        <a href="<?= base_url('public/coordinator/studentList');?>" class="btn btn-secondary">Back</a>
     </div>
        </div>
    </div>
</div>
<div class="page-header">
    <h1 class="text-center">List of Internship Student</h1> </br>
  </div>
  
  <div class="container">
        
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Student Name</th>
                    <th>Matric Number</th>
                    <th>Course</th>
                    <th>Tel. No.</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($student) && is_array($student)) : ?>
                    <?php foreach ($student as $student_data) : ?>
                        <tr>
                            <td><?= ++$x ?></td>
                            <td><?= esc($student_data['personal_details_name']) ?></td>
                            <td><?= esc($student_data['user_no']) ?></td>
                            <td><?= esc($student_data['course_code']) ?></td>
                            <td><?= esc($student_data['tel_no']) ?></td>
                            <td><?= esc($student_data['email']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


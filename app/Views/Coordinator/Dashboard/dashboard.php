<div class="container">
    <br>
    <h1 class="text-center">Dashboard</h1>
    <br>

    <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-sm-12">
                    <br><br>
                    <h3 class="text-center">Student Internship Status</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="student-internship-status-bar"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="student-internship-status"></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <br><br>
                    <h3 class="text-center">Student Placement According to State</h3>
                    <div id="student-placement-state"></div>
                </div>

            </div>

        </div>
    </div>

    <br>
</div>
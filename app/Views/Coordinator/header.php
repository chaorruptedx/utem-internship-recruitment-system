<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>UTeM Internship Recruitment System</title>
	<style>
		body {
			background-image: url("<?php echo base_url('public/background/Background.jpg'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
		.copyrights {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			background-color: #212529;
			color: #FFFFFF80;
			text-align: center;
		}
	</style>
	<link rel="stylesheet" href="<?php echo base_url('public/assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/datatables/datatables.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/c3/c3.min.css'); ?>">

</head>

<body>


<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<div class="container">
			<ul class="navbar-nav">
				<a class="navbar-brand" href="#">
					<img src="<?= base_url('public/logo/Universiti Teknikal Malaysia Melaka (UTeM) - Logo.png'); ?>" alt="logo" style="width:50px;">
				</a>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('public/coordinator/dashboard');?>"><span class="fas fa-home"></span>&nbsp;&nbsp;Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('public/coordinator/studentlist');?>"><span class='far fa-file-alt'></span>&nbsp;&nbsp;List of Intership Student</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
						<span class="fas fa-user"></span>&nbsp;&nbsp;Hi, <?= (session()->get('name')) ? session()->get('name') : session()->get('email'); ?>
					</a>
					<div class="dropdown-menu pull-right">
						<a class="dropdown-item" href="<?= base_url('public/coordinator/account_management');?>"><span class="fas fa-user-cog"></span>&nbsp;&nbsp;Account Management</a>
						<a class="dropdown-item" href="<?= base_url('public/coordinator/logout');?>"><span class="fas fa-sign-out-alt"></span>&nbsp;&nbsp;Logout</a>
					</div>
				</li>
			</ul>
		</div>
  	</div>
</nav>
</br>



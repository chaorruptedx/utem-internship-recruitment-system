<div class="container">
    <br>
    <h1 class="text-center">User Account Details</h1>
    <br>

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <form action="<?= base_url('public/coordinator/account_management/index/') ?>" method="post" enctype="multipart/form-data">
        <?= csrf_field() ?>

        <div class="text-center">
            <img src="<?= ($profile_picture != null) ? base_url('public/'.$profile_picture['path'].$profile_picture['name']) : base_url('public/profile_picture/default_profile_picture.jpg'); ?>" alt="Profile Picture" class="mx-auto rounded" style="max-width: 200px; max-height: 200px;">
        </div><br><br>

        <div class="card">
            <div class="card-body">

                <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="customFile" accept="image/*">
                    <label class="custom-file-label" for="customFile">Profile Picture</label>
                </div><br><br>

                <div class="form-group">
                    <label for="email" class="font-weight-bold">E-mail</label>
                    <input type="email" name="email" value="<?= $userModel['email']; ?>" placeholder="Enter E-mail" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="email" class="font-weight-bold">New Password</label>
                    <input type="password" name="password" placeholder="Enter New Password" class="form-control"/>
                </div>

            </div>
        </div>
        <br>

        <div class="text-right btn-form form-group">
            <button type="submit" name="submit" class="btn btn-primary"><span class="fas fa-check"></span>&nbsp;&nbsp;Update Account</button>
        </div>
    </form>

    <br>
</div>
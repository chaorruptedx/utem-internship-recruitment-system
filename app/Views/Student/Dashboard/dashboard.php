<div class="container">
<br>
    <h1 class="text-center">Dashboard</h1>
<br>

<style>
.cardcon {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
  border-radius: 20px;
}

</style>

<div class="container">
    <h2>Announcement </h2>
        <div class="cardcon">
        <ul>
            <?php foreach ($AnnouncementModel as $announcement_data) : ?>
                <h4><?= esc($announcement_data['title']); ?></h4>
                <li><?= esc($announcement_data['content']); ?></li><hr>
            <?php endforeach ?>
        </ul>
      
        </div>
</div>

    <br><br>

    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Application</h2>
            </div>
        </div>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Organization Name</th>
                    <th>Status</th>
                    <th>Date of Application</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($internship_application) && is_array($internship_application)) : ?>
                    <?php foreach ($internship_application as $internship_application_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($internship_application_data['organization_name']); ?></td>
                            <td class="<?= esc(application_status_student($internship_application_data['application_status'], 2)); ?>"><?= esc(application_status_student($internship_application_data['application_status'], 1)); ?></td>
                            <td><?= date( 'd M Y, h:i A', strtotime(esc($internship_application_data['created_at']))); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/student/StudentApplicants/view_organization/'.$internship_application_data['id_organization_details']);?>"><span class="fas fa-eye"></span></a>
                                <?php if ($internship_application_data['application_status'] == 3) : ?>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal-accept-<?= $internship_application_data['id']; ?>"><span class="fas fa-check"></span></a>
                                &nbsp;
                                <a href="#" data-toggle="modal" data-target="#myModal-reject-<?= $internship_application_data['id']; ?>"><span class="fas fa-times"></span></a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal-accept-<?= $internship_application_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header bg-success">
                                        <h4 class="modal-title text-light">Accept Internship Placement</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to approve your internship placement at <b><?= esc($internship_application_data['organization_name']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/student/dashboard/accept_internship/'.$internship_application_data['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Approve</a>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-window-close"></span>&nbsp;&nbsp;Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal-reject-<?= $internship_application_data['id']; ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title text-light">Reject Internship Placement</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Are you sure you want to reject your internship placement at <b><?= esc($internship_application_data['organization_name']); ?></b>?
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <a href="<?= base_url('public/student/dashboard/reject_internship/'.$internship_application_data['id']);?>" type="button" class="btn btn-danger"><span class="fas fa-times"></span>&nbsp;&nbsp;Reject</a>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-window-close"></span>&nbsp;&nbsp;Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

<br><br>

<div class="card">
    <div class="card-body">
        <h3>Status Flow :</h3>                 
        <p class="text-center"><span class="text-primary">Applied</span> <span class="fas fa-angle-right"></span> <span class="text-warning">In Review</span> <span class="fas fa-angle-right"></span> <span class="text-info">Waiting for Student Approval</span> <span class="fas fa-angle-right"></span> <span class="text-success">Student Accepted</span></p>
    </div>
</div>
<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 20px;
  text-align: center;
}

/* Style the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #333;
}

/* Style the topnav links */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
}

/* Left and right column */
.column.side {
  width: 27%;
}

/* Middle column */
.column.middle {
  width: 70%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column.side, .column.middle {
    width: 100%;
  }
}
.cardnav {
  background-color: #3b6de5;
  padding: 45px;
  margin-top: 20px;
 
}
.cardnav ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.cardnav ul li {
  padding-bottom: 15px;
}

.cardnav ul li a {
  color: #fff;
  font-family: 'Poppins', sans-serif;
  font-size: 15px;
  font-weight: 400;
letter-spacing: 2px;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.header .main-nav ul li a:hover,.header .main-nav ul li a:focus {
  color: #FFF;
  text-decoration: none;
}

.cardcon {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

.name {
    margin: 34px 0;
}

.name h1 {
    font-family: 'Poppins', sans-serif;
    font-size: 30px;
    font-weight: 700;
    color: #FFF;
	letter-spacing: 1px;
    margin: 0 0 5px;
    
}

.name span {
    color: #d8d8e9;
    font-family: 'Poppins', sans-serif;
    font-size: 15px;
    font-weight: 300;
}
.avatar {
    overflow: hidden;
    width: 165px;
    height: 165px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    border-radius: 50%;
}
.section {
    padding: 15px 25px;
    position: relative;
    border-bottom: solid 1px #c18282;
}

html {
  scroll-behavior: smooth;
}

/*-----------------------------------------------------------------*/
/*                      GLOBAL STYLES                              */
/*-----------------------------------------------------------------*/
body {
  color: #1a1a1a;
  background-color: #fafdff;
  font-family: 'Poppins', sans-serif;
  overflow-x: hidden;
}

h1,h2,h3,h4,h5,h6 {
  font-family: 'Poppins', sans-serif;
}

img {
  max-width: 100%;
  height: auto;
}

ul {
  padding-left: 15px;
}

li {
  padding: 0 0 10px;
}


/*-----------------------------------------------------------------*/
/*                      LAYOUT                                     */
/*-----------------------------------------------------------------*/
.wrapper {
  width: 1000px;
  margin: 0 auto;
}

/* === Navigation === */
.header .main-nav ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.header .main-nav ul li {
  padding-bottom: 15px;
}

.header .main-nav ul li a {
  color: #fff;
  font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 400;
letter-spacing: 2px;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.header .main-nav ul li a:hover,.header .main-nav ul li a:focus {
  color: #FFF;
  text-decoration: none;
}




/*-----------------------------------------------------------------*/
/*                      MAIN CONTENT                               */
/*-----------------------------------------------------------------*/
.main-content {
  background: rgb(150, 116, 116);
  width: 740px;
}

.main-content p {
  color: #2c2c2c;
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  font-weight: 300;
  line-height: 28px;
}

/* === Section Configure === */
.main-content section {
  padding: 60px 40px;
  position: relative;
  border-bottom: solid 1px rgb(182, 95, 95);
}

/* === Section Header === */
.main-content section .section-header {
  margin-bottom: 40px;
  position: relative;
}

.main-content section .section-header h2 {
  color: #f74470;
  display: inline-block;
  font-family: 'Poppins', sans-serif;
  font-size: 17px;
  font-weight: 700;
letter-spacing: 3px;
  margin: 0;
  text-transform: uppercase;
  position: relative;

}

/*-----------------------------------------------------------------*/
/*                      SECTION ABOUT                              */
/*-----------------------------------------------------------------*/
.about .intro ul.info {
  margin-top: 40px;
  padding: 0;
  list-style: none;
}

.about .intro ul.info li {
  
  padding: 7px 0;
  font-family: 'Poppins', sans-serif;
  font-weight: 300;
  color: #0C0C0C;
  font-size: 14px;
}

.about .intro ul.info li:first-child {
  padding-top: 0;
}

.about .intro p {
  margin: 0;
}

.about .skills .item {
  margin-top: 40px;
}

.about .skills .item .skill-info {
  margin-bottom: 30px;
}

.about .skills .item .skill-info h3 {
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  margin: 0;
}

.about .skills .item .skill-info span {
  font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 300;
  color: #9C9C9C;
}
.about .resume-download {
  display: block;
  position: absolute;
  right: 0;
  top: -12px;
  background: #5655da;
font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 400;
  color: #FFF;
  padding: 5px 10px;
  border-radius: 3px;
}


/* === Progress Bar === */
.progress {
  height: 4px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #E0E0E0;
  border-radius: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.progress-bar {
  float: left;
  width: 0;
  height: 100%;
  font-size: 12px;
  line-height: 1px;
  color: #fff;
  text-align: center;
  background-color: #f74470;
  -webkit-box-shadow: none;
  box-shadow: none;
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

/*-----------------------------------------------------------------*/
/*                      RESPONSIVE                                  */
/*-----------------------------------------------------------------*/
@media only screen and (min-width: 480px) and (max-width: 767px) {
  .works .item-outer .mix {
      width: 50%;
  }
}

@media only screen and (max-width: 991px) {
  .wrapper {
      width: 100%;
  }

  .header {
      -ms-transform: translateX(-260px);
      -webkit-transform: translateX(-260px);
      transform: translateX(-260px);
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
  }

  .pushed {
      -ms-transform: translateX(0);
      -webkit-transform: translateX(0);
      transform: translateX(0);
  }

  .main-content {
      width: 100%;
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
  }

  .main-pushed {
      -ms-transform: translateX(260px);
      -webkit-transform: translateX(260px);
      transform: translateX(260px);
  }
  .works .item-outer .item {
      height: 280px;
  }
}

@media only screen and (max-width: 767px) {
  .welcome {
      padding: 60px !important;
  }

  .welcome .quote {
      width: initial;
  }

  .facts .fact-item {
      margin-bottom: 20px;
  }

  .resume .resume-item {
      margin-top: 30px !important;
  }

  .blog .item .text {
      margin-top: 25px;
  }
}

@media only screen and (max-width: 480px) {
  .main-content section {
      padding: 60px 30px;
  }
}

@media only screen and (max-height: 600px) {
  .header .name {
      margin: 15px 0;
  }

  .header .main-nav ul li {
      padding-bottom: 10px;
  }

  .header .social-icons {
      margin: 0 0 15px;
  }

  .header .copyright {
      bottom: -18px;
      position: relative;
  }
  .column {
    float: left;
    width: 33.33%;
    padding: 15px;
  }

}

</style>

<!-- Main Wrapper -->

<main class="wrapper">
<form action="<?= base_url('public/student/resume/edit_resume/'.$ResumeModel['id']);?>" method="post">
                <?= csrf_field() ?>
  <div class="row">
	  <div class="column side">
  		<div class="cardnav">
        <div class="avatar">
          <img src="../assets/img/zaki.jpg" > 
			  </div>

		    <div class="form-group">
            <label for="name" class="font-weight-bold">Name</label>
            
            <input type="text" name="name" value="" placeholder="Student Name" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="type" class="font-weight-bold">Course</label>
            <select name="type" class="form-control">
                <option value="" selected disabled>Choose Course ...</option>
                <option value="1">Bachelor In Computer Science (Software Development)</option>
                <option value="2">Bachelor In Computer Science (Database Management)</option>
            </select>
        </div>
      </div>
    </div>

  	<div class="column middle">
  	  <div class="cardcon">
		 	  <div class="section">
				  <section id="about" class="about">
					<h2 class="text-primary">ABOUT ME</h2>	
				</div>

			<div class="intro">
        <div class="form-group">
          <br><label for="job_description" class="font-weight-bold">About Me</label>
          <textarea class="form-control" rows="4" name="objective_statement" value="<?= $ResumeModel['objective_statement']; ?> " placeholder="Describe yourself briefly"></textarea>
        </div>  

        <div class="row">
          <div class="col">
            <label for="matricnumber" class="font-weight-bold">Matric Number</label>
              <input type="text" class="form-control" placeholder="Matric Number">
          </div>
          <div class="col">
            <label for="ic" class="font-weight-bold">IC Number</label>
            <input type="text" class="form-control" placeholder="IC Number">
          </div>
      </div><br>
        <div class="row">
          <div class="col">
            <label for="email" class="font-weight-bold">Email Address</label>
            <input type="text" class="form-control" placeholder="Emai Address">
          </div>
          <div class="col">
            <label for="tel_no" class="font-weight-bold">Phone Number</label>
            <input type="text" class="form-control" placeholder="Phone Number">
          </div>
          <div class="col">
            <label for="age" class="font-weight-bold">Age</label>
            <input type="text" class="form-control" placeholder="Age">
          </div>
        </div>
        <br>

        <section id="achievement" class="achievement">
          <div>
			      <div class="section">
				    <h2 class="text-primary">ACADEMIC ACHIEVEMENT</h2>	
			    </div>

				  <br><P> Last Semester Results </p>
          <div class="row">
            <div class="col">
              <label for="gpa" class="font-weight-bold">GPA</label>
              <input type="text" class="form-control" placeholder="GPA">
            </div>
            <div class="col">
              <label for="cgpa" class="font-weight-bold">CGPA</label>
              <input type="text" class="form-control" placeholder="CGPA">
            </div>
        </section>
        <br>

			  <div class="section">
				  <h2 class="text-primary">SKILL & KNOWLEDGE</h2>        
			  </div>
		      <section id="skill" class="skill">
            <br><label for="cgpa" class="font-weight-bold">Skills</label>
                <input type="text" class="form-control" placeholder="Skill 1">
                <input type="text" class="form-control" placeholder="Skill 1">
                <input type="text" class="form-control" placeholder="Skill 2">
                <input type="text" class="form-control" placeholder="Skill 3">
                <input type="text" class="form-control" placeholder="Skill 4">
          </section>

			    <section id="experience" class="experience">
			      <br><div class="section">
				      <h2 class="text-primary">EXPERIENCE & OTHER ACHIEVEMENT</h2>	
			      </div>
			      <table class="table table-striped">
  				    <thead>
    				    <tr>
      					  <th scope="col">No</th>
      					  <th scope="col">Year</th>
      					  <th scope="col">Achievement</th>
    				    </tr>
  				    </thead>
 				    <tbody>
    				  <tr>
      				  <th scope="row">1</th>
                  <form method="post" action="">
                    <td>
                      <input type="text" class="form-control" placeholder="Year">
                    </td>
                    <td>
                    <input type="text" class="form-control" placeholder="Achievement">
                    </td>
    				  </tr>
  				  </tbody>
			      </table>
			    </section>

			  <br><div class="section">
			    <section id="referees" class="referees">
				  <h2 class="text-primary">REFEREES</h2>	
			  </div>
			    <table class="table table-striped">
      			<thead>
        			<tbody>
            			<tr>
                		<th>NAME</th>
                			<form method="post" action="">
                        <td>
                          <input type="text" class="form-control" placeholder="Referee Name">
                        </td>
            			</tr>
						      <tr>
                		<th>POSITION</th>
                      <form method="post" action="">
                        <td>
                          <input type="text" class="form-control" placeholder="Position">
                        </td>
            			</tr>
						      <tr>
                		<th>TEL.NO</th>
                			<form method="post" action="">
                        <td>
                          <input type="text" class="form-control" placeholder="Phone Number">
                        </td>
            			</tr>
						      <tr>
                		<th>EMAIL</th>
                			<form method="post" action="">
                        <td></td>
            			</tr>
					<tbody>
				</thead>
			</table>
  </div>
  
  <div class="text-right btn-form form-group">
      <button type="submit" name="submit" class="btn btn-primary float-right" >Update Resume</button>
     </div>
     <div class="btn-form form-group">
        <a href="<?= base_url('public/student/resume');?>" class="btn btn-secondary">Back</a>
     </div>
</div>
</main>
<!-- Main Wrapper end -->

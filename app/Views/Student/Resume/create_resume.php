<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 20px;
  text-align: center;
}

/* Style the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #333;
}

/* Style the topnav links */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
}

/* Left and right column */
.column.side {
  width: 27%;
}

/* Middle column */
.column.middle {
  width: 70%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column.side, .column.middle {
    width: 100%;
  }
}
.cardnav {
  background-color: #3b6de5;
  padding: 45px;
  margin-top: 20px;
 
}
.cardnav ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.cardnav ul li {
  padding-bottom: 15px;
}

.cardnav ul li a {
  color: #fff;
  font-family: 'Poppins', sans-serif;
  font-size: 15px;
  font-weight: 400;
letter-spacing: 2px;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.header .main-nav ul li a:hover,.header .main-nav ul li a:focus {
  color: #FFF;
  text-decoration: none;
}

.cardcon {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

.name {
    margin: 34px 0;
}

.name h1 {
    font-family: 'Poppins', sans-serif;
    font-size: 30px;
    font-weight: 700;
    color: #FFF;
	letter-spacing: 1px;
    margin: 0 0 5px;
    
}

.name span {
    color: #d8d8e9;
    font-family: 'Poppins', sans-serif;
    font-size: 15px;
    font-weight: 300;
}
.avatar {
    overflow: hidden;
    width: 165px;
    height: 165px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    border-radius: 50%;
}
.section {
    padding: 15px 25px;
    position: relative;
    border-bottom: solid 1px #c18282;
}

html {
  scroll-behavior: smooth;
}

/*-----------------------------------------------------------------*/
/*                      GLOBAL STYLES                              */
/*-----------------------------------------------------------------*/
body {
  color: #1a1a1a;
  background-color: #fafdff;
  font-family: 'Poppins', sans-serif;
  overflow-x: hidden;
}

h1,h2,h3,h4,h5,h6 {
  font-family: 'Poppins', sans-serif;
}

img {
  max-width: 100%;
  height: auto;
}

ul {
  padding-left: 15px;
}

li {
  padding: 0 0 10px;
}


/*-----------------------------------------------------------------*/
/*                      LAYOUT                                     */
/*-----------------------------------------------------------------*/
.wrapper {
  width: 1000px;
  margin: 0 auto;
}

/* === Navigation === */
.header .main-nav ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.header .main-nav ul li {
  padding-bottom: 15px;
}

.header .main-nav ul li a {
  color: #fff;
  font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 400;
letter-spacing: 2px;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.header .main-nav ul li a:hover,.header .main-nav ul li a:focus {
  color: #FFF;
  text-decoration: none;
}




/*-----------------------------------------------------------------*/
/*                      MAIN CONTENT                               */
/*-----------------------------------------------------------------*/
.main-content {
  background: rgb(150, 116, 116);
  width: 740px;
}

.main-content p {
  color: #2c2c2c;
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  font-weight: 300;
  line-height: 28px;
}

/* === Section Configure === */
.main-content section {
  padding: 60px 40px;
  position: relative;
  border-bottom: solid 1px rgb(182, 95, 95);
}

/* === Section Header === */
.main-content section .section-header {
  margin-bottom: 40px;
  position: relative;
}

.main-content section .section-header h2 {
  color: #f74470;
  display: inline-block;
  font-family: 'Poppins', sans-serif;
  font-size: 17px;
  font-weight: 700;
letter-spacing: 3px;
  margin: 0;
  text-transform: uppercase;
  position: relative;

}

/*-----------------------------------------------------------------*/
/*                      SECTION ABOUT                              */
/*-----------------------------------------------------------------*/
.about .intro ul.info {
  margin-top: 40px;
  padding: 0;
  list-style: none;
}

.about .intro ul.info li {
  
  padding: 7px 0;
  font-family: 'Poppins', sans-serif;
  font-weight: 300;
  color: #0C0C0C;
  font-size: 14px;
}

.about .intro ul.info li:first-child {
  padding-top: 0;
}

.about .intro p {
  margin: 0;
}

.about .skills .item {
  margin-top: 40px;
}

.about .skills .item .skill-info {
  margin-bottom: 30px;
}

.about .skills .item .skill-info h3 {
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  margin: 0;
}

.about .skills .item .skill-info span {
  font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 300;
  color: #9C9C9C;
}
.about .resume-download {
  display: block;
  position: absolute;
  right: 0;
  top: -12px;
  background: #5655da;
font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 400;
  color: #FFF;
  padding: 5px 10px;
  border-radius: 3px;
}


/* === Progress Bar === */
.progress {
  height: 4px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #E0E0E0;
  border-radius: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.progress-bar {
  float: left;
  width: 0;
  height: 100%;
  font-size: 12px;
  line-height: 1px;
  color: #fff;
  text-align: center;
  background-color: #f74470;
  -webkit-box-shadow: none;
  box-shadow: none;
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

/*-----------------------------------------------------------------*/
/*                      RESPONSIVE                                  */
/*-----------------------------------------------------------------*/
@media only screen and (min-width: 480px) and (max-width: 767px) {
  .works .item-outer .mix {
      width: 50%;
  }
}

@media only screen and (max-width: 991px) {
  .wrapper {
      width: 100%;
  }

  .header {
      -ms-transform: translateX(-260px);
      -webkit-transform: translateX(-260px);
      transform: translateX(-260px);
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
  }

  .pushed {
      -ms-transform: translateX(0);
      -webkit-transform: translateX(0);
      transform: translateX(0);
  }

  .main-content {
      width: 100%;
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
  }

  .main-pushed {
      -ms-transform: translateX(260px);
      -webkit-transform: translateX(260px);
      transform: translateX(260px);
  }
  .works .item-outer .item {
      height: 280px;
  }
}

@media only screen and (max-width: 767px) {
  .welcome {
      padding: 60px !important;
  }

  .welcome .quote {
      width: initial;
  }

  .facts .fact-item {
      margin-bottom: 20px;
  }

  .resume .resume-item {
      margin-top: 30px !important;
  }

  .blog .item .text {
      margin-top: 25px;
  }
}

@media only screen and (max-width: 480px) {
  .main-content section {
      padding: 60px 30px;
  }
}

@media only screen and (max-height: 600px) {
  .header .name {
      margin: 15px 0;
  }

  .header .main-nav ul li {
      padding-bottom: 10px;
  }

  .header .social-icons {
      margin: 0 0 15px;
  }

  .header .copyright {
      bottom: -18px;
      position: relative;
  }
  .column {
    float: left;
    width: 33.33%;
    padding: 15px;
  }
}

</style>

<!-- Main Wrapper -->
<main class="wrapper">

    <?php if (!empty($success)) : ?>
        <div class="alert alert-success" role="success">
                <?= $success ?>
        </div>
    <?php endif ?>
    
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error) ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <?php if (active_link(['Resume'], ['create_resume'])) : ?>
    <div class="alert alert-warning" role="alert">
      Please update all your resume details first
    </div>
    <?php endif ?>

<form action="<?= base_url('public/student/resume/create_resume'); ?>" method="post" enctype="multipart/form-data">
  <div class="row">
	  <div class="column side">
  		<div class="cardnav">
        <div class="avatar">
        <img src="<?= ($profile_picture != null) ? base_url('public/'.$profile_picture['path'].$profile_picture['name']) : base_url('public/profile_picture/default_profile_picture.jpg'); ?>" alt="Profile Picture" class="mx-auto rounded" style="max-width: 200px; max-height: 200px;">
			  </div>

        <div class="custom-file">
            <input type="file" name="file" class="custom-file-input" id="customFile" accept="image/*">
            <label class="custom-file-label" for="customFile">Profile Picture</label>
        </div>

		    <!-- <div class="form-group">

            <label for="name" class="font-weight-bold">Name</label>
            <input type="text" name="name" placeholder="Student Name" class="form-control"/>
            
        </div>

        <div class="form-group">
            <label for="type" class="font-weight-bold">Course</label>
            <select name="id_course" class="form-control">
                <?php foreach ( $AlCourseModel  as $course_data) : ?>
                  <option value="<?= $course_data['id'] ?>"><?=$course_data['name']?></option>
                <?php endforeach; ?>
            </select>
        </div> -->

        <div class="name">
          <h1><?= $personal_detail['name']; ?></h1>
          <span><?=$personal_detail['course_name']; ?></span><br>
        </div>

      </div>
    </div>

  	<div class="column middle">
  	  <div class="cardcon">
		 	  <div class="section">
				  <section id="about" class="about">
					<h2 class="text-primary">ABOUT ME</h2>	
				</div>

			<div class="intro">
        <div class="form-group">
          <br><label for="objective_statement" class="font-weight-bold">About Me</label>
          <textarea class="form-control" rows="4" name="objective_statement" placeholder="Describe yourself briefly" required><?= esc($resume['objective_statement']); ?></textarea>
        </div>  

        <div class="row">
          <div class="col">
            <label for="user_no" class="font-weight-bold">Matric Number</label>
              <input type="text" name="user_no" class="form-control" placeholder="Matric Number" value="<?= esc($personal_detail['user_no']); ?>" disabled>
          </div>
          <div class="col">
            <label for="nric_no" class="font-weight-bold">IC Number</label>
            <input type="text" class="form-control" name="nric_no" placeholder="IC Number" value="<?= esc($personal_detail['nric_no']); ?>" disabled>
          </div>
      </div><br>
        <div class="row">
          <div class="col">
            <label for="tel_no" class="font-weight-bold">Phone Number</label>
            <input type="text" name='tel_no' class="form-control" placeholder="Phone Number" value="<?= esc($personal_detail['tel_no']); ?>">
          </div>
        </div><br>
        <div class="form-group">
            <label for="address" class="font-weight-bold">Address</label>
            <input type="text" name='address'class="form-control"  placeholder="Address" value="<?= esc($personal_detail['address']); ?>">
          </div>
        <br>

        <section id="achievement" class="achievement">
          <div>
			      <div class="section">
				    <h2 class="text-primary">ACADEMIC ACHIEVEMENT</h2>	
			    </div>

				  <br><P> Last Semester Results </p>
          <div class="row">
            <div class="col">
              <label for="cgpa" class="font-weight-bold">CGPA</label>
              <input type="text" name="cgpa" class="form-control" placeholder="CGPA" value="<?= esc($cgpa['cgpa']); ?>">
            </div>
        </section>
        <br>

			  <div class="section">
				  <h2 class="text-primary">SKILL & KNOWLEDGE</h2>        
			  </div>
		      <section id="skill" class="skill">
            
          </section>
          <br>
          <!-- <div class="progress progress-striped active">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
        </div>
    </div> -->

  <div id="main-container-skill">
    <div class="container-item-skill-0 row" id="">
      <div class="col-sm-4">
        <input type="text" name="skill[name][0]" value="<?= $skills[0]['name'] ?>" class="form-control" placeholder="Skill" required>
      </div>
      <div class="col-sm-6">
          <label> BASIC </label>
        <label><input id="skills-and-knowledge" name="skill[scale][0]" class="progres" type="radio" value="1" <?= ($skills[0]['scale'] == 1) ? 'checked' : ''; ?> required> 1</label>
        <label><input id="skills-and-knowledge" name="skill[scale][0]" class="progres" type="radio" value="2" <?= ($skills[0]['scale'] == 2) ? 'checked' : ''; ?> required> 2</label>
        <label><input id="skills-and-knowledge" name="skill[scale][0]" class="progres" type="radio" value="3" <?= ($skills[0]['scale'] == 3) ? 'checked' : ''; ?> required> 3</label>
        <label><input id="skills-and-knowledge" name="skill[scale][0]" class="progres" type="radio" value="4" <?= ($skills[0]['scale'] == 4) ? 'checked' : ''; ?> required> 4</label>
        <label><input id="skills-and-knowledge" name="skill[scale][0]" class="progres" type="radio" value="5" <?= ($skills[0]['scale'] == 5) ? 'checked' : ''; ?> required> 5</label>
        <label> ADVANCED </label>
      </div>
      <div class="col-sm-2">
        <!-- <div class="button-group">
            <button class="btn btn-md btn-danger remove-item" type="button">-</button>
        </div> -->
      </div>
      <br>
      <br>
    </div>

    <?php if (!empty($skills) && is_array($skills)) : ?><?php $x=0 ?>
        <?php foreach ($skills as $skills_data) : ?>
        <?php if ($x != 0) { ?>
          <div class="container-item-skill-<?= $x; ?> row" id="">
            <div class="col-sm-4">
              <input type="text" name="skill[name][<?= $x; ?>]" value="<?= $skills_data['name'] ?>" class="form-control" placeholder="Skill" required>
            </div>
            <div class="col-sm-6">
                <label> BASIC </label>
              <label><input id="skills-and-knowledge" name="skill[scale][<?= $x; ?>]" class="progres" type="radio" value="1" <?= ($skills_data['scale'] == 1) ? 'checked' : ''; ?> required> 1</label>
              <label><input id="skills-and-knowledge" name="skill[scale][<?= $x; ?>]" class="progres" type="radio" value="2" <?= ($skills_data['scale'] == 2) ? 'checked' : ''; ?> required> 2</label>
              <label><input id="skills-and-knowledge" name="skill[scale][<?= $x; ?>]" class="progres" type="radio" value="3" <?= ($skills_data['scale'] == 3) ? 'checked' : ''; ?> required> 3</label>
              <label><input id="skills-and-knowledge" name="skill[scale][<?= $x; ?>]" class="progres" type="radio" value="4" <?= ($skills_data['scale'] == 4) ? 'checked' : ''; ?> required> 4</label>
              <label><input id="skills-and-knowledge" name="skill[scale][<?= $x; ?>]" class="progres" type="radio" value="5" <?= ($skills_data['scale'] == 5) ? 'checked' : ''; ?> required> 5</label>
              <label> ADVANCED </label>
            </div>
            <div class="col-sm-2">
              <div class="button-group">
                  <button id="remove-skill" class="btn btn-md btn-danger remove-skill-<?=$x?>" type="button" onclick="removeSkill(<?= $x ?>)">-</button>
              </div>
            </div><?php $x++; ?>
            <br>
            <br>
          </div>
          <?php }
            else
            {
              $x++;
            }
          ?>
          <?php endforeach; ?>
    <?php endif ?>

    </div>
      <button id="add-more-skill" class="btn btn-md btn-info" type="button">Add More</button>
          

			    
          <section id="experience" class="experience">
			      <br><div class="section">
            <h2 class="text-primary">ACHIEVEMENT</h2>		
			      </div>
              <div class="container pt-4"> 
                <div class="table-responsive"> 
                <table class="table table-striped">
                    <thead> 
                      <tr>
                        <th scope="col">Year</th>
                        <th scope="col">Achievement</th>
                        <th scope="col">&nbsp</th>
    				          </tr>
                    </thead> 
                    <tbody id="tbodyachive"> 
                       <tr>  
                       <td><input required type="text" name="achievement[year][]" class="form-control" placeholder="Year" value="<?= $achievements[0]['year'] ?>"></td>
                       <td><input required type="text" name="achievement[name][]" class="form-control" placeholder="Achievement" value="<?= $achievements[0]['name'] ?>"></td>
                      <td><button class="btn btn-md btn-info " id="addBtnAchieve" type="button">+</button> </td>
                      </tr> 
                      <?php if (!empty($achievements) && is_array($achievements)) : ?><?php $x=0 ?>
                        <?php foreach ($achievements as $achievements_data) : ?>
                          <?php if ($x != 0) { ?>
                            <tr>
                        <td><input required type="text" name="achievement[year][]" class="form-control" placeholder="Year" value="<?= $achievements_data['year'] ?>"></td>
                       <td><input required type="text" name="achievement[name][]" value="<?= $achievements_data['name'] ?>" class="form-control" placeholder="Achievement"></td>
              <td class="text-center"> 
                <button class="btn btn-danger remove" 
                  type="button">-</button> 
                </td> 
                
              </tr>
                          <?php $x++; ?>
                          <?php }
                            else
                            {
                              $x++;
                            }
                          ?>
                          <?php endforeach; ?>
                    <?php endif ?>
                    </tbody> 
                  </table> 
                </div> 
              </div> 
          </section>

          
          <section id="experience" class="experience">
			      <br><div class="section">
				      <h2 class="text-primary">WORKING EXPERIENCE</h2>	
			      </div>
              <div class="container pt-4"> 
                <div class="table-responsive"> 
                <table class="table table-striped">
                    <thead> 
                      <tr>
                        <th scope="col">Start Year</th>
                        <th scope="col">End Year</th>
                        <th scope="col">Job Title</th>
                        <th scope="col">Position</th>
                        <th scope="col">&nbsp</th>
    				          </tr>
                    </thead> 
                    <tbody id="tbody"> 
                    
                       <tr>  
                      <td><input required type="text" name="experience[start_year][]" class="form-control" placeholder="Start Year" value="<?= $experiences[0]['start_year']?>"></td>
                      <td><input required type="text" name="experience[end_year][]" class="form-control" placeholder="End Year" value="<?= $experiences[0]['end_year']?>"></td>
                      <td><input required type="text" name="experience[name][]" class="form-control" placeholder="Job Tittle" value="<?= $experiences[0]['name']?>"></td>
                      <td><input required type="text" name="experience[position][]" class="form-control" placeholder="Position" value="<?= $experiences[0]['position']?>"></td>
                      <td><button class="btn btn-md btn-info" id="addBtn" type="button">+</button> </td>
                      </tr> 

                      <?php if (!empty($experiences) && is_array($experiences)) : ?><?php $x=0 ?>
                        <?php foreach ($experiences as $experiences_data) : ?>
                          <?php if ($x != 0) { ?>

                            <tr>
                              <td><input required type="text" name="experience[start_year][]" class="form-control" placeholder="Start Year" value="<?= $experiences_data['start_year'] ?>"></td><td><input required type="text" name="experience[end_year][]" class="form-control" placeholder="End Year" value="<?= $experiences_data['end_year'] ?>"></td><td><input required type="text" name="experience[name][]" class="form-control" placeholder="Job Tittle" value="<?= $experiences_data['name'] ?>"></td><td><input required type="text" name="experience[position][]" class="form-control" placeholder="Position" value="<?= $experiences_data['position'] ?>"></td>
                                <td class="text-center"> 
                                  <button class="btn btn-danger remove"
                                    type="button">-</button> 
                                  </td> 
                                  
                                </tr>

                            <?php $x++; ?>
                          <?php }
                            else
                            {
                              $x++;
                            }
                          ?>
                          <?php endforeach; ?>
                    <?php endif ?>

                    </tbody> 
                  </table> 
                </div> 
              </div> 
          </section>


			  <br><div class="section">
			    <section id="referees" class="referees">
				  <h2 class="text-primary">REFEREES</h2>	
			  </div>
			    <table class="table table-striped">
      			<thead>
        			<tbody>
            			<tr>
                		<th>NAME</th>
                			<form method="post" action="">
                        <td>
                          <input type="text" class="form-control" placeholder="Referee Name" value="<?= $coordinator['name'] ?>" disabled>
                        </td>
            			</tr>
						      <!-- <tr>
                		<th>POSITION</th>
                      <form method="post" action="">
                        <td>
                          <input type="text" class="form-control" placeholder="Position" value="<?= $coordinator['id_course'] ?>" disabled>
                        </td>
            			</tr> -->
						      <tr>
                		<th>TEL.NO</th>
                			<form method="post" action="">
                        <td>
                          <input type="text" class="form-control" placeholder="Phone Number" value="<?= $coordinator['tel_no'] ?>" disabled>
                        </td>
            			</tr>
						      <tr>
                		<th>EMAIL</th>
                        <td><input type="text" class="form-control" placeholder="Email" value="<?= $coordinator['email'] ?>" disabled></td>
            			</tr>
					<tbody>
				</thead>
			</table>

      
  </div>
  <div class="text-right btn-form form-group">
      <button type="submit" name="submit" class="btn btn-primary float-right" >Update Resume</button>
     </div>
     <div class="btn-form form-group">
        <a href="<?= base_url('public/student/resume');?>" class="btn btn-secondary">Back</a>
     </div>
  
  
</div>
</main>
<!-- Main Wrapper end -->

<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 20px;
  text-align: center;
}

/* Style the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #333;
}

/* Style the topnav links */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
}

/* Left and right column */
.column.side {
  width: 27%;
  

}

/* Middle column */
.column.middle {
  width: 70%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column.side, .column.middle {
    width: 100%;
  }
}
.cardnav {
  background-color: #3b6de5;
  padding: 45px;
  margin-top: 20px;
 
}
.cardnav ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.cardnav ul li {
  padding-bottom: 15px;
}

.cardnav ul li a {
  color: #fff;
  font-family: 'Poppins', sans-serif;
  font-size: 15px;
  font-weight: 400;
letter-spacing: 2px;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.header .main-nav ul li a:hover,.header .main-nav ul li a:focus {
  color: #FFF;
  text-decoration: none;
}

.cardcon {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

.name {
    margin: 34px 0;
}

.name h1 {
    font-family: 'Poppins', sans-serif;
    font-size: 30px;
    font-weight: 700;
    color: #FFF;
	letter-spacing: 1px;
    margin: 0 0 5px;
    
}

.name span {
    color: #d8d8e9;
    font-family: 'Poppins', sans-serif;
    font-size: 15px;
    font-weight: 300;
}
.avatar {
    overflow: hidden;
    width: 165px;
    height: 165px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    border-radius: 50%;
}
.section {
    padding: 15px 25px;
    position: relative;
    border-bottom: solid 1px #c18282;
}

html {
  scroll-behavior: smooth;
}

/* CONTENT */
/*
Template Name: Blvck - Personal vCard & Resume Template
Version: 1.0
Author: PxlSolutions
Author URI: http://themeforest.net/user/pxlsolutions
Description: CV, Resume, vCard HTML Template

[TABLE OF CONTENTS]

01. Global Styles
02. Preloader
03. Layout
04. Header
05. Main Content
08. Section Resume
09. Section Works
10. Section Testimonials
11. Section Blog
12. Section Customers
13. Section Contact
14. Other Elements
15. Responsive
*/
/*-----------------------------------------------------------------*/
/*                      GLOBAL STYLES                              */
/*-----------------------------------------------------------------*/
body {
  color: #1a1a1a;
  background-color: #fafdff;
  font-family: 'Poppins', sans-serif;
  overflow-x: hidden;
}

h1,h2,h3,h4,h5,h6 {
  font-family: 'Poppins', sans-serif;
}

img {
  max-width: 100%;
  height: auto;
}

ul {
  padding-left: 15px;
}

li {
  padding: 0 0 10px;
}


/*-----------------------------------------------------------------*/
/*                      LAYOUT                                     */
/*-----------------------------------------------------------------*/
.wrapper {
  width: 1000px;
  margin: 0 auto;
}

/* === Navigation === */
.header .main-nav ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.header .main-nav ul li {
  padding-bottom: 15px;
}

.header .main-nav ul li a {
  color: #fff;
  font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 400;
letter-spacing: 2px;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.header .main-nav ul li a:hover,.header .main-nav ul li a:focus {
  color: #FFF;
  text-decoration: none;
}




/*-----------------------------------------------------------------*/
/*                      MAIN CONTENT                               */
/*-----------------------------------------------------------------*/
.main-content {
  background: rgb(150, 116, 116);
  width: 740px;
}

.main-content p {
  color: #2c2c2c;
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  font-weight: 300;
  line-height: 28px;
}

/* === Section Configure === */
.main-content section {
  padding: 60px 40px;
  position: relative;
  border-bottom: solid 1px rgb(182, 95, 95);
}

/* === Section Header === */
.main-content section .section-header {
  margin-bottom: 40px;
  position: relative;
}

.main-content section .section-header h2 {
  color: #f74470;
  display: inline-block;
  font-family: 'Poppins', sans-serif;
  font-size: 17px;
  font-weight: 700;
letter-spacing: 3px;
  margin: 0;
  text-transform: uppercase;
  position: relative;

}

/*-----------------------------------------------------------------*/
/*                      SECTION ABOUT                              */
/*-----------------------------------------------------------------*/
.about .intro ul.info {
  margin-top: 40px;
  padding: 0;
  list-style: none;
}

.about .intro ul.info li {
  
  padding: 7px 0;
  font-family: 'Poppins', sans-serif;
  font-weight: 300;
  color: #0C0C0C;
  font-size: 14px;
}



.about .intro ul.info li:first-child {
  padding-top: 0;
}

.about .intro p {
  margin: 0;
}

.about .skills .item {
  margin-top: 40px;
}

.about .skills .item .skill-info {
  margin-bottom: 30px;
}

.about .skills .item .skill-info h3 {
  font-family: 'Poppins', sans-serif;
  font-size: 14px;
  margin: 0;
}

.about .skills .item .skill-info span {
  font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 300;
  color: #9C9C9C;
}
.about .resume-download {
  display: block;
  position: absolute;
  right: 0;
  top: -12px;
  background: #5655da;
font-family: 'Poppins', sans-serif;
  font-size: 12px;
  font-weight: 400;
  color: #FFF;
  padding: 5px 10px;
  border-radius: 3px;
}


/* === Progress Bar === */
.progress {
  height: 4px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #E0E0E0;
  border-radius: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.progress-bar {
  float: left;
  width: 0;
  height: 100%;
  font-size: 12px;
  line-height: 1px;
  color: #fff;
  text-align: center;
  background-color: #f74470;
  -webkit-box-shadow: none;
  box-shadow: none;
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

/* === Form Control === */
.form-control {
  display: block;
  width: 100%;
  height: 50px;
  padding: 0;
  font-size: 14px;
  font-weight: 300;
  line-height: 1.6;
  color: #555;
  background-color: #fff;
  background-image: none;
  border-bottom: solid 1px #0C0C0C;
  border-top: 0;
  border-left: 0;
  border-right: 0;
  border-radius: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.form-control:focus {
  border-color: #C7C7C7;
  outline: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.form-group {
  margin-bottom: 25px;
}

/* === Owl Configure === */
.owl-theme .owl-controls .owl-page span {
  width: 10px;
  height: 10px;
  margin: 5px 3px;
  filter: Alpha(Opacity=20);
  opacity: .2;
  background: #f8557d;
}

.owl-theme .owl-controls {
  margin-top: 30px;
}

/*-----------------------------------------------------------------*/
/*                      RESPONSIVE                                  */
/*-----------------------------------------------------------------*/
@media only screen and (min-width: 480px) and (max-width: 767px) {
  .works .item-outer .mix {
      width: 50%;
  }
}

@media only screen and (max-width: 991px) {
  .wrapper {
      width: 100%;
  }

  .header {
      -ms-transform: translateX(-260px);
      -webkit-transform: translateX(-260px);
      transform: translateX(-260px);
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
  }

  .pushed {
      -ms-transform: translateX(0);
      -webkit-transform: translateX(0);
      transform: translateX(0);
  }

  .main-content {
      width: 100%;
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
  }

  .main-pushed {
      -ms-transform: translateX(260px);
      -webkit-transform: translateX(260px);
      transform: translateX(260px);
  }
  .works .item-outer .item {
      height: 280px;
  }
}

@media only screen and (max-width: 767px) {
  .welcome {
      padding: 60px !important;
  }

  .welcome .quote {
      width: initial;
  }

  .facts .fact-item {
      margin-bottom: 20px;
  }

  .resume .resume-item {
      margin-top: 30px !important;
  }

  .blog .item .text {
      margin-top: 25px;
  }
}

@media only screen and (max-width: 480px) {
  .main-content section {
      padding: 60px 30px;
  }
}

@media only screen and (max-height: 600px) {
  .header .name {
      margin: 15px 0;
  }

  .header .main-nav ul li {
      padding-bottom: 10px;
  }

  .header .social-icons {
      margin: 0 0 15px;
  }

  .header .copyright {
      bottom: -18px;
      position: relative;
  }
  .column {
    float: left;
    width: 33.33%;
    padding: 15px;
  }

  .tasks{
	background-color: #F6F8F8;
	padding: 10px;
	border-radius: 5px;
	margin-top: 10px;
}

  
}

</style>

<!-- Main Wrapper -->
<main class="wrapper">



	<!-- Main Content -->
    
<div class="row">
	<div class="column side">
  		<div class="cardnav">
  			<div class="avatar">
          <img src="<?= ($profile_picture != null) ? base_url('public/'.$profile_picture['path'].$profile_picture['name']) : base_url('public/profile_picture/default_profile_picture.jpg'); ?>" alt="Profile Picture" class="mx-auto rounded" style="max-width: 200px; max-height: 200px;">
			</div>

			<div class="name">
				<h1><?= $personalDetailsModel['name']; ?></h1>
				<span><?=$AlCourseModel['name']; ?></span><br>
			</div>

			<ul class="navigation">
				
				<li><a href="#about">About Me</a></li>
				<li><a href="#achievement">Academic Achievement</a></li>
				<li><a href="#skill">Skill & Knowledge</a>
				<li><a href="#experience">Experience & Other Achievement</a>
				<li><a href="#referees">Referees</a>
			</ul>

			
		</div>
  	</div>

  
  	<div class="column middle">
  		<div class="cardcon">
        <div class="text-right btn-form form-group">
        <button onclick="window.print()" class="btn btn-info fas fa-print">Print Resume</button>
      </div>
		 	<div class="section">
      
				  <section id="about" class="about">
					<h2 class="text-primary">ABOUT ME</h2>	
			</div>

			<!-- Intro -->
			<div class="intro">
				
				<br><p><?= $ResumeModel['objective_statement']; ?> </p>
      

				<ul class="info">
          <li><strong>Faculty:</strong> <?//=$AlFacultyModel['name']; ?> Faculty of Information and Communications Technology </li>
					<li><strong>Matric Number:</strong> <?= $personalDetailsModel['user_no']; ?></li>
					<li><strong>IC Number:</strong> <?= $personalDetailsModel['nric_no']; ?></li>
					<li><strong>Email:</strong> <?= $UserModel['email']; ?></li>
					<li><strong>Phone:</strong> <?= $personalDetailsModel['tel_no']; ?></li>
					<li><strong>Address:</strong> <?= $personalDetailsModel['address']; ?></li>
				</ul>
			</div>
			</section>

			<!-- Intro end -->
			<section id="achievement" class="achievement">
			<div class="section">
				<h2 class="text-primary">ACADEMIC ACHIEVEMENT</h2>	
			</div>

				<br><P> Last Semester Results </p>

			<div class="container-fluid">
  				<div class="row">
      				<div class="col-sm-6"><strong>CGPA: <?= $cgpa['cgpa']; ?> </strong> </div>
    			</div> <br>
			</div>

			<div class="section">
				<h2 class="text-primary">SKILL & KNOWLEDGE</h2>	
			</div>
			</section>
		

			<!-- Skills -->
			<section id="skill" class="skill">
			<div class="skills">

				<div class="row">

        <?php if (!empty($skills) && is_array($skills)) : ?>
          <?php foreach ($skills as $skills_data) : ?>

              <div class="col-md-6 col-sm-6 col-xs-12 item"><br>
                <div class="skill-info clearfix">
                  <h3 class="pull-left"><?= esc($skills_data['name']); ?></h3>
                  <span class="pull-right"><?= esc(student_skills($skills_data['scale'], 1)); ?></span>
                </div>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="<?= esc(student_skills($skills_data['scale'], 2)); ?>"
                  aria-valuemin="0" aria-valuemax="100" style="width:<?= esc(student_skills($skills_data['scale'], 1)); ?>">
                  </div>
                </div>
              </div>

            <?php endforeach; ?>
          <?php endif ?>

				</div>
			</div>
			</section>

			<!-- <section id="experience" class="experience">
			<br><div class="section">
				<h2 class="text-primary">EDUCATION</h2>	
			</div>
			<table class="table table-striped">
  				<thead>
    				<tr>
      					<th scope="col">No</th>
      					<th scope="col">Year (From)</th>
                <th scope="col">Year (Until)</th>
      					<th scope="col">Institution Name</th>
    				</tr>
  				</thead>
 				<tbody>
    				<tr>
            <th class="text-center"><?= ++$no ?></th>
      					<td> <?= $personalDetailsModel['start_year'];?> </td>
                <td><?= $personalDetailsModel['end_year'];?></td>
      					<td><?= $personalDetailsModel['edu_name'];?></td>
    				</tr>
  				</tbody>
			</table>
      <div>
			</section> -->

      <section id="education" class="experience">
			  <br><div class="section">
				  <h2 class="text-primary">ACHIEVEMENT</h2>	
			    </div>
			    <table class="table table-striped">
  				  <thead>
    				  <tr>
                <!-- <th scope="col">No</th> -->
      					<th scope="col">Year</th>
      					<th scope="col">Achievement</th>
    				  </tr>
  				  </thead>
 				  <tbody>
           <?php if (!empty($achievements) && is_array($achievements)) : ?>
          <?php foreach ($achievements as $achievements_data) : ?>
    				<tr>
            <!-- <th scope="row"><?= ++$x; ?></th> -->
      					<td><?= $achievements_data['year'];?></td>
      					<td><?= $achievements_data['name'];?></td>
    				</tr>
           <?php endforeach; ?>
          <?php endif ?>
  				</tbody>
			</table>
			</section>

      <section id="education" class="experience">
			<br><div class="section">
				<h2 class="text-primary">WORKING EXPERIENCE</h2>	
			</div>
			<table class="table table-striped">
  				<thead>
    				<tr>
      					<!-- <th scope="col">No</th> -->
      					<th scope="col">Start Year</th>
                <th scope="col">End Year</th>
      					<th scope="col">Experience</th>
    				</tr>
  				</thead>
 				<tbody>
         <?php if (!empty($experiences) && is_array($experiences)) : ?>
          <?php foreach ($experiences as $experiences_data) : ?>
    				<tr>
      					<!-- <th scope="row">1</th> -->
      					<td><?= $experiences_data['start_year'];?></td>
                <td><?= $experiences_data['end_year'];?></td>
                <td><?= $experiences_data['position'];?></td>
    				</tr>
            <?php endforeach; ?>
          <?php endif ?>
  				</tbody>
			</table>
			</section>

      

			<br><div class="section">
			<section id="referees" class="referees">
				<h2 class="text-primary">REFEREES</h2>	
			</div>
			<table class="table table-striped">
      			<thead>
        			<tbody>
            			<tr>
                			<th>NAME</th>
                			<td><?= esc($coordinator['name']); ?></td>
            			</tr>
						<!-- <tr>
                			<th>POSITION</th>
                			<td>Lecturer</td>
            			</tr> -->
						<tr>
                			<th>TEL.NO</th>
                			<td><?= esc($coordinator['tel_no']); ?></td>
            			</tr>
						<tr>
                			<th>EMAIL</th>
                			<td><?= esc($coordinator['email']); ?></td>
            			</tr>
					<tbody>
				</thead>
			</table>
			<!-- Skills end -->
		</section>
		<!-- Section About end -->
  </div>
  <br><div class="btn-block pull-right ">
      <a href="<?= base_url('public/student/resume/edit_resume/'.$ResumeModel['id']);?>" class="btn btn-secondary pull-right" style="float: right;">Edit Profile</a>
    </div>
</div>
</main>
<!-- Main Wrapper end -->

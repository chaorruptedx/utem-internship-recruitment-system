<br>
<br>
<footer>
	<div class="copyrights">
		<p>&copy; <?= date('Y') ?> UTeM Internship Management System</p>
	</div>
</footer>

<script src="<?php echo base_url('public/assets/bootstrap/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/select2/js/select2.full.min.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#data-table').DataTable();
		var element = document.getElementById("data-table");
		if (element !== null)
  			element.classList.add("table-striped", "table-hover", "table-dark", "text-center");

		$('select').select2();

		$("select#compare-organization-1").on("change", function() {

			var id_organization_1 = $("select#compare-organization-1").val();

			url = <?= json_encode(base_url('public/student/studentapplicants/compare_organization_data')) ?>;
			

			var request = $.ajax({
				url: url,
				method: "POST",
				data: {id_organization: id_organization_1},
				dataType: "json"
			});

			request.done( function(data) {

				$('td#compare-1-name').empty().append(data.name);
				$('td#compare-1-sector').empty().append(data.type);
				$('td#compare-1-address').empty().append(data.address);
				$('td#compare-1-state').empty().append(data.state_name);
				$('td#compare-1-days').empty().append(data.start_day);
				$('td#compare-1-hours').empty().append(data.open_hour);
				$('td#compare-1-email').empty().append(data.email);
				$('td#compare-1-tel').empty().append(data.tel_no);

				$('a#compare-1-view').css("visibility", "visible").attr("href", "view_organization/"+data.id);
			});
		});

		$("select#compare-organization-2").on("change", function() {

			var id_organization_2 = $("select#compare-organization-2").val();

			url = <?= json_encode(base_url('public/student/studentapplicants/compare_organization_data')) ?>;
			

			var request = $.ajax({
				url: url,
				method: "POST",
				data: {id_organization: id_organization_2},
				dataType: "json"
			});

			request.done( function(data) {

				$('td#compare-2-name').empty().append(data.name);
				$('td#compare-2-sector').empty().append(data.type);
				$('td#compare-2-address').empty().append(data.address);
				$('td#compare-2-state').empty().append(data.state_name);
				$('td#compare-2-days').empty().append(data.start_day);
				$('td#compare-2-hours').empty().append(data.open_hour);
				$('td#compare-2-email').empty().append(data.email);
				$('td#compare-2-tel').empty().append(data.tel_no);

				$('a#compare-2-view').css("visibility", "visible").attr("href", "view_organization/"+data.id);
			});
		});

		$("select#compare-organization-3").on("change", function() {

			var id_organization_3 = $("select#compare-organization-3").val();

			url = <?= json_encode(base_url('public/student/studentapplicants/compare_organization_data')) ?>;
			

			var request = $.ajax({
				url: url,
				method: "POST",
				data: {id_organization: id_organization_3},
				dataType: "json"
			});

			request.done( function(data) {

				$('td#compare-3-name').empty().append(data.name);
				$('td#compare-3-sector').empty().append(data.type);
				$('td#compare-3-address').empty().append(data.address);
				$('td#compare-3-state').empty().append(data.state_name);
				$('td#compare-3-days').empty().append(data.start_day);
				$('td#compare-3-hours').empty().append(data.open_hour);
				$('td#compare-3-email').empty().append(data.email);
				$('td#compare-3-tel').empty().append(data.tel_no);

				$('a#compare-3-view').css("visibility", "visible").attr("href", "view_organization/"+data.id);
			});
		});
	} );
</script>
</script>

<script> 
    $(document).ready(function () { 
  
      // Denotes total number of rows 
      var rowIdx = 0; 
      var max_fields_limit      = 5; //set limit for maximum input fields
     var x = 1; //initialize counter for text box
     var experiences_count = '<?= $experiences_count; ?>';
      if (experiences_count != null)
      {
        x = experiences_count;
      }
      // jQuery button click event to add a row 
      $('#addBtn').on('click', function () { 
          if(x < max_fields_limit){ //check conditions
            x++; //counter increment
  
        // Adding a row inside the tbody. 
        $('#tbody').append(`<tr>
             <td><input required type="text" name="experience[start_year][]" class="form-control" placeholder="Start Year"></td><td><input required type="text" name="experience[end_year][]" class="form-control" placeholder="End Year"></td><td><input required type="text" name="experience[name][]" class="form-control" placeholder="Job Tittle"></td><td><input required type="text" name="experience[position][]" class="form-control" placeholder="Position"></td>/>
              <td class="text-center"> 
                <button class="btn btn-danger remove"
                  type="button">-</button> 
                </td> 
                
              </tr>`); 
          }
      }); 
  
      // jQuery button click event to remove a row. 
      $('#tbody').on('click', '.remove', function () { 
  
        // Getting all the rows next to the row 
        // containing the clicked button 
        var child = $(this).closest('tr').nextAll(); 
  
        // Iterating across all the rows  
        // obtained to change the index 
        child.each(function () { 
  
          // Getting <tr> id. 
          var id = $(this).attr('id'); 
  
          // Getting the <p> inside the .row-index class. 
          var idx = $(this).children('.row-index').children('p'); 
  
          // Gets the row number from <tr> id. 
          var dig = parseInt(id.substring(1)); 
  
          // Modifying row index. 
          idx.html(`Row ${dig - 1}`); 
  
          // Modifying row id. 
          $(this).attr('id', `R${dig - 1}`); 
        }); 
  
        // Removing the current row. 
        $(this).closest('tr').remove(); 
  
        // Decreasing total number of rows by 1. 
        rowIdx--; x--;
      }); 
    }); 
  </script> 
  <script> 
    $(document).ready(function () { 
  
      // Denotes total number of rows 
      var rowIdx = 0; 
      var max_fields_limit      = 5; //set limit for maximum input fields
     var y = 1; //initialize counter for text box
      var achievements_count = '<?= $achievements_count; ?>';
      if (achievements_count != null)
      {
        y = achievements_count;
      }

      // jQuery button click event to add a row 
      $('#addBtnAchieve').on('click', function () { 
          if(y < max_fields_limit){ //check conditions
            y++; //counter increment
  
        // Adding a row inside the tbody. 
        $('#tbodyachive').append(`<tr>
                        <td><input type="text" name="achievement[year][]" class="form-control" placeholder="Year" required></td>
                       <td><input type="text" name="achievement[name][]" class="form-control" placeholder="Achievement" required></td>
              <td class="text-center"> 
                <button class="btn btn-danger remove" 
                  type="button">-</button> 
                </td> 
                
              </tr>`); 
          }
      }); 
  
      // jQuery button click event to remove a row. 
      $('#tbodyachive').on('click', '.remove', function () { 
  
        // Getting all the rows next to the row 
        // containing the clicked button 
        var child = $(this).closest('tr').nextAll(); 
  
        // Iterating across all the rows  
        // obtained to change the index 
        child.each(function () { 
  
          // Getting <tr> id. 
          var id = $(this).attr('id'); 
  
          // Getting the <p> inside the .row-index class. 
          var idx = $(this).children('.row-index').children('p'); 
  
          // Gets the row number from <tr> id. 
          var dig = parseInt(id.substring(1)); 
  
          // Modifying row index. 
          idx.html(`Row ${dig - 1}`); 
  
          // Modifying row id. 
          $(this).attr('id', `R${dig - 1}`); 
        }); 
  
        // Removing the current row. 
        $(this).closest('tr').remove(); 
  
        // Decreasing total number of rows by 1. 
        rowIdx--; y--;
      }); 
    }); 
  </script> 

  <script>

// $('input#skills-and-knowledge').on('click', function(){
//   var valeur = 0;
//   $('input:checked').each(function(){
//        if ( $(this).attr('value') > valeur )
//        {
//            valeur =  $(this).attr('value');
//        }
//   });
//   $('.progress-bar').css('width', valeur+'%').attr('aria-valuenow', valeur);    
// });

var add_more_skill_counter = 1;
var add_more_skill_id = 1;
var skills_count = '<?= $skills_count ?>';
if (skills_count != null)
{
  add_more_skill_counter = skills_count;
  add_more_skill_id = skills_count;
}
var add_more_skill = (function addMoreSkill()
		{
      if (add_more_skill_counter < 6) {
        var form_element = [
          '<div class="container-item-skill-'+add_more_skill_id+' row">',
					'<div class="col-sm-4">',
        '<input type="text" name="skill[name]['+add_more_skill_id+']" class="form-control" placeholder="Skill" required>',
      '</div>',
      '<div class="col-sm-6">',
          '<label> BASIC </label>&nbsp;',
        '<label><input id="skills-and-knowledge" name="skill[scale]['+add_more_skill_id+']" class="progres" type="radio" value="1" required> 1 &nbsp;</label>',
        '<label><input id="skills-and-knowledge" name="skill[scale]['+add_more_skill_id+']" class="progres" type="radio" value="2" required> 2 &nbsp;</label>',
        '<label><input id="skills-and-knowledge" name="skill[scale]['+add_more_skill_id+']" class="progres" type="radio" value="3" required> 3 &nbsp;</label>',
        '<label><input id="skills-and-knowledge" name="skill[scale]['+add_more_skill_id+']" class="progres" type="radio" value="4" required> 4 &nbsp;</label>',
        '<label><input id="skills-and-knowledge" name="skill[scale]['+add_more_skill_id+']" class="progres" type="radio" value="5" required> 5 &nbsp;</label>',
        '<label> ADVANCED </label>',
      '</div>',
      '<div class="col-sm-2">',
        '<div class="button-group">',
            '<button id="remove-skill" class="btn btn-md btn-danger remove-skill-'+add_more_skill_id+'" type="button" onclick="removeSkill('+add_more_skill_id+')">-</button>',
        '</div>',
      '</div>',
      '<br>',
      '<br>',
      '</div>',
				];
        $("div#main-container-skill").append(form_element.join(''));
        add_more_skill_counter++;
        add_more_skill_id++;
      }
      else
      {
        alert('Maximum 6 skills only.');
      }
		});

    function removeSkill(add_more_skill_id)
		{
        $("div.container-item-skill-"+add_more_skill_id+"").remove();
        add_more_skill_counter--;
		};
  
    
  $("button#add-more-skill").on("click", add_more_skill);

  </script>
</body>

</html>



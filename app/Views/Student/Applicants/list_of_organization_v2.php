<div class="container">
    <br>
    <h1 class="text-center">Applicants</h1>
    <br>
    
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <h2>List of Organization</h2>
            </div>
        </div>
            
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Name</th>
                    <th>Sector</th>
                    <th>Address</th>
                    <th>State</th>
                    <th>Contact Number</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($organization_details) && is_array($organization_details)) : ?>
                    <?php foreach ($organization_details as $organization_details_data) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no ?></td>
                            <td><?= esc($organization_details_data['name']); ?></td>
                            <td><?= esc(organization_type($organization_details_data['type'])); ?></td>
                            <td><?= esc($organization_details_data['address']); ?></td>
                            <td><?= esc($organization_details_data['state_name']); ?></td>
                            <td><?= esc($organization_details_data['tel_no']); ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('public/student/StudentApplicants/view_organization/'.$organization_details_data['id']);?>"><span class="fas fa-eye"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>

    <br>
</div>


<div class="container">
    <br>
    <h1 class="text-center">Compare Organization</h1>
    <br>



<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover table-dark">
      <thead>
        <tr>
          <th scope="col">&nbsp</th>
          <th scope="col">
          <div class="form-group">
            <select id="compare-organization-1" name="compare_organization_1" class="form-control">
                <option value="" selected disabled>Choose Organization ...</option>
                <?php if (!empty($list_of_organization) && is_array($list_of_organization)) : ?>
                    <?php foreach ($list_of_organization as $list_of_organization_data) : ?>
                        <option value="<?= esc($list_of_organization_data['id']); ?>"><?= esc($list_of_organization_data['name']); ?></option>
                    <?php endforeach; ?>
                <?php endif ?>
            </select>
        </div>
          </th>
          <th scope="col">
          <div class="form-group">
            <select id="compare-organization-2" name="compare_organization_2" class="form-control">
                <option value="" selected disabled>Choose Organization ...</option>
                <?php if (!empty($list_of_organization) && is_array($list_of_organization)) : ?>
                    <?php foreach ($list_of_organization as $list_of_organization_data) : ?>
                        <option value="<?= esc($list_of_organization_data['id']); ?>"><?= esc($list_of_organization_data['name']); ?></option>
                    <?php endforeach; ?>
                <?php endif ?>
            </select>
        </div>
          </th>
          <th scope="col">
          <div class="form-group">
            <select id="compare-organization-3" name="compare_organization_3" class="form-control">
                <option value="" selected disabled>Choose Organization ...</option>
                <?php if (!empty($list_of_organization) && is_array($list_of_organization)) : ?>
                    <?php foreach ($list_of_organization as $list_of_organization_data) : ?>
                        <option value="<?= esc($list_of_organization_data['id']); ?>"><?= esc($list_of_organization_data['name']); ?></option>
                    <?php endforeach; ?>
                <?php endif ?>
            </select>
        </div>
          </th>
        </tr>
    
      </thead>
      <tbody class="text-center">
            <tr>
                <th>Name</th>
                <td id="compare-1-name"></td>
                <td id="compare-2-name"></td>
                <td id="compare-3-name"></td>
            </tr>
            <tr>
                <th>Organization Sector</th>
                <td id="compare-1-sector"></td>
                <td id="compare-2-sector"></td>
                <td id="compare-3-sector"></td>
            </tr>
            <tr>
                <th>Organization Address</th>
                <td id="compare-1-address"></td>
                <td id="compare-2-address"></td>
                <td id="compare-3-address"></td>
            </tr>
            <tr>
                <th>Organization State</th>
                <td id="compare-1-state"></td>
                <td id="compare-2-state"></td>
                <td id="compare-3-state"></td>
            </tr>
            <tr>
                <th>Working Days</th>
                <td id="compare-1-days"></td>
                <td id="compare-2-days"></td>
                <td id="compare-3-days"></td>
            </tr>
            <tr>
                <th>Working Hours</th>
                <td id="compare-1-hours"></td>
                <td id="compare-2-hours"></td>
                <td id="compare-3-hours"></td>
            </tr>
            <tr>
                <th>Email</th>
                <td id="compare-1-email"></td>
                <td id="compare-2-email"></td>
                <td id="compare-3-email"></td>
            </tr>
            <tr>
                <th>Tel. No.</th>
                <td id="compare-1-tel"></td>
                <td id="compare-2-tel"></td>
                <td id="compare-3-tel"></td>
            </tr>

        </tbody>
        <tfoot class="text-center">
          <tr>
            <th>&nbsp</th>
            
            <th class=""><a id="compare-1-view" href="<?= base_url('public/student/studentapplicants/view_organization');?>" class="btn btn-success" style="visibility: hidden;">View Details</a></th>
            <th class=""><a id="compare-2-view" href="<?= base_url('public/student/studentapplicants/view_organization');?>" class="btn btn-success" style="visibility: hidden;">View Details</a></th>
            <th class=""><a id="compare-3-view" href="<?= base_url('public/student/studentapplicants/view_organization');?>" class="btn btn-success" style="visibility: hidden;">View Details</a></th>
          </tr>
        </tfoot>

</table>
<div class="btn-form form-group text-right">
        <a href="<?= base_url('public/student/studentapplicants/organization_list');?>" class="btn btn-secondary">Back</a>
     </div>
</div>





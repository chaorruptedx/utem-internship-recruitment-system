<div class="container">
    <br>
    <h1 class="text-center">View Organization Details</h1>
    <br>

    <div class="text-center">
            <img src="<?= ($organization_details['profile_picture_name'] != null) ? base_url('public/'.$organization_details['profile_picture_path'].$organization_details['profile_picture_name']) : base_url('public/profile_picture/default_profile_picture.jpg'); ?>" alt="Profile Picture" class="mx-auto rounded" style="max-width: 200px; max-height: 200px;">
    </div><br><br>

    <div class="table-responsive">
        <table id="data-table" class="table table-bordered table-striped table-hover table-dark">
            <tbody class="text-center">
                <tr>
                    <td class="font-weight-bold" style="width: 200px">Organization Name</td>
                    <td><?= $organization_details['name']; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Type</td>
                    <td><?= ($organization_details['type'] == 1) ? 'Government' : 'Private'; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Address</td>
                    <td><?= $organization_details['address']; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">State</td>
                    <td><?= $organization_details['state_name']; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Working Day</td>
                    <td><?= working_day($organization_details['start_day']); ?> - <?= working_day($organization_details['end_day']); ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Wokring Hours</td>
                    <td><?= date("g:i A", strtotime($organization_details['open_hour'])); ?> - <?= date("g:i A", strtotime($organization_details['close_hour'])); ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Job Description</td>
                    <td><?= $organization_details['job_description']; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Tel. No.</td>
                    <td><?= $organization_details['tel_no']; ?></td>
                </tr>
            </tbody>
        </table>
    </div>

    <br>

    <div class="text-right btn-form form-group">
        <a href="<?= base_url('public/student/StudentApplicants/organization_list');?>" class="btn btn-secondary"><span class="fas fa-chevron-left"></span>&nbsp;&nbsp;Back</a>
        <button type="submit" name="submit" data-toggle="modal" data-target="#myModal" class="btn btn-<?= ($internship_application) ? 'secondary' : 'primary'; ?>" <?= ($internship_application) ? 'disabled' : ''; ?>><span class="fas fa-check"></span>&nbsp;&nbsp;<?= ($internship_application) ? 'Applied' : 'Apply'; ?></button>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Internship Application</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    Are you sure you want to apply internship at <b><?= esc($organization_details['name']); ?></b>?
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <a href="<?= base_url('public/student/StudentApplicants/apply_organization/'.$organization_details['id']);?>" type="button" class="btn btn-success"><span class="fas fa-check"></span>&nbsp;&nbsp;Yes</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times"></span>&nbsp;&nbsp;No</button>
                </div>
            </div>
        </div>
    </div>

    <br>
</div>
<div class="page-header">
    <br>
    <h1 class="text-center">LIST OF ORGANIZATION</h1> </br>
  </div>
<style>
    
.cardcon {
  background-color: #a0d6e6;
  padding: 20px;
  margin-top: 20px;
  border-radius: 25px;
}

</style>
  <div class="container">
    <form action="<?= base_url('public/student/StudentApplicants/organization_list/');?>" method="post">
        <div class="row">
            <div class="col-8 offset-2">
            </div>
          <div class="col-8 offset-2">
            <div class="form-group">
              <label for=""></label>
              <input    class="form-control"  type="text" name="name" id=""   placeholder="Organization Name" value="<?= $name_search ?>">
            </div>
          </div>

          <div class="col-8 offset-2">
            <div class="form-group">
              <select class="form-control" name="id_state">
                <option value="" selected disabled>Choose State ...</option>
                <?php if (!empty($al_state) && is_array($al_state)) : ?>
                    <?php foreach ($al_state as $al_state_data) : ?>
                        <option value="<?= $al_state_data['id'] ?>"><?= $al_state_data['name'] ?></option>
                    <?php endforeach; ?>
                <?php endif ?>
              </select>
            </div>
          </div>

          <div class="col-8 offset-2">
            <div class="form-group">
                <input    class="form-control"  type="text" name="job_description"  placeholder="Keyword" value="<?= $job_description_search; ?>" >
            </div>
          </div>

        </div>

        <div class="row text-center">
          <div class="col-1"></div>
          <div class="col-5">
            <input class="btn btn-light" type="reset" value="Reset" />
          </div>
          <div class="col-5">
            <input class="btn btn-info" type="submit" value="Search" />
          </div>
          <div class="col-1"></div>
        </div>
    </form>

        <br>
        <br>
    <div class="row">
        <?php if (!empty($organization_details) && is_array($organization_details)) : ?>
            <?php foreach ($organization_details as $organization_details_data) : ?>
                
                    <div class="col-md-6">
                        <div class="cardcon"> 
                            <table class="table table-striped">
                                <thead>
                                    <tbody>
                                        <tr>
                                            <th>ORGANISATION NAME</th>
                                            <td><?= $organization_details_data['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>SECTOR</th>
                                            <td>
                                                <?php
                                                if (esc($organization_details_data['type']) == '1') :
                                                    echo 'Government';
                                                elseif (esc($organization_details_data['type']) == '2') :
                                                    echo 'Private';
                                                endif;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ADDRESS</th>
                                            <td><?= $organization_details_data['address']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>STATE</th>
                                            <td><?= $organization_details_data['state_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>CONTACT NUMBER</th>
                                            <td><?= $organization_details_data['tel_no']; ?></td>
                                        </tr>
                                    </tbody>
                                </thead>
                            </table>
                            <div class="col text-center">
                                <a href="<?= base_url('public/student/StudentApplicants/view_organization/'.$organization_details_data['id']);?>" class="btn btn-primary"><span class="fas fa-eye"></span>&nbsp;&nbsp;View Details</a>
                            </div>
                        </div>
                    </div>
                
            <?php endforeach; ?>
        <?php endif ?>
    </div>

</div>
    

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>UTeM Internship Recruitment System</title>
	<style>
		body {
			background-image: url("<?php echo base_url('public/background/Background.jpg'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
		.copyrights {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			background-color: #212529;
			color: #FFFFFF80;
			text-align: center;
		}
	</style>
	<link rel="stylesheet" href="<?php echo base_url('public/assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/datatables/datatables.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/select2/css/select2.min.css'); ?>">
	<!--link rel="stylesheet" href="<?php echo base_url('public/assets/css/style.css'); ?>"-->


<!-- GOOGLE FONTS -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
	

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



<body>

<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<div class="container">
			<ul class="navbar-nav">
				<a class="navbar-brand" href="#">
					<img src="<?= base_url('public/logo/Universiti Teknikal Malaysia Melaka (UTeM) - Logo.png'); ?>" alt="logo" style="width:50px;">
				</a>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('public/student/dashboard');?>"><span class="fas fa-home"></span>&nbsp;&nbsp;Home</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
						<span class="fas fa-file"></span>&nbsp;&nbsp;Resume
					</a>
					<div class="dropdown-menu pull-right">
						<a class="dropdown-item" href="<?= base_url('public/student/resume');?>"><span class="fas fa-file-csv"></span>&nbsp;&nbsp;My Resume</a>
						<!-- <a class="dropdown-item" href="<?= base_url('public/student/resume/create_resume');?>"><span class="fas fa-edit"></span>&nbsp;&nbsp;Create Resume</a> -->
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
						<span class="fas fa-file-invoice"></span>&nbsp;&nbsp;Applicants
					</a>
					<div class="dropdown-menu pull-right">
						<a class="dropdown-item" href="<?= base_url('public/student/studentapplicants/organization_list');?>"><span class="fas fa-list"></span>&nbsp;&nbsp;List of Organization</a>
						<a class="dropdown-item" href="<?= base_url('public/student/studentapplicants/index');?>"><span class="fas fa-user-cog"></span>&nbsp;&nbsp;Comparing Organization</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
						<span class="fas fa-user"></span>&nbsp;&nbsp;Hi, <?= (session()->get('name')) ? session()->get('name') : session()->get('email'); ?>
					</a>
					<div class="dropdown-menu pull-right">
						<a class="dropdown-item" href="<?= base_url('public/student/account_management');?>"><span class="fas fa-user-cog"></span>&nbsp;&nbsp;Account Management</a>
						<a class="dropdown-item" href="<?= base_url('public/student/logout');?>"><span class="fas fa-sign-out-alt"></span>&nbsp;&nbsp;Logout</a>
					</div>
				</li>
			</ul>
		</div>
  	</div>
</nav>

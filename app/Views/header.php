<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>UTeM Internship Recruitment System</title>
	<style>
		body {
			background-image: url("<?php echo base_url('public/background/Background.jpg'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
		.copyrights {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			background-color: #212529;
			color: #FFFFFF80;
			text-align: center;
		}
	</style>
    <link rel="stylesheet" href="<?php echo base_url('public/assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/assets/login/css/main.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/assets/login/css/util.css'); ?>">
</head>

<body>

<nav class="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<div class="container">
			<ul class="navbar-nav">
				<a class="navbar-brand" href="#">
					<img src="<?= base_url('public/logo/Universiti Teknikal Malaysia Melaka (UTeM) - Logo.png'); ?>" alt="logo" style="width:50px;">
				</a>
				<li class="nav-item">
					<a class="nav-link <?= active_link(['Home'], ['index']); ?>" href="<?= base_url('public/');?>"><span class="fas fa-home"></span>&nbsp;&nbsp;Home</a>
				</li>
			</ul>
		</div>
  	</div>
</nav>
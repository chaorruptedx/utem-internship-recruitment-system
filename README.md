# UTeM Internship Recruitment System

## Requirements

* Apache
* PHP version 7.2 or higher
* MySQL
* Git - https://git-scm.com/
* Brain

## Installation

* Download source code => ```git clone https://gitlab.com/chaorruptedx/utem-internship-recruitment-system```
* Import database from `database/utem_internship_recruitment_v5.2.sql` into your MySQL
* Update database configuration. Edit the file `app/Config/Database.php` with real data (do not push the Database.php into the GitLab), for example:
```php
public $default = [
		'DSN'      => '',
		'hostname' => 'localhost',
		'username' => 'root',
		'password' => 'Zaki0123',
		'database' => 'utem_internship_recruitment',
		'DBDriver' => 'MySQLi',
		'DBPrefix' => '',
		'pConnect' => false,
		'DBDebug'  => (ENVIRONMENT !== 'production'),
		'cacheOn'  => false,
		'cacheDir' => '',
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		'port'     => 3306,
	];
```

## Git Command

* To Pull System from GitLab:
	* ```git pull```

* To Push System to GitLab:
	* ```git pull```
	* ```git status```
	* ```git add YourFilePathwayName```
	* ```git status```
	* ```git commit -m "YourMessage"```
	* ```git push```

## References

* Trello - https://trello.com/b/0H0jvWkn/workshop-2
* CodeIgniter 4 - https://codeigniter.com/user_guide/index.html
* Bootstrap 4 - https://www.w3schools.com/bootstrap4/
* Font Awesome 5 - https://www.w3schools.com/icons/fontawesome5_intro.asp